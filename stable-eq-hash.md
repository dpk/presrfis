# Generation-friendly stable `eq?-hash` with compaction and low memory overhead

To explain this system, I will make two simplifying assumptions: that all heap-allocated objects are exactly one word in size; and that memory is allocated in regions of fixed sizes. None of these assumptions are core to the system and overcoming them does not necessarily introduce new indirections to memory access.

The actual memory allocated by the GC is organized into folios. New terminology is used only to avoid strongly tying the concept of a folio to any existing part of a memory manager’s internals.

Each folio consists of two parts. The first is an actual region of memory allocated by the collector, initially prepared to hold a fixed number of word-sized objects. The second is a header which contains a fixnum-sized identifier for the folio and a bitvector called the deadvector which always has as many bits as the initial number of word-sized objects held by the region, initialized set to all zeroes. Both the region and the header can safely be relocated for compaction (provided pointers throughout the heap are updated, of course). The identifier for a folio must be unique among all live folios. Given the known live current address of an object, looking up both the corresponding header of the folio it lives in, and by extension the address of the start of its region, are assumed to be reasonably cheap operations.

To allocate an object, the memory manager simply finds the folio it is currently writing to and returns the address of the next unused word-sized object within its region. The folios belong to, or directly correspond to, a generation of managed objects.

When the collector wishes to compact the part of the heap corresponding to a particular folio, it copies and shrinks the folio region, removing each dead objects and setting the bit in the deadvector corresponding to the position of that object in the original folio.

To take an `eq?-hash`, then, one finds the folio header associated with a live object, subtracts the live address of the start of the folio region from the live address to find the current index within the array of managed words. Then, for each 1 bit within the deadvector until as many 0 bits as the original current index have been seen, add 1 to the current index to get the original position of the object within the folio before it was compacted. This is the low part of the `eq?-hash`. Then shift the unique folio identifier to the left by as many bits as are needed to hold the low part and IOR the two together and you have your hash.

## Practical improvements

Representing objects more than one word in size is a trivial adaptation: simply consider that object to occupy more than one position in the folio and consequently also in the deadvector.

In reality, of course, objects which are only one word in size are extremely rare. An immediate saving can be made for the majority of use cases by assigning one bit in the deadvector only to every two words in the managed region, or only to every four words, depending on the strategy used to represent pairs. One could even choose the granularity of the deadvector according to the types of objects held within the folio, for maximum memory efficiency.

Objects larger than the fixed region size will require the collector to allocate folios of different sizes (or to add an indirection to the representation). This too is a fairly trivial adaptation, though it does add some overhead to the folio header for an extra size field.

A potential issue for some memory-intensive systems is that one may end up with a large number of folios each containing very few objects, such that the overhead of the folio headers and deadvectors is larger than those objects themselves. This situation could be avoided by killing the original folio structures and moving such objects into a ‘dead folio summary area’, where they are simply stored together directly with their hash values, reducing the overhead from the size of the folio header to just one extra word per object affected.

This system can potentially even support R2RS `object-unhash` safely with not totally unreasonable efficiency: split a hash back into the position and unique folio identifier, find the folio, and find the object within the folio.
