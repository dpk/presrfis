;; (read-line* port terminator) where port is an input port, and
;; terminator is either a bytevector or integer ≤ 255, in which case
;; the procedure reads bytes and returns a bytevector, or a string or
;; a character, in which case the procedure reads characters and
;; returns a string.
;;
;; If port is in the EOF state when read-line* is called, it returns
;; an EOF object. Otherwise, it reads from port until it encounters
;; a sequence of characters or bytes matching terminator, or EOF. It
;; then returns a string or bytevector containing all the characters
;; or bytes up to and inclusive of the terminator. If EOF is reached,
;; the returned string or bytevector will not end in the terminator.
(define (read-line* port terminator)
  (cond ((bytevector? terminator)
         (read-binary-line port terminator))
        ((and (integer? terminator)
              (<= terminator 255))
         (read-binary-line port (bytevector terminator)))
        ((string? terminator)
         (read-character-line port terminator))
        ((char? terminator)
         (read-character-line port (string terminator)))
        (else (error "terminator argument to read-line* must be string, character, bytevector, or integer <= 255" terminator))))

(define (read-binary-line port terminator)
  (read-any-line port terminator
                 read-u8 peek-u8 write-u8
                 = ;; could be fx=?
                 bytevector-u8-ref (bytevector-length terminator)
                 open-output-bytevector get-output-bytevector))

(define (read-character-line port terminator)
  (read-any-line port terminator
                 read-char peek-char write-char
                 char=?
                 string-ref (string-length terminator)
                 open-output-string get-output-string))

(define (read-any-line port terminator
                       read-morsel peek-morsel write-morsel
                       morsel=?
                       ref terminator-length
                       open-output get-output)
  (if (eof-object? (peek-morsel port)) (eof-object)
      (call-with-port
       (open-output)
       (lambda (ouf)
         (let read-more ((poss '()))
           (if (any (lambda (x) (>= x terminator-length)) poss)
               (get-output ouf)
               (let ((m (read-morsel port)))
                 (if (eof-object? m)
                     (get-output ouf)
                     (let ((new-poss
                            (filter-map
                             (lambda (pos)
                               (and (morsel=? m (ref terminator pos))
                                    (+ pos 1)))
                             poss)))
                       (write-morsel m ouf)
                       (read-more
                        (if (morsel=? m (ref terminator 0))
                            (cons 1 new-poss)
                            new-poss)))))))))))
