# contrib tree

This tree contains possibly useful code contributed by others which is only of indirect relevance to things elsewhere in my Pre-SRFIs tree. I claim no ownership or responsibility for it.
