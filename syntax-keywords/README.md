# Syntax-Based Keyword Arguments to Procedures and Macros

## Defining Procedures and Procedure-Like Syntax: `define-with-keywords`

### Issues

- Bikeshed the name `define-with-keywords`
- The ability to have a rest argument would be very nice
- As would the ability, at the very least, to put the keyword arguments before the positional ones (like SRFI 89 allows), if not to just mix and match them in any order (like Racket allows), which would make having a rest argument much more natural
  - But this causes problems with the feature where keyword arguments can be given positionally
- Lastly, a way to gather rest keyword arguments would be very handy, but also very tricky without keywords being bound and having identifier properties

### Introduction

`define-with-keywords` provides a SRFI 89-like way to define procedure-like syntax (periphrastic macros) that take keyword arguments in pure R7RS small. The macros created behave somewhat like Python’s keyword argument functions in that keyword arguments can also be specified positionally, by giving them in the order they appear in the definition. The difference is that arguments can be positional-only but not keyword-only — in other words, it supports the equivalent of Python’s `/` operator in function definitions, but not the equivalent of the (bare) `*` operator. This is mainly because of limitations of `syntax-rules` (keyword names cannot automatically be generated from binding names).

```scheme
(define-with-keywords (lyst a (k: k) (l: l (* k 2)))
  (list a k l))

(lyst 1)      ;;=> ERROR: required keyword argument missing, k:
(lyst 1 k: 2) ;;=> (1 2 4)
(lyst 1 k: 2 l: 3) ;;=> (1 2 3)
(lyst 1 l: 2 k: 3) ;;=> (1 3 2)

;; Providing keyword arguments positionally.
(lyst 1 2)   ;;=> (1 2 4)
(lyst 1 2 3) ;;=> (1 2 3)
```

The above examples work in the implementation given here on Chibi, Chicken (with `(keyword-syntax #f)`), Gauche, and Guile. It doesn’t work in Gerbil, probably firstly because `k:` and `l:` get parsed as keyword datums and not as identifiers as the code expects, but even if you change them to e.g. `:k` and `:l` it doesn’t like the expansion of `define-with-keywords` for an unknown reason. It won’t work natively on R6RS implementations that don’t support the extended `syntax-rules` of R7RS, but I’m told it works with [this](../r7rs-syntax-rules.scm) wrapper around `syntax-rules` in at least Chez.

`define-with-keywords` as used above actually defines macros; that is, `lyst` has no first-class status. You can pass an extra identifier as the first argument to `define-with-keywords` and it will be bound to a real, first-class procedure taking the same required and optional arguments, but only positionally:

```scheme
(define-with-keywords lyst-proc (lyst a (k: k) (l: l (* k 2)))
  (list a k l))

;; `lyst` itself will work as above, but it’s also possible to do
;; things like this with `lyst-proc`:
(map lyst-proc '(1 2 3) '(4 5 6)) ;; => ((1 4 8) (2 5 10) (3 6 12))
```

### Specification

*Syntax:*

```scheme
(define-with-keywords
  [proc-name]
  (name positional-arg ...
        (required-keyword-name required-keyword-binding) ...
        (optional-keyword-name optional-keyword-binding initializer) ...)
  body ...)
```

`proc-name` (if given), `name`, and all of the `positional-arg`, `required-keyword-name`, `required-keyword-binding`, `optional-keyword-name`, and `optional-keyword-binding` are identifiers. `initializer`s can be any expression. `body` has the same meaning as in a `lambda` expression.

Note that all the required keyword arguments have to come before the optional keyword arguments in the formal parameters of the macro. (This does not affect the order in which keyword arguments may be passed to the defined macro.) Also, periphrastic macros created with `define-with-keywords` can only have a fixed number of positional arguments and no rest arguments.

*Semantics:* Binds the syntax keyword `name` to a syntax transformer that acts like a procedure receiving the given positional arguments, followed by keyword arguments.

[srfi-206]: https://srfi.schemers.org/srfi-206/srfi-206.html

When the macro defined under `name` is expanded, an ordered sequence <var>A</var> created, containing as many slots as the total number of `positional-arg`s + `required-keyword-binding`s + `optional-keyword-binding`s. Each `required-keyword-name` and `optional-keyword-name` is associated with the slot corresponding to its binding. Each slot is initially empty. Expansion proceeds as follows:

1. The first <var>n</var> slots in <var>A</var> are filled with the expressions given as arguments to the macro `name` for the <var>n</var> `positional-arg`s.
2. Further slots are filled from left to right with further expressions in the argument list of the macro `name`, until a `required-keyword-name` or `optional-keyword-name` auxiliary syntax keyword is encountered. (This could be immediately, after zero such further slots have been filled.)
3. Each of the following consecutive pairs of two arguments to the macro `name` consists of one `required-keyword-name` or `optional-keyword-name` (the keyword name) followed by a value expression. For each such pair of consecutive arguments, the empty slot corresponding to the keyword name is filled with the value expression, until all such pairs have been processed.
4. The final expansion of the macro `name` consists of the application of a procedure <var>f</var>, specified in the next section, to the arguments <var>A</var>. Any remaining empty slots, corresponding to `optional-keyword-name`s, are filled with an expression which will evaluate to an unspecified but distinct value ∅.

<var>f</var> is a procedure taking as many arguments as the number of slots in <var>A</var>. The first <var>n</var> arguments, where <var>n</var> is as defined above, have the names of the `positional-arg`s of the macro `name` in left-to-right order. The subsequent <var>n′</var> arguments, where <var>n′</var> is the number of `required-keyword-binding`s, have the names of the `required-keyword-binding`s in left-to-right order. All the remaining arguments, of which there are <var>n″</var> corresponding to the `optional-keyword-binding`s, have unique temporary names.

When <var>f</var> is called, each of the `optional-keyword-binding`s is bound from left-to-right as if by `let*` according to the following rule: if the corresponding argument with a unique temporary name has the value ∅, then it is bound to the result of evaluating the corresponding expression `initializer`; otherwise it is bound to the value of that argument. The `body` given at the definition of the macro `name` is evaluated in the environment containing all the bindings of the `positional-arg`s, `required-keyword-binding`s, and `optional-keyword-binding`s, and the return value of the call to <var>f</var> is the value of this evaluation.

This algorithm is not prescriptive, and implementations may use alternative implementation strategies, provided that the same result is achieved for all non-erroneous inputs to the macro `name`.

When invoking the macro defined under `name`, it is an error:

- to fail to provide required positional arguments before the keyword bindings (this implementation will do whatever the host implementation does when an auxiliary syntax keyword is used in the wrong position);
- to attempt to provide by a keyword binding a value to a keyword parameter which has already been passed positionally (this implementation will prefer the positionally-given value);
- to attempt to provide two keyword bindings for the same keyword parameter (this implementation will take the last one given);
- if, after one keyword name has appeared, the remainder of the argument list to the macro does not follow the correct property-list–like pattern of keyword names followed by value expressions (this implementation will treat out-of-place keyword names and value expressions as additional positional arguments);
- to fail to provide a required keyword binding (this implementation will signal a `syntax-error`).

(The lax behaviour of this implementation can be fixed to signal an error in each case. In some cases this would be easy, in other cases less so.)

If `proc-name` is given, it is bound as if by [SRFI 227][srfi-227] `define-optionals*` to a procedure with the body `body` which receives the mandatory positional arguments named by `positional-arg`s and `required-keyword-binding`s, and the optional arguments named by `optional-keyword-binding`s and initialized by the corresponding `initializer` expressions, in the order given in the definition of the macro `name`.

[srfi-227]: https://srfi.schemers.org/srfi-227/srfi-227.html

### Suggested Behaviour on Schemes with `syntax-case`

(Not actually implemented yet.)

On Schemes with R6RS `syntax-case`, `name` is bound to identifier syntax that returns the same procedure as `proc-name` is (or would be) bound to when not used in operator position.

## Defining Macros with Keyword Arguments: `keyword-syntax`

### Issues

- This is a terrible hack: a completely new approach, not being embedded inside `syntax-rules` but rather replacing it, would be better

### Introduction

`keyword-syntax` provides a simple way to destructure keyword arguments within a `syntax-rules` macro.

For example, the SRFI 9/R7RS small `define-record-type` macro depends on argument order, which can be difficult to remember. `define-struct` uses keyword arguments:

```scheme
(define-syntax define-struct
  (syntax-rules ::: ()
    ((_ . args)
     (keyword-syntax args
         ((name: name)
          (predicate: predicate)
          (constructor: constructor)
          (fields: ((field-name accessor . maybe-setter) ...)))
       (define-record-type name
         (constructor field-name ...)
         predicate
         (field-name accessor . maybe-setter) ...)))))

(define-struct name: <pare>
               predicate: pare?
               constructor: kons
               fields: ((x kar set-kar!)
                        (y kdr set-kdr!)))
```

### Specification

*Syntax:*

```scheme
(keyword-syntax args ((keyword-name pattern [default]) ...) template)
```

All of the `keyword-name`s must be identifiers. All of the `pattern`s must be valid `syntax-rules` patterns. (It is an error (i.e. I haven’t worked out in what particular unexpected way it will behave when you do this) if `pattern` contains a pattern variable name which is bound by a `syntax-rules` pattern which lexically encloses it.) `args` must be a list of alternating `keyword-name`s and values. `template` must be a valid `syntax-rules` template with respect to the concatenation of all the `pattern`s. (Note that pattern variables bound by a lexically-enclosing `syntax-rules` pattern will be substituted into this template before `keyword-syntax` sees it. I’m fairly certain this won’t cause unexpected behaviour in the general case, but I can’t be 100% sure.)

TODO: Allow renaming the ellipsis used in `keyword-syntax`.

*Semantics:*

TODO: Work out how to sanely specify the semantics.

## Acknowledgements

The idea of doing keyword arguments somewhat in this fashion was suggested by Marc Nieper-Wißkirchen. I learned the trick for matching keyword arguments in `syntax-rules` from Alex Shinn’s [`(chibi binary-record)`.](https://github.com/ashinn/chibi-scheme/blob/master/lib/chibi/binary-record.scm) gwatt on Libera Chat #scheme pointed out how to make it work on Chez Scheme by hacking ellipsis renaming into R6RS’s `syntax-rules`.
