(define-syntax %required-keyword-missing
  (syntax-rules ()
    ((_ name) (syntax-error "required keyword argument missing" 'name))))

(define-syntax keyword-syntax
  (syntax-rules ()
    ((_ args ((keyword-name pattern-var . maybe-default) ...) template)
     (%ks-generate-temporaries ()
                               ((keyword-name pattern-var . maybe-default) ...)
                               args template))))

(define-syntax %ks-generate-temporaries
  (syntax-rules ()
    ((_ specs-w/gensyms
        ((keyword-name pattern-var . maybe-default)
         . more-specs-w/o-gensyms)
        args template)
     (%ks-generate-temporaries
      ((keyword-name gensym pattern-var . maybe-default) . specs-w/gensyms)
      more-specs-w/o-gensyms args template))
    ((_ specs-w/gensyms () args template)
     (%ks-build-macro keyword-expand ()
                      specs-w/gensyms
                      () () () ()
                      args template))))

(define-syntax %ks-build-macro
  (syntax-rules ()
    ((_ ke-name
        (clause ...)
        ((keyword-name gensym pattern-var)
         (more-keyword-name more-gensym more-pattern-var . more-maybe-default) ...)
        handled-keyword-names
        (handled-gensym ...)
        (handled-pattern-var ...)
        (handled-default ...)
        args template)
     (%ks-build-macro
      ke-name
      (((_ (keyword-name new-value . more-args)
           (handled-gensym ... gensym more-gensym ...))
        (ke-name more-args
                 (handled-gensym ... new-value more-gensym ...)))
       clause ...)
      ((more-keyword-name more-gensym more-pattern-var . more-maybe-default) ...)
      (keyword-name . handled-keyword-names)
      (handled-gensym ... gensym)
      (handled-pattern-var ... pattern-var)
      (handled-default ... (%required-keyword-missing keyword-name))
      args template))
    ((_ ke-name
        (clause ...)
        ((keyword-name gensym pattern-var default)
         (more-keyword-name more-gensym more-pattern-var . more-maybe-default) ...)
        handled-keyword-names
        (handled-gensym ...)
        (handled-pattern-var ...)
        (handled-default ...)
        args template)
     (%ks-build-macro
      ke-name
      (((_ (keyword-name new-value . more-args)
           (handled-gensym ... gensym more-gensym ...))
        (ke-name more-args
                 (handled-gensym ... new-value more-gensym ...)))
       clause ...)
      ((more-keyword-name more-gensym more-pattern-var . more-maybe-default) ...)
      (keyword-name . handled-keyword-names)
      (handled-gensym ... gensym)
      (handled-pattern-var ... pattern-var)
      (handled-default ... default)
      args template))

    ((_ ke-name (clause ...) () (keyword-name ...) (gensym ...) (pattern-var ...) defaults args template)
     (%letrec-syntax
         ((ke-name
           (syntax-rules ::: (keyword-name ...)
             clause ...
             ((_ () (args* :::))
              (%let-syntax
                  ((keyword-pattern-var-bind
                    (syntax-rules ()
                      ((_ pattern-var ...) template))))
                (keyword-pattern-var-bind args* :::))))))
       (ke-name args defaults)))))

;; Local Variables:
;; eval: (put '%let-syntax 'scheme-indent-function 1)
;; eval: (put '%letrec-syntax 'scheme-indent-function 1)
;; End:
