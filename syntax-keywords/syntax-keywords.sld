(define-library (syntax-keywords)
  (import (scheme base)
          (srfi 227))
  (export define-with-keywords)

  (include "syntax-keywords.scm"))
