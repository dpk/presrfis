(define-library (keyword-syntax)
  (import (scheme base))

  (cond-expand
   ((library (srfi 188))
    (import (srfi 188))
    (begin
      (define-syntax %let-syntax
        (syntax-rules ()
          ((_ . blah) (splicing-let-syntax . blah))))
      (define-syntax %letrec-syntax
        (syntax-rules ()
          ((_ . blah) (splicing-letrec-syntax . blah))))))
   (else
    (import (scheme write))
    (begin
      (display "WARNING: (keyword-syntax) imported on a Scheme with no \
                splicing-let-syntax support; macros defined using it will \
                not be able to productively expand into definitions"
               (current-error-port))
      (begin
        (define-syntax %let-syntax
          (syntax-rules ()
            ((_ . blah) (let-syntax . blah))))
        (define-syntax %letrec-syntax
          (syntax-rules ()
            ((_ . blah) (letrec-syntax . blah))))))))

  (include "keyword-syntax.scm"))
