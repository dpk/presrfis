# Faceted ports: an alternative to the custom ports of R6RS/SRFI 181

**Help wanted!** I would be *very* grateful if someone who finds this proposal good would pick it up and run with it and turn it into a SRFI. Besides my numerous responsibilities in R7RS Large, I have a degree to finish and will have to start taking on paying work very soon to try to make a start on paying back my student loan. As chair of WG2, the responsibility for speccing out these things falls on me by default, so it will probably happen anyway at some point, but it will happen faster – and get the whole R7RS Large project finished faster – if someone else does it.

## Abstract

An alternative interface for creating custom ports and for accessing information from ports is defined. The interface is more flexible and extensible than the rigid set of port operations defined for custom ports in [R6RS](https://www.r6rs.org/corrected/html/r6rs-lib/r6rs-lib-Z-H-9.html) and in its close relative [SRFI 181](https://srfi.schemers.org/srfi-181/srfi-181.html). Some other minor issues with these previous specifications are also corrected.

## Issues

See the ISSUE sections throughout the document.

## Rationale

The relation between ports and facets in this SRFI can be thought of as similar to the relation between compound and simple conditions in R6RS, or compounds and components in SRFI 222. However, unlike in condition objects, a port is not a facet, and a facet is not a port: the two form distinct layers of the interface. It is not possible to compound two ports together into a single port – you cannot compound an input port and an output port into a single bidirectional port – for reasons including that port position getting and setting between the two underlying ports would not be guaranteed to work properly in this case. Therefore it is also not possible to extract a list of the facets from a port after creating it: you must only interact with the port using the public API. To ensure this, it is also not possible to extract the field values of a facet from the facet itself: it must be compounded into a port first to provide a public API.

In a sense, one can think of facets as an arbitrarily-extensible set of keyword arguments to the `make-port` operation: the behaviour of them and restrictions on them are compatible with this interpretation.

Compared to R6RS and SRFI 181, this interface allows ports to be extended with arbitrary properties and operations.

## Specification

### Facet types

Facet types are opaque, sealed record types whose fields are all immutable. They are created using a special form defined below. The fields of a facet type cannot be accessed on facets themselves, only through operations on ports which incorporate facets of those types. In this specification, facet types are listed first with the identifier for their record type descriptor, then with the name and signature for their constructor procedures (including the fields of the facet type as the arguments), then as the type test predicate for the record type, then finally as any additional port operations which are publically exported which use the facet type.

ISSUE: Do we even need to expose the record type descriptor, since these things are sealed and opaque? That would let us get rid of the `make-` prefix on the constructor procedures.

### Creating ports

#### `(make-port UNDERLYING-PORT FACET ...)` — procedure

Create a new port out of the given `FACET`s. A single port must not have more than one `FACET` of the same type.

ISSUE: I think that restriction can’t easily be enforced as long as facet types are opaque.

The `UNDERLYING-PORT` is either another port object or `#f`. When an operation other than those that use any of the essential facets, or one of the other `FACET`s passed to the `make-port` procedure call, is invoked on the port created by `make-port`, it looks first in the facets passed to `make-port`, then at those in the underlying port, then in the port underlying that, etc. The `UNDERLYING-PORT` is locked once the new port is created.

ISSUE: Needs better spec.

#### `(port-lock PORT)` — procedure

Locks the given `PORT` and returns an unlocked copy of the port.

Locking a port prevents any operation which uses an essential or utility facet from being performed on that port. The unlocked copy must be used for all such future operations.

#### `(port-locked? PORT)` — procedure

Returns `#t` if the given `PORT` is locked, or `#f` if it is not locked.

Implementations of new port procedures which make use of a new utility facet should check whether the port is locked before allowing that procedure to be used on a port.

#### `(make-transcoded-port UNDERLYING-PORT TRANSCODER)` — procedure

A convenience procedure for creating a textual port from a binary port. Does what you think it does.

ISSUE: Needs better spec.

### Essential facets

Every port must contain at least one of these facets. Furthermore, a `read-bytes-facet` can be compounded together with a `write-bytes-facet` or vice versa; and a `read-chars-facet` can be compounded together with a `write-chars-facet` or vice versa; but no other combination of these essential facets is allowable. An assertion violation should be signalled by `make-port` if none of these facets are present, or if a combination of facets is used which violates this restriction.

#### `read-bytes-facet` — facet type <br> `(make-read-bytes-facet READER-PROC)` — procedure <br> `(read-bytes-facet? OBJ)` — procedure

`READER-PROC` is a procedure of three arguments which is called by the implementation to slurp bytes from a binary port:

- `BYTEVECTOR` is a mutable input bytevector whose length and nature is unspecified except as required by the two below arguments;

- `START` is a nonnegative exact integer which is less than the maximum index in `BYTEVECTOR`

- `COUNT` is a nonnegative exact integer where `START` + `COUNT` is less than or equal to the maximum index in `BYTEVECTOR`

Reading from a port with a `read-bytes-facet` results in the `READER-PROC` being called: it should read up to `COUNT` bytes into `BYTEVECTOR` (destructively modifying the existing contents) beginning at the index `START` and return the number of bytes it actually read as an exact integer. If the port is at the end of the file, it should return 0 and leave `BYTEVECTOR` untouched.

NOTE: The signature of the `READER-PROC` is compatible with the `READ!` procedure on custom bytevector ports in R6RS and SRFI 181.

ISSUE: Should `READER-PROC` return something other than 0 to indicate end of file?

#### `write-bytes-facet` — facet type <br> `(make-write-bytes-facet WRITER-PROC)` — procedure <br> `(write-bytes-facet? OBJ)` — procedure

`WRITER-PROC` is a procedure of three arguments which is called by the implementation to spit bytes into a binary port. The three arguments have the same specification as for `read-bytes-facet`.

Writing to a port with a `write-bytes-facet` results in the `WRITER-PROC` being called: it should write up to `COUNT` bytes from `BYTEVECTOR`, beginning from the byte at index `START`. If `COUNT` is 0, the port is at the end of file. `WRITER-PROC` should return the number of bytes written as an exact integer.

ISSUE: Should `WRITER-PROC` use a `COUNT` other than 0 to send EOF?

#### `read-chars-facet` — facet type <br> `(make-read-chars-facet READER-PROC)` — procedure <br> `(read-chars-facet? OBJ)` — procedure

`READER-PROC` is a procedure of three arguments which is called by the implementation to slurp characters from a textual port:

- `U32VECTOR` is a mutable input u32vector ([SRFI 160](https://srfi.schemers.org/srfi-160/srfi-160.html)) whose length and nature is unspecified except as required by the two below arguments;

- `START` is a nonnegative exact integer which is less than the maximum index in `U32VECTOR`

- `COUNT` is a nonnegative exact integer where `START` + `COUNT` is less than or equal to the maximum index in `U32VECTOR`

Reading from a port with a `read-bytes-facet` results in the `READER-PROC` being called: it should read the Unicode codepoint values (as integers) of up to `COUNT` characters into `U32VECTOR` (destructively modifying the existing contents) beginning at the index `START` and return the number of characters it actually read as an exact integer. If the port is at the end of the file, it should return 0 and leave `U32VECTOR` untouched.

NOTE: The use of a u32vector is compatible neither with R6RS (which used a string) nor with SRFI 181 (which used a string or a vector of characters and expected portable programs to provide reader procedures which could handle both). R7RS Large is expected to deprecate string mutation and allow it to have very poor performance. A vector of characters is overkill on a 64-bit machine, however, increasing the storage requirement for reading characters by a factor of two, or even more if someone has an implementation where chars are not immediate but heap allocated; and a vector of characters will stress the garbage collector unnecessarily, whereas a u32vector doesn’t need to be scanned for marking.

#### `write-chars-facet` — facet type <br> `(make-write-chars-facet WRITER-PROC)` — procedure <br> `(write-chars-facet? OBJ)` — procedure

`WRITER-PROC` is a procedure of three arguments which is called by the implementation to spit characters into a textual port. The three arguments have the same specification as for `read-chars-facet`.

Writing to a port with a `write-chars-facet` results in the `WRITER-PROC` being called: it should write up to `COUNT` characters from `U32VECTOR`, beginning from the codepoint integer value at index `START`. If `COUNT` is 0, the port is at the end of file. `WRITER-PROC` should return the number of characters written as an exact integer.

NOTE: See above.

### Utility facets

Utility facets add addiitional I/O-related functionality to a port beyond basic reading and writing.

#### `close-input-facet` — facet type <br> `(make-close-input-facet CLOSE-PROC)` — procedure <br> `(close-input-facet? OBJ)` — procedure

`CLOSE-PROC` is a procedure of no arguments. When `close-port` or `close-input-port` is called on a port with a `close-input-facet`, the `CLOSE-PROC` is called to close the port.

IMPLEMENTATION RESPONSBILITIES: An implementation should ensure that only input ports can have a `close-input-facet`.

NOTE: This is incompatible with R6RS and SRFI 181, where bidirectional ports are closed in both directions at once. However, it may be advantageous in some cases to close them at different times, in particular with sockets on POSIX, which are bidirectional file descriptors where `(close-input-port sock)` should call `shutdown(sock_fd, SHUT_RD)`, `(close-output-port sock)` should call `shutdown(sock_fd, SHUT_WR)`, and `(close-port sock)` should call `shutdown(sock_fd, SHUT_RDWR)`.

ISSUE: Perhaps, though, such use cases should simply use two ports.

#### `close-output-facet` — facet type <br> `(make-close-output-facet CLOSE_PROC)` — procedure <br> `(close-output-facet? OBJ)` — procedure

`CLOSE-PROC` is a procedure of no arguments. When `close-port` or `close-output-port` is called on a port with a `close-output-facet`, the `CLOSE-PROC` is called to close the port.

IMPLEMENTATION RESPONSBILITIES: An implementation should ensure that only output ports can have a `close-output-facet`.

NOTE: See the above entry.

#### `port-position-facet` — facet type <br> `(make-port-position-facet GETTER SETTER)` — procedure <br> `(port-position-facet? OBJ)` — procedure

`GETTER` is a procedure of no arguments; `SETTER` is a procedure of one argument, an exact nonnegative integer. When `port-position` is called on a port that has a `port-position-facet`, it returns the value of calling the `GETTER` procedure; when `set-port-position!` is called on a port that has a `port-position-facet`, it calls `SETTER` with the new position that should be set.

NOTE: Technically, R6RS and SRFI 181 allow getters without setters and vice versa. Is this actually useful?

#### `u8-ready?-facet` — facet type <br> `(make-u8-ready?-facet READY?-PROC)` — procedure <br>

`READY?-PROC` is a procedure of no arguments. When `u8-ready?` is called on a port that has a `u8-ready?-facet`, it returns the value of calling the `READY-PROC`.

IMPLEMENTATION RESPONSIBILITIES: An implementation should ensure that only binary input ports can have a `u8-ready?-facet`.

NOTE: This is not compatible with R6RS and SRFI 181, which simply do not provide a way to check `u8-ready?` on a custom port. A portable implementation of this SRFI will have to re-export its own version of `u8-ready?`.

#### `char-ready?-facet` — facet type <br> `(make-char-ready?-facet)`

`READY?-PROC` is a procedure of no arguments. When `char-ready?` is called on a port that has a `char-ready?-facet`, it returns the value of calling the `READY-PROC`.

IMPLEMENTATION RESPONSIBILITIES: An implementation should ensure that only textual input ports can have a `char-ready?-facet`.

NOTE: The same consideration applies to implementations atop R6RS/SRFI 181 as for `u8-ready?-facet`.

ISSUE: R6RS provides no facilities for flushing of output ports; SRFI 181 does. Was SRFI 181 right, or was R6RS? Should we have a `flush-output-facet`?

### Informative facets

Informative facets usually contain information about the underlying operating system resources behind a port. They do not provide additional I/O functionality.

#### `file-name-facet` — facet type <br> `(make-file-name-facet FILE-NAME)` — procedure <br> `(file-name-facet? OBJ)` — procedure <br> `(port-file-name PORT)` — procedure

This facet allows the name of the file to be retrieved from a port object which is reading from and/or writing to that file. `FILE-NAME` must be a string. The `port-file-name` procedure, called on a port which has a `file-name-facet` facet, returns this string.

NOTE: This corresponds to the `ID` argument to the procedures to make custom ports in R6RS and SRFI 181. In implementations built on top of R6RS, the `ID` will be set to the empty string if there is no `file-name-facet`. In implementations built on top of SRFI 181, the `ID` will be set to `#f` in this case. (R6RS requires that the `ID` be a string; SRFI 181 allows it to be any arbitrary object. Neither provides a means of getting the file name back out of a port in any case, so the trick suggested in the Implementation section will be needed to implement the `port-file-name` procedure.)

#### `file-descriptor-facet` — facet type <br> `(make-file-descriptor-facet FD)` — procedure <br> `(file-descriptor-facet? OBJ)` — procedure <br> `(port-file-descriptor PORT)` — procedure

On operating systems where open files, pipes, sockets, etc. are represented by integer file descriptors, this facet allows the number of the file descriptor to be retrieved from the port. `FD` must be an exact nonnegative integer. The `port-file-descriptor` procedure, called on a port which has a `file-descriptor-facet`, returns this integer.

NOTE: An important rule for programs in high-level languages such as Scheme is ‘don’t mess with file descriptors you aren’t given’. If you are writing a program according to a certain protocol (e.g. [UCSPI](https://cr.yp.to/proto/ucspi.txt)) which you know gives you certain ports at startup, you can use those; you have been given them, and you can convert them to ports with e.g. `fd->port` from [SRFI 170](https://srfi.schemers.org/srfi-170/srfi-170.html). But once a port has been opened, a Scheme implementation will feel that it has full control over buffering output to it, getting and setting the port position, etc., and will not want or expect anything else to go messing with the file descriptor underlying it. There may be a limited set of operations which it would make sense to do through an FFI on such a file descriptor (e.g. `fstat(2)`, `isatty(3)`), but in general a file descriptor, once wrapped in a Scheme port object, should only be accessed through that port, to prevent undefined behaviour.

#### `transcoder-facet` — facet type <br> `(make-transcoder-facet TRANSCODER)` — procedure <br> `(transcoder-facet? OBJ)` — procedure <br> `(port-transcoder PORT)` — procedure

On transcoded ports, this facet allows the transcoder to be retrieved from the port. `TRANSCODER` must be a transcoder. The `port-transcoder` procedure, called on a port which has a `transcoder-facet`, returns this transcoder.

### Defining new facet types

#### `(define-port-facet-type <<facet type name>> <<constructor id>> <<predicate id>> <<field accessor name>> ...)` — syntax

SYNTAX: All subforms are identifiers.

SEMANTICS: Defines a new facet type whose record type descriptor is bound to `<<facet type name>>`, whose constructor (which takes as many arguments as there are `<<field accessor name>>`s) is bound to `<<constructor id>>`, and whose type test predicate is bound to `<<predicate id>>`.

The `<<field accessor name>>`s are bound to a series of procedures which, when called on a port which has a facet of the newly-defined type, will access the corresponding field value which was passed to the constructor of the facet.

EXAMPLE: `transcoder-facet` could be defined as follows:

```scheme
(define-port-facet-type transcoder-facet
  make-transcoder-facet transcoder-facet?
  port-transcoder)
```

ISSUE: Most fields in port facets have a type restriction. How is this enforced?

## Implementation

Except for the minor problems noted in some entries, it is possible to implement this proposal on top of R6RS or SRFI 181. Informative facets which are not part of the specification of custom ports in R6RS/SRFI 181 can be implemented by storing them in a hash table (weak-keyed, ideally, so that ports are still collectable).

## Acknowledgements

Dimitris Vyzovitis’s design for sources and sinks in Gerbil was an inspiration for this proposal. Jani Juhani Sinervo’s port properties proposal also inspired the inclusion of the informative facets.
