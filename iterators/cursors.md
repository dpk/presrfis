# Iteration Cursors

Iteration cursors are intended as an alternative to (and hopefully replacement for) the imperative generators of [SRFI 158][srfi-158]. Unlike generators, cursors support a purely-functional as well as a stateful mode, and the decision whether to traverse a sequence functionally or imperatively is in the hands of the programmer consuming the sequence. Also, sequences iterated over using cursors can contain any Scheme datum, unlike SRFI 158 generators which cannot contain the standard EOF object because it is used as a sentinel.

Both creating and traversing a cursor can be done in a very similar manner to traversing a Scheme list, making the idiom familiar to Scheme programmers.

[srfi-158]: https://srfi.schemers.org/srfi-158/srfi-158.html

## Concepts

A <dfn>cursor</dfn> is an object of disjoint type. A cursor represents a <dfn>place</dfn> in a potentially-infinite <dfn>sequence</dfn>. If the sequence is finite, the cursor and its place may be at the <dfn>end</dfn> of the sequence; iff not, the place refers to the cursor’s current <dfn>value</dfn>.

## Primitive Producers

### `(make-cursor advance-proc initial-value state-var ...)` (Procedure)

Returns a new cursor, not at the end of its sequence, with `initial-value` as its initial value.

When the cursor is advanced with `cursor-next` or `cursor-next!`, the procedure `advance-proc` is called with a procedure `next-proc` as its first argument, and the `state-var`s as its subsequent argument. The `advance-proc` should call `next-proc` exactly once and return its return value. If `next-proc` is called with no arguments, the cursor reaches the end of its sequence at the advancement. Otherwise, it must be called with the new value of the cursor as its first argument, plus as many values as there are `state-var`s — these become the `state-var`s that will be used on the next iteration of the cursor.

### `(make-end-cursor)` (Procedure)

Return a cursor which is already at the end of its sequence.

## Convenience Syntax

### `cursor`

*Syntax:* `(cursor name ((state-var initial-value) ...) body ...)`

`name` and all the `state-vars` must be identifiers.

*Semantics:* Returns a cursor which it creates as follows:

`cursor` creates an `advance-proc` procedure whose first formal argument is named by `name` and whose remaining formals are the given `state-var`s and whose body is the given `body`. It then calls it with the `initial-value`s and a `next-proc` which will either call `make-cursor` if called with an initial value for the cursor and new values for the `state-vars`, or else `make-end-cursor` if called with no arguments.

This allows iteration cursors to be created in a manner familiar to Scheme programmers from ‘named `let`’. For examples, see the implementations of `list->cursor` and `vector->cursor`.

## Handy Built-in Producers

### `(list->cursor ls)`

Return a cursor iterator for the list `ls`.

### `(vector->cursor vec)`

Return a cursor iterator for the vector `vec`.

## Consumer Interface

### `(cursor-end? c)` (Procedure)

Returns `#t` if the cursor `c` is at the end of its sequence, or `#f` otherwise.

### `(cursor-head c)` (Procedure)

Returns the value of the cursor at its current place. It is an error if the sequence is exhausted.

### `(cursor-next c)` (Procedure)

Return a freshly allocated cursor which is `c` advanced by one place in the sequence. It is an error if the sequence is exhausted.

### `(cursor-next! c)` (Procedure)

Mutates `c` to point to the next place in the sequence, returning `c`. It is an error if the sequence is exhausted.
