(define *record-password* (cons '*record-access* '()))

(define-syntax define-applicable-record-type
  (syntax-rules ()
    ((_ type-name
        (constructor-name field-name ...)
        predicate-name
        (field-name* accessor-name . more) ...
        proc-spec)
     (begin
       (define proc proc-spec)
       (define (constructor-name field-name ...)
         (letrec* ((inst
                    (lambda args
                      (if (and (not (null? args))
                               (eq? (car args) *record-password*))
                          ;; record accessed
                          (apply (case-lambda
                                  ((access-name)
                                   (case access-name
                                     ((field-name) field-name) ...))
                                  ((set-name set-value)
                                   (case set-name
                                     ((field-name)
                                      (set! field-name set-value)) ...)))
                                 (cdr args))
                          ;; record applied rather than accessed
                          (apply (proc inst) args)))))
           inst))
       (define-accessors field-name* accessor-name . more) ...))))

(define-syntax define-accessors
  (syntax-rules ()
    ((_ field-name accessor-name)
     (define (accessor-name record)
       (record *record-password* 'field-name)))
    ((_ field-name accessor-name setter-name)
     (begin
       (define (accessor-name record)
         (record *record-password* 'field-name))
       (define (setter-name record new-value)
         (record *record-password* 'field-name new-value))))))
