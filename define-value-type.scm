;; `define-value-type' is like (scheme base) `define-record-type',
;; except all fields are immutable and the ‘mutators’, if specified,
;; are actually functional field updaters which return a new record
;; with the given field updated to the new value.

(define-syntax define-value-type
  (syntax-rules ()
    ((_ name (constructor field-name_0 ...) pred
        (field-name_1 accessor-name . maybe-updater-name) ...)
     (begin
       (define-record-type name
         (constructor field-name_0 ...) pred
         (field-name_1 accessor-name) ...)
       (define-next-updater
         constructor ()
         (accessor-name . maybe-updater-name) ...)))))

(define-syntax define-next-updater
  (syntax-rules ()
    ((_ _ _)
     (begin))
    ((_ constructor
        (previous-accessor ...)
        (accessor-name updater-name)
        (more-accessor-name . more-maybe-updater-name) ...)
     (begin
       (define (updater-name record new-field-value)
         (constructor (previous-accessor record) ...
                      new-field-value
                      (more-accessor-name record) ...))
       (define-next-updater
         constructor (previous-accessor ... accessor-name)
         (more-accessor-name . more-maybe-updater-name) ...)))
    ((_ constructor
        (previous-accessor ...)
        (accessor-name)
        (more-accessor-name . more-maybe-updater-name) ...)
     (define-next-updater
       constructor (previous-accessor ... accessor-name)
       (more-accessor-name . more-maybe-updater-name) ...))))

