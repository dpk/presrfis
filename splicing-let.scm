;; Only works in hypothetical Schemes which allow intermingling of
;; definitions and expressions.

(define-syntax splicing-let
  (lambda (stx)
    (syntax-case stx ()
      ((_ ((var expr) ...) expr-or-def ...)
       (with-syntax (((real-var ...) (generate-temporaries #'(var ...)))
                     ((temp-var ...) (generate-temporaries #'(var ...))))
         #'(begin
             (define real-var (void)) ...
             (let ((temp-var expr) ...)
               (set! real-var expr) ...)
             (splicing-let-syntax
                 ((var
                   (identifier-syntax
                     (_ real-var)
                     ((set! _ new-expr) (set! real-var new-expr)))) ...)
               expr-or-def ...)))))))
