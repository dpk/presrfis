# SRFI xxx: Low-level semantics and operators for procedures taking named arguments

## Abstract

Low-level mechanisms to create and receive keyword arguments are defined. Keyword arguments are defined as semantically completely distinct from positional arguments, rather than overloading property list semantics onto run-time procedure argument lists; and the names of keyword arguments are not expressions on their own, but must appear in procedure call expressions in proper name-value order and are then given special semantics in the underlying procedure call mechanism.

## Rationale

For the rationale of keyword arguments in general, see SRFI 89.

As to this particular design, the semantics of keywords not being expressions on their own but being treated specially within procedure call expressions was introduced by Kawa and Racket, and offers the advantage that a keyword datum can never accidentally be interpreted at run time as a positional argument when it was intended to introduce a named argument, or vice versa. The following example from Common Lisp demonstrates that this can be a problem:

```lisp
(defun broken (&optional (a 1) (b 2) &key (c 3) (d 4))
  (list a b c d))

(broken :c 1 :d 2) ; (1 2 1 2) was probably expected, but this
                   ; evaluates to (:c 1 3 2)
```

The same semantic problem is shared by SRFI 89 as well as DSSSL and most of the derived implementations of keywords in various Scheme implementations.

This specific design for the underlying mechanism of keyword arguments, based on that of Racket, is designed to be easily adoptable by existing Scheme implementations, by means of slight changes in the macro expander and the addition of an extra slot in the representation of procedure objects. No other changes, for example to the calling convention used in the implementation’s compiler, are required. An implementation strategy for existing Scheme implementations is suggested below.

Compared to Racket’s original implementation of keyword arguments, this specification maintains the order in which keywords were passed to the procedure, which is useful when implementing foreign-function interfaces to certain other languages, such as Objective-C, Swift, and Smalltalk.

## Concepts

### Keyword datums

Keyword datums are Scheme values with their own lexical syntax, but they are not expressions. It is an error to evaluate them, in the same way that it is an error to evaluate the empty list. Like symbols, they have a name and are interned.

### Keyword list

A keyword list is a type, not necessarily disjoint, which contains an ordered (i.e. remembering the order of its entries) mapping of keyword datums to values for a particular procedure call.

It is *not* an error at the low level for a keyword list to contain the same keyword datum more than once. (A high level interface may enforce the restriction that only one keyword with a given name is allowed to be passed to a procedure.)

### Keyword procedures

A keyword procedure in this system is actually a compound of two procedures, a regular *positional procedure* and a *keyword specializer*. When a procedure is called without any keyword arguments at all, the positional procedure is called with the given operands, like a normal Scheme procedure. When a procedure with keyword arguments is called, the keyword specializer is first called on an keyword list of the keyword arguments alone, in the order they were passed to the procedure. This returns a second procedure, which is called with the remaining, positional arguments.

A procedure which takes keyword arguments can be conceptualized as a regular Scheme procedure which has been internally extended to have an extra slot pointing to a second positional procedure (the specializer). This is likely the most efficient possible implementation.

## Extensions to the lexical syntax of R7RS small

The R7RS small grammar is extended with the following additional rule:

> ⟨keyword datum⟩ → `#:` ⟨identifier⟩

The R7RS small definition of ⟨simple datum⟩ is extended the additional option ‘| ⟨keyword datum⟩’.

The R7RS small definition of ⟨operand⟩ is substituted by:

> ⟨operand⟩ → ⟨positional operand⟩ |  
>   ⟨named operand⟩
>
> ⟨positional operand⟩ → ⟨expression⟩
> 
> ⟨named operand⟩ → ⟨keyword datum⟩ ⟨expression⟩

<i>Rationale:</i> While it is unlikely that good style for using keyword arguments will ever encompass mixing named and positional operands in arbitrary order, experience has shown that, while it is most natural to place named arguments at the end of an argument list in the majority of cases, the ability to place keyword arguments at the start of the arguments list is useful and intuitive for procedures which receive keyword arguments plus rest arguments, especially in alternative Scheme notations like Scribble. Since named arguments are completely semantically distinct from positional arguments, there is no reason in this system to restrict a procedure to having keyword arguments in one particular place, thus they may be mixed arbitrarily.

## Operators

### Keyword datums

#### `(keyword? obj)` (procedure)

As in SRFI 88.

#### `(string->keyword str)` (procedure)

As in SRFI 88.

#### `(keyword->string str)` (procedure)

As in SRFI 88.

### Keyword lists

#### `(alist->keyword-list alist)` (procedure)

Convert the given association list (with keyword datums in the cars of its members and the associated values in the cdrs) into a keyword list.

<i>Rationale:</i> This procedure is necessary so that procedures with keyword arguments can have those arguments supplied dynamically in the manner of `apply`.

#### `(keywords-fold kons knil kw-list)` (procedure)

The fundamental iterator for the keyword list type. For each entry in the `kw-list` from left to right, the `kons` procedure is iteratively called with the args `acc key val`, where `acc` is `knil` on the first call and the result of the previous call, `key` is a keyword datum and `val` is the value associated with that datum.

No `keyword-list?` procedure is provided because there is no guarantee that keyword lists are disjoint, nor that they will have any particular form if they are not. I.e. a valid implementation can use an association list as its keyword list type (in which case the above procedure `alist->keyword-list` may be an identity function).

### Keyword procedures

#### `(make-keyword-procedure positional-procedure specializer)` (procedure)

Return a new keyword procedure with the given `positional-procedure` and `specializer`.

#### `(keyword-specialize kw-proc kw-args)` (procedure)

Call the specializer of the given `kw-proc` with the single argument `kw-args`, which should be a keyword list, returning the result of the specializer.

#### `(keyword-procedure? obj)` (procedure)

Return `#t` if the `obj` is a keyword procedure (i.e. if it has a keyword specializer and can be used with `keyword-specialize`).

## Example

The following two Scheme expressions are semantically equivalent.

```scheme
(f 1 2 #:key 3)

((keyword-specialize f (alist->keyword-list '((#:key . 3)))) 1 2)
```

Given this Scheme procedure:

```scheme
(define keyword-proc-test
  (make-keyword-procedure
   (lambda pos-args
     (cons 'called-with-positionals: pos-args))
   (lambda (kws)
     (lambda pos-args
       (list
        'called-with-keywords:
        (keywords-fold
         (lambda (acc key val)
           (cons* key val acc))
         '()
         kws)
        'and-positionals:
        pos-args)))))
```

invocations evaluate as follows:

```scheme
(keyword-proc-test 1 2 3)
;;=> (called-with-positionals: 1 2 3)

(keyword-proc-test #:a 1 #:b 2)
;;=> (called-with-keywords: (#:a 1 #:b 2) and-positionals: ())

(keyword-proc-test 0 #:a 1 2 #:b 3 4)
;;=> (called-with-keywords: (#:a 1 #:b 3) and-positionals: (0 2 4))

(define pre-specialized
  (keyword-specialize
   keyword-proc-test
   (alist->keyword-list '((#:foo . bar) (#:fred . 2)))))

(pre-specialized 'alpha 'beta)
;;=> (called-with-keywords: (#:foo bar #:fred 2) and-positionals: (alpha beta))
```

The following is an example (but a toy example only) of a high-level form for creating procedures with keyword arguments. Compared to an ideal such form (which might resemble the `lambda*` and `define*` of SRFI 89): the syntax for giving formals is less than ideal (but easy to parse with `syntax-rules`); there is no explicit provision for creating a procedure with required keyword arguments; default values for keyword arguments cannot depend on the values of other arguments; there is no protection against providing the same keyword name more than once, or against providing a keyword name which is not understood by the procedure; and keyword binding performance is quadratic in the number of keywords. None of these issues are intrinsic to the low-level mechanism described in this SRFI.

```scheme
(define *keyword-not-given* (cons '*keyword-not-given* '()))
(define (keyword-ref kw-list kw default-thunk)
  (let ((val (keywords-fold
              (lambda (acc key val)
                (if (eq? key kw) val acc))
              *keyword-not-given*
              kw-list)))
    (if (eq? val *keyword-not-given*)
        (default-thunk)
        val)))

;; SYNTAX:
;; (kw-lambda ⟨formals⟩ ⟨keyword formals⟩ ⟨body⟩)
;;
;; ⟨formals⟩ and ⟨body⟩ as in regular lambda
;; ⟨keyword formals⟩ → ((⟨keyword datum⟩ ⟨identifier⟩ ⟨default value⟩) ...)
;; ⟨default value⟩ → ⟨expression⟩
(define-syntax kw-lambda
  (syntax-rules ()
    ((_ pos-formals ((formal-kw formal-var default) ...) . body)
     (let ((pos-proc
            (lambda (formal-var ... . pos-formals) . body)))
       (make-keyword-procedure
        (lambda pos-args
          (apply pos-proc default ... pos-args))
        (lambda kws
          (let ((formal-var
                 (keyword-ref kws 'formal-kw (lambda () default)))
                ...)
            (lambda pos-args
              (apply pos-proc formal-var ... pos-args)))))))))

(define h
  (kw-lambda (a . r) ((#:key k #f))
    (list a k r)))

(h 7)
;;=> (7 #f ())
(h 7 8 9 10)
;;=> (7 #f (8 9 10))
(h 7 #:key 8 9 10)
;;=> (7 8 (9 10))
```

## Implementation

I intend to maintain a branch of Chibi implementing this spec (although without the expectation it will ever be merged, at least not unless this spec becomes part of R7RS and Alex decides to adopt it then). An implementation integrating with Kawa’s existing keyword procedure system is also intended.

### An implementation strategy for existing Scheme implementations

An extra slot is added to the internal representation of procedure objects for the keyword specializer. `keyword-specialize` is a primitive procedure which accesses this slot, if it has anything in it.

When the macro expander encounters a ⟨procedure call⟩ expression containing any ⟨named operand⟩s, it gathers the keyword objects and expressions within that procedure call and first creates an expression that will generate an keyword list mapping the keywords to the values of the expressions they correspond to. It then hygienically replaces the original ⟨procedure call⟩ expression with the equivalent of an expression `((keyword-specialize ⟨operator⟩ ⟨keyword list expression⟩) ⟨positional operand⟩)`, with the same ⟨operator⟩ and the same ⟨positional operand⟩s in the same order as in the original ⟨procedure call⟩ expression. It creates this expression hygienically, so that `keyword-specialize` always refers to the procedure of that name specified here, and any other references in the ⟨keyword list expression⟩ likewise cannot be overriden.

### Accommodating Scheme implementations which already have SRFI 88/DSSSL-style keywords which are self-evaluating expressions

According to this specification it is merely an error to evaluate a keyword object, not a guaranteed error-is-signalled situation. This means an implementation where they are self-evaluating is conformant, even though it is recommended that new implementations should signal a compile error.

However, this specification requires that all procedure call expressions which appear to contain valid key-value pairs be transformed into an appropriate call to `keyword-specialize`. This poses a problem for interaction with existing code which expects to be able to receive keyword datums as ordinary Scheme values in its argument list.

However, because there is no guarantee on what form a keyword list value has, an implementation can use this form to recall not only what key-value mappings existed in the original procedure call expression, but also where (i.e. in what positions) they appeared.

A conforming backwards-compatible implementation of `keyword-specialize` does what it usually does on procedures created with `make-keyword-procedure`, but on all other procedures it returns a procedure which calls that procedure with the keyword datums and associated values put back in their proper places as if nothing had happened.

This is a less than ideal compatibility solution in terms of performance, but it allows implementations to mostly maintain the old DSSSL-style semantics they may have had. There will probably, however, be an effect on the order of argument evaluation, potentially affecting implementation-specific code which depends on that implementation’s own order of argument evaluation.
