# A (hopefully) robust way to simulate a current directory in each thread

## Help wanted!

Are you a POSIX wizard? Can you see any major flaws in the idea suggested below to solve the given problem? Please let me know!

## The problem

The name of the problem is somewhat misleading, but only somewhat: the goal is to be able to make the current working directory part of the dynamic environment of Scheme programs in a way consistent with (but not necessarily implemented by) parameter objects.

In other words, it should be possible to do something like this:

```scheme
(with-current-directory "/home/dpk/bandersnatch"
  ;; lots more stuff happens here ...
  (open-input-file "./frumiousness.txt")
  ;; more stuff happens here
  )
```

and the `.` in the path of `frumiousness.txt` should *robustly* refer to a file in the `/home/dpk/bandersnatch` directory, even if it that directory has been moved by another process or deleted between the start of the `with-current-directory` block and the point where `open-input-file` wants a file relative to `.`.

This is easy enough to maintain in a single-threaded environment, but in a multi-threaded environment it’s tricky because the current directory is global per process. This applies obviously with green threads, but POSIX stupidly made the current directory a global setting even in the presence of its own threading system.

## A solution

The implementation maintains a global current directory mutex, plus a parameter (= a thread-safe dynamically scoped variable) for the operational current directory. This parameter is not exposed to user code, it’s purely an implementation detail.

When `with-current-directory` is invoked, the implementation does the following:

1. Acquire the global current directory lock.
2. `chdir` to the path given to be our new current directory. (If this or any other system call causes an error, the lock is released and a condition is signalled.)
3. `open(".", O_RDONLY)` is called and the resulting file descriptor is stored inside an opaque handle object.
4. Release the global current directory lock.
5. The operational current directory is parameterized to that new opaque handle object throughout the `with-current-directory` block.

(A more efficient equivalent, on Linux only, would be to `open` the directory with `O_DIRECTORY` to get a file descriptor. Due to the use of potentially relative pathnames, this doesn’t relieve one of the necessity to acquire the global current directory lock.)

Many operations can then use the `at` variants of the relevant system calls (e.g. `openat` instead of `open`), using the file descriptor in the parameterized handle object to simulate the call being done with that current directory.

Other operations with no `at` variant (`rmdir` and `posix_spawn` are the main ones I’ve found so far) have to do this:

1. Acquire the global current directory lock.
2. `fchdir` to the file descriptor contained within the handle object within the parameter for the operational current directory.
3. Do the requested file operation.
4. Release the global current directory lock.

In fact, `with-current-directory` has to do something like this as well to handle the case of one relative directory change within another relative directory change.

You might think the file descriptor inside the opaque handle object can safely be closed when control flow exits the `with-current-directory` block, so we avoid leaking fds. Not so fast! The presence of first-class continuations of unlimited extent means we can’t do this. The reason we stored the file descriptor inside a handle object, instead of just setting the parameter directly to the fd number, is so we can attach a finalizer to that object which closes the file descriptor. Only when the garbage collector finds that this handle is no longer held by any captured continuation is it safe to release the fd back to the operating system. Depending on the garbage collector to close open files is generally considered a bad idea, but I think in Scheme it’s probably the only option for cases like this.

## Problems

This means that certain file operations can only be done effectively single-threaded, because the current-directory lock is global. The most common operations are able to avoid this thanks to the `at` variants of the system calls. In theory, an implementation could detect when a pathname used for opening a file or similar is relative and only acquire the lock when it is. This is potentially a security nightmare, because doing pathname processing in user space is always a security nightmare.

FFI functions depending on the current directory will have a fun time dealing with this system. Any FFI will probably need to be extended so that calls which need to acquire the global directory lock and issue an actual `chdir` can be appropriately wrapped.

What if there are symbolic links in the pathname given to `with-current-directory`? I think in this case this scheme will behave differently to POSIX, but I’m not sure. Needs testing.

This solution as specified is not workable for a similar problem with umasks, but it might be possible to solve that with continuation marks.

## Acknowledgements

John Cowan pointed out the problem with the naive approach, and Jani Juhani Sinervo came up with the outline of the solution. I worked out the details.
