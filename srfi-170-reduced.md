# Basic operating system interface (reduced from SRFI 170)

This attempts to provide only those APIs which are likely to be fairly widely supported and/or are easy to completely emulate even on non-POSIX OSes. It fully specifies the behaviour in terms of POSIX but implementations on non-POSIX OSes should be able to emulate that behaviour exactly.

Buffering in this spec is implementation-specific. In order to guarantee data has been written to a file and empty the local buffers, use of the R7RS small procedure `flush-output-port` is required. There is no way to control how much an input port is buffered.

## Procedures unchanged from SRFI 170

* `create-directory`
* `rename-file` (must be an atomic operation under the same constraints as on POSIX systems)
* `delete-directory` (must fail on non-empty directories, but on the off chance a non-POSIX OS does not allow this atomically, it’s okay for the Scheme implementation to check first, then delete anyway even if entries were added between the check and the deletion)
* `truncate-file`
* `open-directory`
* `read-directory`
* `close-directory`
* `set-environment-variable!`
* `delete-environment-variable!`
* `terminal?` (implies nothing more than that it may make sense to send ANSI control signals to the port; an implementation which always returns `#f` is allowed, if this concept doesn’t make sense locally)

## Procedures with only minor changes from SRFI 170

The <var>dotfiles?</var> argument is deleted from `directory-files` because <var>dotfiles?</var> is relevant only to a convention used on POSIX systems.

The `temp-file-prefix` parameter is provided, but no initial particular value is required — the implementation should set it to some OS-specific location for temporary files. This of course affects the following procedures implicitly:

* `create-temp-file`
* `call-with-temporary-filename`

## Other procedures etc.

### `(open-file fname port-type [write-mode])` (Procedure)

Returns an port as in the SRFI 170 version of this procedure. `port-type` is a symbol, one of the names of the constants in SRFI 170, and `write-mode` is one of the symbols `truncate`, `append`, `exclusive`, or `#f`.

### `current-directory` (Parameter)

A parameter object containing the full path of the current working directory of the Scheme process. Unlike SRFI 170, this is required to be emulated as thread-specific and indeed specific to a dynamic environment, as it is set by `parameterize` and not by a separate procedure.

### `(directory-exists? fname)` (Procedure)

Return `#t` if the given `fname` exists in the file system and is a directory. (Rationale: This procedure name is longer than `directory?` but matches R7RS small `file-exists?`)

### `(file-access-time fname)` (Procedure)

Returns a date-time object (of a type yet to be specified and agreed upon) containing the last access time of the file at `fname`. Signals an error if `fname` does not exist or is on a file system or operating system which does not store access times.

### `(file-modification-time fname)` (Procedure)

Returns a date-time object (of a type yet to be specified and agreed upon) containing the last modification time of the file at `fname`. Signals an error if `fname` does not exist or is on a file system or operating system which does not store modification times.

### `(file-status-change-time fname)` (Procedure)

Returns a date-time object (of a type yet to be specified and agreed upon) containing the last change of status time of the file at `fname`. Signals an error if `fname` does not exist or is on a file system or operating system which does not store change of status times.

### `(file-creation-time fname)` (Procedure)

Returns a date-time object (of a type yet to be specified and agreed upon) containing the creation time of the file at `fname`. Signals an error if `fname` does not exist or is on a file system or operating system which does not store creation times.

### `(file-size fname)` (Procedure)

Return the size in bytes of the file at `fname`. Signals an error if `fname` does not exist.

### `(file-readable? fname)` (Procedure)

Returns `#t` if the current Scheme process has permission to read the file at `fname` or `#f` otherwise. Signals an error if `fname` does not exist.

### `(file-writable? fname)` (Procedure)

Returns `#t` if the current Scheme process has permission to write to the file at `fname` or `#f` otherwise. Signals an error if `fname` does not exist.

## Permitted derogations etc.

An implementation may signal a compile error on a program containing calls to any of `file-access-time`, `file-modification-time`, `file-status-change-time`, and `file-creation-time` if it cannot under any circumstances return answers to them, rather than waiting until runtime.
