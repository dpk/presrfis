;;; Vector utility functions

;; Last item in vec
(define (vector-last vec)
  (vector-ref vec (- (vector-length vec) 1)))

;; Copy of vec with the element at idx replaced by val.
(define (vector-with vec idx val)
  (let ((out (vector-copy vec)))
    (vector-set! out idx val)
    out))

;; Copy of vec with the last element replaced by val.
(define (vector-with-last vec val)
  (vector-with vec (- (vector-length vec) 1) val))

;; Copy of vec with val as an extra element at the end.
(define (vector-add-right vec val)
  (let ((out (make-vector (+ 1 (vector-length vec)))))
    (vector-set! out (vector-length vec) val)
    (vector-copy! out 0 vec)
    out))

(define (vector-remove-right vec)
  (vector-copy vec 0 (- (vector-length vec) 1)))

;; Divide vec into segments of at most max-size elements.
(define (vector-segments vec max-size)
  (let ((out (make-vector (ceiling-quotient (vector-length vec) max-size))))
    (let loop ((in-idx 0)
               (out-idx 0))
      (if (>= in-idx (vector-length vec)) out
          (begin
            (vector-set! out out-idx
                         (vector-copy vec in-idx
                                      (min (+ in-idx max-size)
                                           (vector-length vec))))
            (loop (+ in-idx max-size) (+ out-idx 1)))))))


;;; Miscellaneous utility functions

;; Return the result of applying the unary procedure op to arg n times
;; accumulatively.
(define (repeatedly n op arg)
  (if (= n 0) arg
      (repeatedly (- n 1) op (op arg))))

;; From (scheme division) -- this implementation may not be correct
;; for negative division.
(define (ceiling-quotient n d)
  (let-values (((q r) (floor/ n d)))
    (if (= r 0) q
        (+ q 1))))
