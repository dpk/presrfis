;; -*- geiser-scheme-implementation: guile -*-

#; ;; Development environment setup
(cond-expand
 (guile
  (install-r7rs!)
  (import (scheme base))
  (load "util.scm")))

;; This value is only suitable for debugging. Clojure’s value is 32,
;; for reference. Access to the vector is O(log_x n) where x is this
;; number.
(define *pvector-branching-factor* 4)

(define-record-type PVector
  (%make-pvector depth root)
  pvector?
  (depth pvector-depth)
  (root pvector-root pvector-root-set!))

(define (make-empty-pvector)
  (%make-pvector 0 #()))

(define-syntax traverse
  (syntax-rules ()
    ((_ pv loop (node level) body ...)
     (let loop* ((node (pvector-root pv))
                 (level 0))
       (define (loop branch)
         (cond ((vector? branch)
                (loop* branch (+ level 1)))
               ((exact-integer? branch)
                (loop* (vector-ref node branch) (+ level 1)))
               ((eq? branch 'first)
                (loop* (vector-ref node 0) (+ level 1)))
               ((eq? branch 'last)
                (loop* (vector-last node) (+ level 1)))))
       body ...))))

(define (pvector-branch-for level depth idx)
  (modulo
   (floor-quotient
    idx
    (expt *pvector-branching-factor* (- depth level)))
   *pvector-branching-factor*))

(define (pvector-node-for pv idx)
  (traverse pv down (node level)
    (if (= level (pvector-depth pv))
        node
        (down (pvector-branch-for level (pvector-depth pv) idx)))))

(define (pvector-tail-node pv)
  (traverse pv down (node level)
    (if (= level (pvector-depth pv))
        node
        (down 'last))))

(define (pvector-tail-node-set! pv new-tail)
  (if (= (pvector-depth pv) 0)
      (pvector-root-set! pv new-tail)
      (traverse pv down (node level)
        (if (= level (- (pvector-depth pv) 1))
            (vector-set! node (- (vector-length node) 1) new-tail)
            (down 'last)))))

(define (pvector-tail-to-root f pv)
  (traverse pv down (node level)
    (if (< level (pvector-depth pv))
        (down 'last))
    (f level node)))

(define (pvector-insertion-level pv)
  (call/cc
   (lambda (return)
     (pvector-tail-to-root
      (lambda (level node)
        (cond ((< (vector-length node) *pvector-branching-factor*)
               (return level))
              ((and (= level 0)
                    (= (vector-length node) *pvector-branching-factor*))
               #f)))
      pv))))

(define (pvector-add-right pv val)
  (cond ((pvector-insertion-level pv)
         => (lambda (ins-level)
              (let ((new-tail
                     (if (= ins-level (pvector-depth pv))
                         (vector-add-right (pvector-tail-node pv) val)
                         (vector val))))
              (%make-pvector
               (pvector-depth pv)
               (traverse pv down (node level)
                 (cond ((= level (pvector-depth pv)) new-tail)
                       ((< level ins-level)
                        (vector-with-last node (down 'last)))
                       ((>= level ins-level)
                        (vector-add-right node (down 'last)))))))))
        (else
         (%make-pvector (+ (pvector-depth pv) 1)
                        (vector (pvector-root pv)
                                (repeatedly (pvector-depth pv)
                                            vector
                                            (vector val)))))))

(define (pvector-add-right! pv val)
  (let ((tail-node (pvector-tail-node pv)))
    (if (< (vector-length tail-node) *pvector-branching-factor*)
        (begin
          (pvector-tail-node-set! pv (vector-add-right tail-node val))
          pv)
        (pvector-add-right pv val))))

(define (%make-pvector/normalized depth root)
  ;; After removal, it’s possible to end up with a dud root node with
  ;; only one branch, which could itself be the root.
  (if (and (> depth 0)
           (= (vector-length root) 1))
      (%make-pvector (- depth 1) (vector-ref root 0))
      (%make-pvector depth root)))

(define (pvector-remove-right pv)
  (%make-pvector/normalized
   (pvector-depth pv)
   (if (and (= (vector-length (pvector-tail-node pv)) 1)
            (> (pvector-depth pv) 0))
       (traverse pv down (node level)
         (if (or (= level (- (pvector-depth pv) 1))
                 (= (vector-length (vector-last node)) 1))
             (vector-remove-right node)
             (vector-with-last node (down 'last))))
       (traverse pv down (node level)
         (if (= level (pvector-depth pv))
             (vector-remove-right node)
             (vector-with-last node (down 'last)))))))

(define (pvector-remove-right! pv)
  (let ((tail-node (pvector-tail-node pv)))
    (if (> (vector-length tail-node) 1)
        (begin
          (pvector-tail-node-set! pv (vector-remove-right tail-node))
          pv)
        (pvector-remove-right pv))))

(define (pvector-set pv idx val)
  (%make-pvector
   (pvector-depth pv)
   (traverse pv down (node level)
     (if (= level (pvector-depth pv))
         (vector-with node (modulo idx *pvector-branching-factor*) val)
         (let ((branch-idx
                (pvector-branch-for level (pvector-depth pv) idx)))
           (vector-with node branch-idx (down branch-idx)))))))

(define (vector->pvector vec)
  (let loop ((depth 0)
             (nodes vec))
    (if (> (vector-length nodes) *pvector-branching-factor*)
        (loop (+ depth 1) (vector-segments nodes *pvector-branching-factor*))
        (%make-pvector depth nodes))))

(define (list->pvector ls) (vector->pvector (list->vector ls)))
(define (pvector . vals) (list->pvector vals))

;; This could be cached within the PVector itself.
(define (pvector-length pv)
  (let loop ((neglevel (pvector-depth pv))
             (node (pvector-root pv))
             (count 0))
    (if (= neglevel 0) (+ count (vector-length node))
        (loop (- neglevel 1)
              (vector-ref node (- (vector-length node) 1))
              (+ count
                 (* (- (vector-length node) 1)
                    (expt *pvector-branching-factor* neglevel)))))))

(define (pvector-ref pv idx)
  (vector-ref (pvector-node-for pv idx)
              (modulo idx *pvector-branching-factor*)))

(define (pvector-set!! pv idx val)
  (vector-set! (pvector-node-for pv idx)
               (modulo idx *pvector-branching-factor*)
               val))
(define (pvector-set! pv idx val)
  (pvector-set!! pv idx val)
  pv)
