(define-library (persistent-vector)
  (import (scheme base))
  (export (;; Constructors
           pvector
           list->pvector
           vector->pvector

           ;; Accessors
           pvector-ref
           pvector-length

           ;; Modifiers
           pvector-add-right
           pvector-add-right!
           pvector-remove-right
           pvector-remove-right!
           pvector-set
           pvector-set!))

  (include "util.scm")
  (include "persistent-vector.scm"))
