;; -*- geiser-scheme-implementation: guile -*-

(import (scheme base)
        (srfi srfi-1)
        (ice-9 peg))

;;;; Character sets as PEGs

(define (char-set->peg cs)
  (lambda (str len pos)
    (and (<= (+ pos 1) len)
         (char-set-contains? cs (string-ref str pos))
         (list (+ pos 1) (list (substring str pos (+ pos 1)))))))

(define-peg-pattern newline body
  (or "\r\n"
      (and "\r" (not-followed-by "\n"))
      "\n"))
(define alphabetic (char-set->peg char-set:letter))
(define upper-case (char-set->peg char-set:upper-case))
(define digit (char-set->peg char-set:digit))
(define space (char-set->peg char-set:whitespace))
(define horizontal-space (char-set->peg char-set:blank))

(define char-set:non-id-punct (string->char-set "()[]{}'`\";,@_^"))
(define char-set:id-punct (char-set-difference char-set:punctuation char-set:non-id-punct))

(define non-id-punct (char-set->peg char-set:non-id-punct))
(define id-punct (char-set->peg char-set:id-punct))

;;;; Formal grammar in terms of (ice-9 peg)

(define-peg-pattern Scheme-Doc all
  (and (ignore (* space))
       (+ paragraph)))

(define-peg-pattern paragraph body
  (or section-beginning-paragraph
      example-paragraph
      list-item-paragraph
      indented-paragraph
      ordinary-paragraph))

(define-peg-pattern section-beginning-paragraph all
  (and section-keyword (ignore ":") paragraph-contents paragraph-end))
(define-peg-pattern list-item-paragraph all
  (and (ignore (and (or "-" "•") (+ horizontal-space)))
       paragraph-contents
       paragraph-end))
(define-peg-pattern example-paragraph all
  (and example-input
       (ignore (and (or (+ horizontal-space)
                        linebreak)
                    "=>"
                    (or (+ horizontal-space)
                        linebreak)))
       paragraph-contents paragraph-end))
(define-peg-pattern indented-paragraph all
  (and (ignore (and ">" (+ horizontal-space)))
       paragraph-contents
       paragraph-end))
(define-peg-pattern ordinary-paragraph all
  (and paragraph-contents paragraph-end))

(define-peg-pattern section-keyword body
  (and upper-case
       (+ (or upper-case
              digit
              "-" "_"))))

(define-peg-pattern paragraph-contents body
  (* (or verbatim inline-code syntax-symbol
         (or (range #\null #\tab)
             (and linebreak (not-followed-by linebreak))
             (range #\x11 #\x10FFFF)))))
(define-peg-pattern paragraph-end none (or (and linebreak (+ linebreak)) linebreak ""))

(define-peg-pattern syntax-symbol all
  (and (ignore "<<")
       syntax-symbol-name
       (? sub-super-script)
       (ignore ">>")))
(define-peg-pattern syntax-symbol-name body
  (+ (or verbatim
         (range #\null #\tab)
         (and linebreak (not-followed-by linebreak))
         (range #\x11 #\x3D)
         (and ">" (not-followed-by ">"))
         (range #\x3F #\x5D)
         (range #\x60 #\x10FFFF))))

(define-peg-pattern inline-code all
  (or double-quoted-inline-code single-quoted-inline-code))
(define-peg-pattern example-input all
  inline-code)

(define-peg-pattern single-quoted-inline-code body
  (and (ignore "`")
       (* (or inline-code-markup
              (range #\null #\tab)
              (and linebreak (not-followed-by linebreak))
              (range #\x11 #\x26)
              (range #\x28 #\x10FFFF)))
       (ignore "'")))
(define-peg-pattern double-quoted-inline-code body
  (and (ignore "``")
       (* (or inline-code-markup
              (range #\null #\tab)
              (and linebreak (not-followed-by linebreak))
              (range #\x11 #\x26)
              (and "'" (not-followed-by "'"))
              (range #\x28 #\x10FFFF)))
       (ignore "''")))

(define (%bare-variable-lookbehind-cheat str len pos)
  ;; necessary to get (ice-9 peg) to give us the contents of
  ;; inline-code as a string and not a list of 1-strings; the pure-PEG
  ;; expression is (non-id-punct / space) variable-name
  (and (> pos 0)
       (or (char-set-contains? char-set:non-id-punct
                               (string-ref str (- pos 1)))
           (char-set-contains? char-set:whitespace
                               (string-ref str (- pos 1))))
       (list pos '())))

(define-peg-pattern inline-code-markup body
  (or verbatim
      (and %bare-variable-lookbehind-cheat bare-variable)
      quoted-variable
      syntax-symbol))

(define-peg-pattern bare-variable all
  (and upper-case
       (* (or upper-case
              digit
              id-punct))
       (? sub-super-script)
       (followed-by (or space non-id-punct))))

(define-peg-pattern quoted-variable all
  (and (ignore "$$")
       quoted-variable-name
       (? sub-super-script)
       (ignore "$$")))
(define-peg-pattern quoted-variable-name body
  (+ (or verbatim
         (range #\null #\tab)
         (and linebreak (not-followed-by linebreak))
         (range #\x11 #\x23)
         (and "$" (not-followed-by "$"))
         (range #\x25 #\x5D)
         (range #\x60 #\x10FFFF))))

(define-peg-pattern verbatim body
  (and (ignore "|||")
       (+ (or (range #\null #\x7B)
              (and "|" (not-followed-by "||"))
              (and "||" (not-followed-by "|"))
              (range #\x7D #\x10FFFF)))
       (ignore "|||")))

(define-peg-pattern sub-super-script body
  (and (? subscript)
       (? superscript)))

(define-peg-pattern subscript all
  (and (ignore "_") sub-super-script-text))
(define-peg-pattern superscript all
  (and (ignore "^") sub-super-script-text))

(define-peg-pattern sub-super-script-text body
  (+ (or alphabetic
         digit
         "-")))

;;;; Utility procedures (ice-9 peg) doesn’t provide

(define (match-pattern-completely nonterm string)
  (let ((result (match-pattern nonterm string)))
    (if (and result
             (= (peg:start result) 0)
             (= (peg:end result) (string-length string)))
        result
        (error "couldn’t parse any further" (peg:end result)))))

;;;; Converting the PEG parse tree to SXML

(define (scheme-doc->sxml text)
  (parse-tree-node->sxml
   (peg:tree (match-pattern-completely Scheme-Doc text))))

(define (parse-tree-node->sxml node)
  (if (string? node) node
      (case (car node)
        ((section-beginning-paragraph)
         (cons* 'section-beginning-paragraph
                `(@ (keyword . ,(caadr node)))
                (map parse-tree-node->sxml (cdadr node))))
        (else
         (cons (car node)
               (map parse-tree-node->sxml (cdr node)))))))

;;;; Converting the SXML to HTML

(define (scheme-doc-sxml->html sxml)
  (append
   (map
    (lambda (pgrp)
      (case pgrp
        ((list-item-paragraph)
         (cons 'ul
               (map (lambda (li)
                      (list 'li
                            (cons 'p
                                  (scheme-doc-sxml-inline->html (cdr li)))))
                    pgrp)))
        ((example-paragraph)
         (cons 'table
               (map (lambda (ex)
                      (list
                       'tr
                       ))
                    pgrp))))
      (group-paragraphs (cdr sxml))))))

(define (scheme-doc-sxml-inline->html sxml)
  '...)

(define (group-paragraphs nodes)
  (let loop ((groups '()) (more nodes))
    (if (null? more) (reverse groups)
        (let*-values (((this-group more)
                       (span
                        (lambda (x) (eq? (car x) (caar more)))
                        more)))
          (loop (cons this-group groups) more)))))
