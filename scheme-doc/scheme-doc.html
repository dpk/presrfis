<!DOCTYPE html>
<html lang=en>
<title>Scheme-Doc</title>
<meta charset=utf-8>
<style>
figure.example > pre.source {
    width: 48%;
    float: left;
}
figure.example > blockquote.rendered {
    margin: 0;
    width: 48%;
    float: right;
}

.formal-grammar p {
  text-indent: -1em;
  margin-right: 1em;
}
.formal-grammar p.related {
    margin-top: 0;
    margin-bottom: 0;
}
.formal-grammar code::before { font-family: serif; content: '‘'; }
.formal-grammar code::after { font-family: serif; content: '’'; }
</style>
<script type=module>
// warning: not the most robust TOC generation code in the world
document.addEventListener('DOMContentLoaded', function () {
    let toc = document.getElementById('toc'),
        lastHeadingLevel;
    for (let headingElt of document.querySelectorAll('h2, h3, h4, h5, h6')) {
        if (!headingElt.id) {
            console.warn('ignored heading without id', headingElt);
            continue;
        }
        let level = headingElt.tagName;
        if (lastHeadingLevel) {
            if (level > lastHeadingLevel) {
                let subtoc = document.createElement('ul');
                toc.lastElementChild.appendChild(subtoc);
                toc = subtoc;
            } else if (level < lastHeadingLevel) {
                toc = toc.parentElement.parentElement;
            }
        }
        let tocEntry = document.createElement('li'),
            tocEntryLink = document.createElement('a');
        tocEntry.appendChild(tocEntryLink);
        tocEntryLink.innerHTML = headingElt.innerHTML;
        tocEntryLink.href = `#${encodeURIComponent(headingElt.id)}`;
        toc.appendChild(tocEntry);
        lastHeadingLevel = level;
    };
});
</script>

<h1>Scheme-Doc</h1>

<p>Daphne Preston-Kendal <br>
  Informal draft, 29 March 2022

<h2>Table of Contents</h2>
<ul id=toc>
</ul>

<h2 id=abstract>Abstract</h2>

<p>Scheme-Doc is a lightweight markup language for documenting Scheme procedures, syntax, parameter objects, and other forms.

<p>Scheme-Doc is not a general-purpose markup language for documents of all kinds, like HTML or its lightweight counterparts such as Markdown. It is specifically designed for the purpose of documenting Scheme code.

<p>Scheme-Doc is designed to be readable and renderable in different contents: it is designed to be sufficiently readable in plain text format on its own; it can be rendered in somewhat prettier format in environments such as text terminals where rich-text formatting options exist, but are limited; or it can be rendered into full rich-text HTML or PDF. It is mainly intended to be used in online documentation or otherwise in documentation which is embedded directly into Scheme source code, such as in a documentation string.

<p>Like Markdown and other lightwight markup languages, Scheme-Doc is inspired by the conventions often used in informal plain-text communications such as email; in particular, conventions used in forums such as Lisp mailing lists and in documentation strings and comments. Unlike those markup languages and informal conventions, however, Scheme-Doc is unambiguously defined by a simple formal grammar.

<p>Scheme-Doc attempts to provide simple markup language to cover the majority of Scheme documentation cases. It does not, however, attempt to be absolutely comprehensive, and it is not a replacement for more advanced formatting solutions such as HTML, Scribble, or LaTeX for the documentation of entire Scheme systems.

<h2 id=issues>Issues</h2>

<ul>
  <li>Should a way of embedding arbitrary mathematical expressions in Scheme-Doc paragraphs be provided? Most in keeping with the Scheme-Doc philosophy would be <a href="http://asciimath.org">AsciiMath</a>, but that’s very complicated to implement as a conversion to typeset form (as indeed is any conversion).
  <li>Is general emphasis (bold/italic) useful? My instinct says no.
  <li>What about general structural features like headings and subheadings (distinct from and acting to divide sequences of sections)? Those could be more relevant
  <li>Tables could also be useful, but are likewise tricky to convert.
</ul>

<h2 id=basic-concepts>Basic Concepts</h2>

<p>A Scheme-Doc text is divided into <dfn>sections</dfn>, and each section consists of multiple <dfn>paragraphs</dfn>. Paragraphs are separated from one another by pairs of newlines. Each section except the first begins with a section beginning paragraph.

<p>A <dfn>newline</dfn> is one of the character sequences CR LF, LF, or CR. A line break which is not a paragraph break is treated as equivalent to horizontal whitespace except where otherwise noted — thus Scheme-Doc texts may safely be hard wrapped.

<p><dfn>Non-identifier punctuation</dfn> is any of the characters <kbd>()[]{}'`";,@_^</kbd>. <dfn>Identifier punctuation</dfn> is any printing puncutation character which is not non-identifier punctuation.

<h2 id=sections>Sections</h2>

<p>Sections are documented here in the order in which they would typically appear following a style similar to that adopted by the R7RS small specifications.

<h3 id=spec-section><code>SPEC:</code></h3>

<p>Contains a basic outline of the thing that is documented by this Scheme-Doc text.

<h3 id=requirements-section><code>REQUIREMENTS:</code></h3>

<p>The <code>REQUIREMENTS:</code> section of a Scheme-Doc text describes type or other restrictions on the input of a procedure.

<pre>
SPEC: `(car PAIR)' (procedure)

REQUIREMENTS: `PAIR' must be a pair.
</pre>

<p>In rich-text rendering, <code>REQUIREMENTS:</code> sections typically appear in small type.

<h3 id=description-section><code>DESCRIPTION:</code></h3>

<p>When documenting procedures, variables, very simple macros, and other forms, the <code>DESCRIPTION:</code> section contains the main body of documentation describing their meaning and behaviour and the effect of any arguments to the procedure.

<p>If a Scheme-Doc text begins with a paragraph which has no section keyword, that paragraph implicitly behinds a <code>DESCRIPTION:</code> section.
  
<h3 id=syntax-section><code>SYNTAX:</code></h3>

<p>When documenting Scheme macros, it is typical to give a description of syntactic restrictions before describing the semantics of the macro expansion. The <code>SYNTAX:</code> section is for describing the syntax of macros.

<p>In rich-text rendering, <code>SYNTAX:</code> sections typically appear with an italic ‘<i>Syntax:</i>’ at the start of their first paragraph.
  
<h3 id=semantics-section><code>SEMANTICS:</code></h3>

<p>The semantic counterpart of <a href="#syntax-section"><code>SYNTAX:</code> sections</a>.

<p>In rich-text rendering, <code>SEMANTICS:</code> sections typically appear with an italic ‘<i>Semantics:</i>’ at the start of their first paragraph.

<h3 id=note-section><code>NOTE:</code></h3>

<p><code>NOTE:</code> sections contain informative notes or warnings about the form being documented.

<p>In rich-text rendering, <code>NOTE:</code> sections typically appear in small type with an italic ‘<i>Note:</i>’ at the start of their first paragraph.

<h3 id=examples-section><code>EXAMPLES:</code></h3>

<p>The examples section contains example usage of the Scheme code being documented. It typically contains a series of <a href="#example-paragraph">example paragraphs</a>, perhaps with explanation.

<h3 id=rationale-section><code>RATIONALE:</code></h3>

<p>The <code>RATIONALE:</code> explains in what circumstances the thing being documented is useful or, in particular, what problem it solves which would not (easily) be solvable without it.

<p>In rich-text rendering, <code>RATIONALE:</code> sections typically appear in small type with an italic ‘<i>Rationale:</i>’ at the start of their first paragraph.
  
<h2 id=paragraphs>Paragraphs</h2>

<h3 id=section-beginning-paragraph>Section Beginning Paragraph</h3>

<p>As described above, a section begins with a paragraph that starts with a section keyword, a word in all-capitals followed by a colon. The section keyword must begin with a capital letter and can contain capital letters, hyphens, underscores, and digits. The section keywords defined above have special meanings; others may be used as desired and rendered as the implementation sees fit. Underscores in section keyword names are intended to stand for spaces when rendered.

<h3 id=example-paragraph>Example Paragraph</h3>

<p>An example paragraph contains an example of evaluating code. It begins with code, in which (unlike in other code blocks) hard newlines are preserved and allowed. Then the character sequence <code>=></code> follows, which may also appear on its own line, followed by ordinary paragraph text (usually, but not always, code in itself) showing or describing the result of evaluating the code.

<h3 id=list-item-paragraph>List Item Paragraph</h3>

<p>An unordered list may be represented by a series of paragraphs, each beginning with a hyphen or Unicode bullet character.

<h3 id=indented-paragraph>Indented Paragraph</h3>

<p>A paragraph may be indented and offset from others by beginning it with a <code>></code> character. No <code>></code> is used to begin any line except the first.

<h3 id=ordinary-paragraph>Ordinary Paragraph</h3>

<p>Any paragraph which does not match the production for any of the other kinds of paragraphs is automatically an ordinary paragraph.

<h2 id=inline-markup>Inline Markup</h2>

<h3 id=paragraph-inline>Paragraph Inline Markup</h3>

<p><a href="#syntax-symbols">Syntax symbols</a> and <a
href="#code">code</a> may be embedded directly within paragraph
markup. Variable names require wrapping in code delimiters to render
as variables.

<h3 id=code>Code</h3>

<p>Code exists in both single quoted and double-quoted forms. In both cases, the opening quote is the backtick and the closing quote is the single quote mark. The two are semantically identical. Vertical whitespace is not preserved except when code is used at the beginning of an <a href="#example-paragraph">example paragraph</a>.

<p>Within code, <a href="#syntax-symbols">syntax symbols</a> and <a href="#variable-names">variable names</a> can be used. All other characters stand for themselves and are typically rendered in a monospace font.

<pre>
To non-destructively add a symbol to the beginning of a list, use the
`cons' procedure with a quoted symbol as the first argument: ``(cons
'abracadabra LIST)''. The `push!' macro destructively modifies a
variable containing a list, adding a new element at the beginning:
`(push! &lt;&lt;element expression>> &lt;&lt;variable>>)'.
</pre>

<h3 id=variable>Variables</h3>

<p>Variables may appear within code. They have two forms: bare-capitalized names and dollar-quoted.

<p>Bare-capitalized variable names begin with a capital letter and are continued by capital letters, digits, and identifier punctuation. They must be both preceded and followed by non-identifier punctuation or space; a sequence of capital letters and digits which is not surrounded by non-identifier punctuation is not a variable name, but an ordinary part of code. A bare-capitalized variable name is converted to lower case according to Unicode locale-independent case-mapping before rendering.

<p>Dollar quoted variable names begin and end with pairs of dollar signs. They may contain any character other than underscores, carets, or another pair of double-dollar signs, and no case conversion is performed on their contents.

<p>Variables be ended with <a href="#sub-super-scripts">subscripts and superscripts</a>. In the case of dollar quoted variable names, subscripts and superscripts are placed before the closing double-dollar sign.

<h3 id=syntax-symbols>Syntax Symbols</h3>

<p>Syntax symbols may appear within ordinary text or within inline code, and generally represent the equivalent of pattern variables in the documentation of <code>syntax-rules</code> macros. They are enclosed within pairs of double angle brackets. They may be ended with <a href="sub-super-scripts">subscripts and superscripts</a>, which are placed before the closing double angle brackets.

<h3 id=sub-super-scripts>Subscript and Superscript</h3>

<p>Both variable names and syntax symbols may end with subscript and/or superscript text. Subscripts begin with an underscore and superscripts with a caret. Both are continued by any number of letters, digits, or hyphens. Both a subscript and a superscript may be given, but they must appear in the order subscript, then superscript.

<h3 id=verbatim>Verbatim</h3>

<p>In verbatim spans, no interpretation of other markup (syntax symbols, variable names, etc.) is performed. Verbatim sections may also contain pairs of newlines without these being interpreted as paragraph breaks, though this has no effect on line break rendering. They may appear within code or within ordinary text, including within syntax symbols or dollar-quoted variable names. Verbatim sections are surrounded by triple pipes.

<p><i>Rationale:</i> Verbatim spans allow all-caps symbols, for example, or operators potentially named <code>&lt;&lt;</code>, to appear in code or text without being interpreted as variable names. They also allow variable names which are both case sensitive and contain symbols such as underscore to be spelled as, for example, <code>$$|||Capitalized_with_underscores|||$$</code>.

<p>Though they are noisy, triple pipes were chosen so as not to interfere with the use of <code>||</code> in Scheme code to refer to the symbol with an empty name. Since verbatim sections are only absolutely needed to quote constructs which are very rare in Scheme code anyway, this is not felt to be a significant problem.

<h2 id=full-examples>Full Examples</h2>

<p>The following example text is from the R7RS small specification, translated into Scheme-Doc. The rendering into typeset text on the right is an example of what a rich text display of Scheme-Doc texts could look like.

<h3><code>cons</code></h3>

<figure class=example>
<pre class=source>
SPEC: `(cons OBJ_1 OBJ_2)' (procedure)

DESCRIPTION: Returns a newly allocated pair whose car is `OBJ_1' and
whose cdr is `OBJ_2'. The pair is guaranteed to be different (in the
sense of `eqv?') from every existing object.

EXAMPLES:

``(cons 'a '())'' => `(a)'

``(cons '(a) '(b c d))'' => `((a) b c d)'

``(cons "a" '(b c))'' => `("a" b c)'

``(cons 'a 3)'' => `(a . 3)'

``(cons '(a b) 'c)'' => `((a b) . c)'

</pre>
  <blockquote class=rendered>
    <p><code>(cons</code> <var>obj</var><sub>1</sub> <var>obj</var><sub>2</sub><code>)</code> (procedure)
    <p>Returns a newly allocated pair whose car is <var>obj</var><sub>1</sub> and whose cdr is <var>obj</var><sub>2</sub>. The pair is guaranteed to be different (in the sense of <code>eqv?</code>) from every existing object.
    <table>
      <tr>
        <td><code>(cons 'a '())</code>
        <td>&Rightarrow;
        <td><code>(a)</code>
      <tr>
        <td><code>(cons '(a) '(b c d))</code>
        <td>&Rightarrow;
        <td><code>((a) b c d)</code>
      <tr>
        <td><code>(cons "a" '(b c))</code>
        <td>&Rightarrow;
        <td><code>("a" b c)</code>
      <tr>
        <td><code>(cons 'a 3)</code>
        <td>&Rightarrow;
        <td><code>(a . 3)</code>
      <tr>
        <td><code>(cons '(a b) 'c)</code>
        <td>&Rightarrow;
        <td><code>((a b) . c)</code>
    </table>
  </blockquote>
</figure>

<h3 style="clear: both;"><code>case</code></h3>

<figure class=example>
<pre class=source>
SPEC: `(case &lt;&lt;key>> &lt;&lt;clause_1>> &lt;&lt;clause_2>> ...)' (syntax)

SYNTAX: &lt;&lt;Key>> can be any expression. Each &lt;&lt;clause>> has the form

> `((&lt;&lt;datum_1>>) &lt;&lt;expression_1>> &lt;&lt;expression_2>> ...)'

where each &lt;&lt;datum>> is an external representation of some object. It
is an error if any of the &lt;&lt;datum>>s are the same anywhere in the
expression. Alernatively, a &lt;&lt;clause>> can be of the form

> `((&lt;&lt;datum_1>>) => &lt;&lt;expression>>)'

The last &lt;&lt;clause>> can be an "else clause," which has one of the
forms

> `(else &lt;&lt;expression_1>> &lt;&lt;expression_2>> ...)'

or

> `(else => &lt;&lt;expression>>)'

SEMANTICS: A `case' expression is evaluated as followed. &lt;&lt;Key>> is
evaluated and its result is compared against each &lt;&lt;datum>>. If the
result of evaluating &lt;&lt;key>> is the same (in the sense of `eqv?') to a
&lt;&lt;datum>>, then the expressions in the corresponding &lt;&lt;clause>> are
evaluated in order and the results of the last expression in the
&lt;&lt;clause>> are returned as the results of the &lt;&lt;case>> expression.

If the result of evaluating &lt;&lt;key>> is different from every &lt;&lt;datum>>,
then if there is an else clause, its expressions are evaluated and the
results of the last are the results of the case expression; otherwise
the result of the case expression is unspecified.

If the selected &lt;&lt;clause>> or else clause uses the `=>' alternate
form, then the &lt;&lt;expression>> is evaluated. It is an error if its
value is not a procedure accepting one argument. This procedure is
then called on the value of the &lt;&lt;key>> and the values returned by
this procedure are returned by the case expression.

EXAMPLES:

``(case (* 2 3)
    ((2 3 5 7) 'prime)
    ((1 4 6 8 9) 'composite))'' => `composite'

``(case (car '(c d))
    ((a) 'a)
    ((b) 'b))'' => (unspecified)

``(case (car '(c d))
    ((a e i o u) 'vowel)
    ((w y) 'semivowel)
    (else => (lambda (x) x)))'' => `c'
</pre>
  <blockquote class=rendered>
    (TODO)
  </blockquote>
</figure>

<h2 style="clear:both" id=formal-grammar>Formal Grammar</h2>

<p>This formal grammar is a parsing expression grammar, and is ambiguous if treated as a context-free grammar, but ambiguities are resolved by always choosing the leftmost of multiple options which produce a valid parse. The notation ‘<sup>&amp;</sup><var>pattern</var>’ means that it must be possible to parse the pattern <var>pattern</var> at the point where it appears in the relevant rule, but the result of parsing <var>pattern</var> at that point is ignored.

<blockquote class=formal-grammar>
  <p>⟨Scheme-Doc text⟩ &rarr; space<sup>*</sup> ⟨paragraph⟩<sup>+</sup>

  <p>⟨paragraph⟩ &rarr; <br>
    ⟨section beginning paragraph⟩ /<br>
    ⟨example paragraph⟩ /<br>
    ⟨list item paragraph⟩ /<br>
    ⟨indented paragraph⟩ /<br>
    ⟨ordinary paragraph⟩
  <p class=related>⟨section beginning paragraph⟩ &rarr; ⟨section keyword⟩ <code>:</code> ⟨paragraph contents⟩ ⟨paragraph end⟩
  <p class=related>⟨example paragraph⟩ &rarr; ⟨inline code⟩ (newline / horizontal-space<sup>+</sup> <code>=></code> (newline / horizontal-space<sup>+</sup>) ⟨paragraph contents⟩ ⟨paragraph end⟩
  <p class=related>⟨list item paragraph⟩ &rarr; (<code>-</code> / <code>•</code>) horizontal-space<sup>+</sup> ⟨paragraph contents⟩ ⟨paragraph end⟩
  <p class=related>⟨indented paragraph⟩ &rarr; <code>></code> horizontal-space<sup>+</sup> ⟨paragraph contents⟩ ⟨paragraph end⟩
  <p class=related>⟨ordinary paragraph⟩ &rarr; ⟨paragraph contents⟩ ⟨paragraph end⟩

  <p>⟨section keyword⟩ &rarr; upper-case-letter (upper-case-letter / number / <code>-</code> / <code>_</code>)<sup>*</sup>

  <p class=related>⟨paragraph contents⟩ &rarr; (⟨verbatim⟩ / ⟨inline code⟩ / ⟨syntax symbol⟩ / (any-character &minus; (newline newline))<sup>*</sup>)<sup>+</sup>
  <p class=related>⟨paragraph end⟩ &rarr; newline newline<sup>+</sup> / newline end-of-file / end-of-file

  <p>⟨verbatim⟩ &rarr; <code>|||</code> (any-character<sup>*</sup> &minus; <code>|||</code>) <code>|||</code>

  <p class=related>⟨inline code⟩ &rarr; ⟨double-quoted inline code⟩ / ⟨single-quoted inline code⟩
  <p class=related>⟨single-quoted inline code⟩ &rarr; <code>`</code> ⟨bare-capitalized variable name⟩<sup>?</sup> (⟨inline code markup⟩ / (any-character &minus; <code>'</code> &minus; (newline newline))<sup>*</sup>)<sup>*</sup> <code>'</code>
  <p class=related>⟨double-quoted inline code⟩ &rarr; <code>``</code> ⟨bare-capitalized variable name⟩<sup>?</sup> (⟨inline code markup⟩ / (any-character &minus; <code>''</code> &minus; (newline newline))<sup>*</sup>)<sup>*</sup> <code>''</code>

  <p>⟨inline code markup⟩ &rarr; ⟨verbatim⟩ / ((non-identifier-punctuation / space) ⟨bare-capitalized variable name⟩) / ⟨dollar-quoted variable name⟩ / ⟨syntax symbol⟩
  <p class=related>⟨bare-capitalized variable name⟩ &rarr; upper-case-letter (upper-case-letter / digit / identifier-punctuation)<sup>*</sup> ⟨subscript⟩<sup>?</sup> ⟨superscript⟩<sup>?</sup> <sup>&amp;</sup>non-identifier-punctuation
  <p class=related>⟨dollar-quoted variable name⟩ &rarr; <code>$$</code> (⟨verbatim⟩ / any-character<sup>*</sup> &minus; <code>$$</code> &minus; <code>_</code> &minus; <code>^</code>)<sup>*</sup> <code>$$</code>
  <p class=related>⟨syntax symbol⟩ &rarr; <code>&lt;&lt;</code>  (⟨verbatim⟩ / any-character<sup>*</sup> &minus; <code>>></code> &minus; <code>_</code> &minus; <code>^</code> &minus; (newline newline))<sup>*</sup> ⟨subscript⟩<sup>?</sup> ⟨superscript⟩<sup>?</sup> <code>>></code>
  <p>
  <p class=related>⟨subscript⟩ &rarr; <code>_</code> (upper-case-letter / lower-case-letter / digit / <code>-</code>)*
  <p class=related>⟨superscript⟩ &rarr; <code>^</code> (upper-case-letter / lower-case-letter / digit / <code>-</code>)*
</blockquote>

<h2>Implementation</h2>

<p>A sample implementation is provided which can produce an SXML output tree for a given input Scheme-Doc text, and subsequently convert this into an SXML tree with HTML markup. The sample implementation runs on Guile only and depends on Guile’s PEG parser and SXML transformation utilities. A portable R7RS implementation is desired.
