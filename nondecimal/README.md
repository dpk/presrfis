# `nondecimal`

Procedures for formatting numbers in systems other than simple base 10 place-value ones. Roman numerals and Greek numerals are supported, as are two different styles of alphabetic numbering.

Everything about this library, especially procedure names, is provisional.

## `(integer->roman-numeral n)` (Procedure)

Converts the exact integer <var>n</var> where 1 ≤ <var>n</var> < 4000 into a Roman numeral in capital letters. A convenience procedure for lowercase numerals may be provided; until then, call `string-downcase` on the result.

TODO: Provide an option for the additive style (4 as IIII, 40 as XXXX, etc.)

## `(integer->greek-numeral n)` (Procedure)

Converts the exact integer <var>n</var> where 1 ≤ <var>n</var> < 10000 into a Greek numeral in capital letters. See above for lowercase numeral support.

## `(integer->alphabetic n alphabet)` (Procedure)

Converts the exact positive integer <var>n</var> into an alphabetic numeral by using repeated letters of <var>alphabet</var> instead of a place-value system: thus with the Latin alphabet, the sequence is 26 z, 27 aa, 28 bb, 29 cc …

## `(integer->place-value n alphabet)` (Procedure)

Converts the exact positive integer <var>n</var> into an alphabetic numeral using a one-based place-value system with the letters of <var>alphabet</var>, in the style of spreadsheet columns: thus with the Latin alphabet, the sequenze is 26 z, 27 aa, 28 ab, 29 ac, … 52 az, 53 ba, 54 bb, 55 bc …

## `alphabet:latin-lower`, `alphabet:latin-upper`, `alphabet:greek-lower`, `alphabet:greek-upper`

Convenience variables containing the Latin and Greek alphabets in order as strings in lower and upper case, for the second arguments of `integer->alphabetic` and `integer->place-value`.

## Notes

### Proposed final naming system

`roman`, `roman/lower`, `greek`, `greek/lower`

I really don’t like the names ‘alphabetic’ and ‘place-value’ for the two other styles.

### Greek final sigma

ς is not used in Greek numerals divisible by 200, presumably because it would be confused with ϛ (stigma), which stands for 6. Applying `string-downcase` to the output of `integer->greek-numeral` in Unicodely-correct implementation of Scheme will do the right thing because of the following ʹ numeral mark.

The case of the alphabetic and place-value systems are trickier. There, you possibly *do* want to use final sigma, so that lower-case alphabetic 18 with the Greek alphabet is ς, 42 is σς, 66 is σσς, etc. The way to do this is to use the *upper-case* Greek alphabet then apply `string-downcase` as before. If this really is the way to go, perhaps it would be better to provide only upper-case `alphabet:latin` and `alphabet:greek` and handle the case transformation by having distinct procedures for upper- and lower-case, as with Roman and Greek numerals.
