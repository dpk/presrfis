(define-library (nondecimal)
  (import (scheme base))
  (cond-expand ((library (srfi 13))
                (import (only (srfi 13) string-join)))
               ((library (srfi 130))
                (import (only (srfi 130) string-join)))
               ((library (srfi 152))
                (import (only (srfi 152) string-join))))
  (export integer->roman-numeral
          integer->greek-numeral
          integer->alphabetic
          integer->place-value
          
          alphabet:latin-lower alphabet:latin-upper
          alphabet:greek-lower alphabet:greek-upper)
  (include "nondecimal.scm"))
