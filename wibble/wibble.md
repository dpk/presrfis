# wibble — a Web request routing framework for Scheme

## Route specification

A route specification (route spec, `route-spec`) is how URLs requested by a browser are matched and bound to Scheme code which handles the request. A route specification is always a list.

Each element in the list is either:

* A symbol other than `_`

  Matches any string not containing a `/` (or the following divider character) and binds the symbol to the string matched.

* The symbol `_`

  Matches any string not containing a `/` or the following divider character but does not bind it to anything.

* A literal string

  Matches, but does not bind, the string given. May not include `/` or the following divider character.

* A literal character

  Sets the divider character for the previous element to the character and does not bind it, or (if it is the first element in the list or follows another literal character) simply matches the character and does not bind it.

* A list of multiple elements, the first being a symbol

  Matches any string not containing a `/` (or the following divider character), passes that string to each of the procedures returned (at route definition time) by the remaining elements of the list in turn left-to-right, and binds the symbol to the result of the last procedure in the list. If the procedure signals and error, the error is logged but does not crash the web app; rather, the match is considered to have failed.

* A list of multiple elements, the first being a non-symbol and the second being a symbol, plus possible further symbols.

  Matches the URL-encoded form of the URL against the SRFI 115 regexp given by the first item in the list and binds its match to the symbol of the second item, possibly after validation and pre-processing of the match according to the procedure semantics defined above.

  TODO: Work out what effect the separator character has on this kind of pattern.

An error is signalled at compile time if the same symbol (other than `_`) is used more than once, either in a list or as a bare symbol.

### Examples

#### `()`

Matches the root of the web application, i.e. the path `/`, only.

#### `("users")`

Matches the path `/users`. If there are other routes defined that begin with the literal string `"users"`, it will also catch `/users/` and redirect (probably with code 307? Unsure) anyone who asks for `/users` to the version with trailing slash; if there are no other such routes, it will redirect people from `/users/` to `/users`.

#### `("users" user-id)`

Matches the path `/users/` followed by any string. That string will then be bound (URL-decoded) to the variable name `user-id` within the route definition. Much like above, if other routes defined start with the same pattern, a trailing slash will be added to any request URL; if not, any trailing slash provided will be stripped.

Presumably the `user-id` is an integer representing the primary key of the user’s entry in a `users` SQL table, or similar. This route provides no guarantee that `user-id` will be an integer — the case where it isn’t would have to be handled in the route definition code. But we can do better than that …

#### `("users" (user-id string->integer))`

Assuming `string->integer` is a procedure like `string->number` which only works on exact integers, this route spec will validate that `user-id` as requested by the browser is a cromulent integer string, and bind the integer value to `user-id` instead. If `string->integer` signals an error because someone gave it a string like `"spam"` instead of one like `"123"`, the error will be caught by the route matcher and cause the match to fail. The route matcher will try to match other routes if possible, but it is likely that no others are defined which handle this case, so the request will automatically fall through and 404.

This route still does not provide any guarantee that the `user-id` given corresponds to an actually existing user in the web application. So we can do even better than that!

#### `("users" (user string->integer find-user-by-id))`

Assuming `find-user-by-id` takes an integer as argument and returns the user row from the database which corresponds to that ID, this form will do as above and first try converting the given user ID to an integer, and then look for the user with that ID in the database and bind an object representing that user’s table row to `user`. If either of these steps fails and signals an error, the same thing happens as above: the route match fails, the error is logged, and matching (likely) falls through to a not found page.

(TODO: If `find-user-by-id` returns `#f` instead of signalling an error when a non-existent user is requested, it would be handy to have utility function in the routing framework that signals an error if its argument is `#f`, or otherwise returns its argument.)

#### `((article string->integer find-article-by-id) #\- _)`

Let’s assume this is a blog or news website application where the URLs look like `/<article-id>-<article-slug>`, where `<article-id>` is an integer like for the user ID in the previous examples, and `<article-slug>` is usually a URL-friendly form of the headline, usually all lower case, and with all of the spaces and other punctuation changed to `-` characters.

This example implements that URL pattern using `-` as the divider character between the article ID and the slug, so only what comes before the `/`. The slug is `_`, i.e., is is neither matched nor bound, so `/123-the-real-article-title` and `/123-some-other-nonsense` will both find and return the same article. A real app will likely wish to canonicalize these URLs by binding the slug to a variable and redirecting if it doesn’t match what it expects for that article, for better search engine rankings or whatever.

### Miscellaneous details

Setting the divider character `#\/` explicitly is generally a no-op. Using another character as the first element in a route spec, though, will bind a URL beginning with `/` followed by that character. So `(#\~ user-name)` would match a typical Unix-style userdir pattern: `/~dpk`, `/~sbp`, etc. Likewise you can create (rather redundant) patterns like `/users/@dpk` or `/users/@sbp` with the route spec `("users" #\/ #\@ user-name)`.

#### Behaviour of string matching over percent-encoded characters

Percent-encoded characters in matched strings are decoded before being passed to the processing pipeline and thus to the handler code. UTF-8 is tried first, then Latin-1 if the percent-encoded characters are invalid UTF-8.

If the separator character is reserved according to [RFC 3986][rfc3986] (like `/`), a percent-encoded version of that character will *not* cause the match to terminate. So the URL path `/input%2Foutput` matches the route spec `(name)`, and the `name` variable in the handler code will be bound to the string `"input/output"`; it will not match the route spec `("input" text)`.

[rfc3986]: https://tools.ietf.org/html/rfc3986

On the other hand, if the separator character is *not* reserved by RFC 3986, a percent-encoded version of the character *will* match the separator and terminate matching for the current pattern. So the URL path `/%7Edpk` will match the route spec `(#\~ username)`, and `username` will be bound to `"dpk"` in the handler code. Likewise, `/123%2Darticle-slug` will match the route spec `(article-id #\- article-slug)`.

TODO: Work out if these semantics make sense.

## Route bindings

All route bindings automatically bind the HTTP OPTIONS method for the given route spec and provide the correct answer, listing the HTTP methods defined for that route.

Routes bound earlier have priority over those bound later.

The syntax shown works only within a given `application` or `define-application` block; the procedure(s) work anywhere.

### `(get route-spec . body)` (Syntax)

Binds the HTTP GET method for the given `route-spec` to the `body` code with appropriate bindings. Also, for convenience, binds the HTTP HEAD method to the given `route-spec` to a wrapper procedure around `body` that returns the exact same headers, but no body.

### `(post route-spec . body)` (Syntax)
### `(put route-spec . body)` (Syntax)
### `(patch route-spec . body)` (Syntax)
### `(delete route-spec . body)` (Syntax)
### `(get* route-spec . body)` (Syntax)
### `(head route-spec . body)` (Syntax)

All these forms bind the respective HTTP method for the given `route-spec`s to the given `body` code with appropriate bindings. `get*` is like `get` but does not automatically bind a HEAD method. Returning different headers from HEAD than from GET is technically a violation of the HTTP specification, but if HEAD is expected to be used often for whatever reason, it may be useful to define a more efficient method of generating the same headers, if it is possible to do so without actually generating the page body internally without sending it.

### `(route method route-spec . body)` (Syntax)

Binds the HTTP method `method` (TODO: a symbol? a string? automatically quoted or not? but presumably always case-insensitive) to for the given `route-spec` to the given `body` code with appropriate bindings. This could be useful if implementing one of the \*DAV technologies which use special HTTP methods not otherwise supported by Wibble’s other bindings.

### `(add-route! application method route-spec route-proc-alist handler)` (Procedure)

Binds the HTTP method `method` to the `route-spec` within the given `application` object, with the difference that lists in the `route-spec` are invalid. Instead, if a symbol in the `route-spec` has a corresponding entry in the `route-proc-alist`, the procedure from the alist will be used to validate/process the corresponding route element.

When the route spec matches for the given method, the given procedure `handler` is called with the validated/processed parts of the URL the bound by matching symbols as its arguments.

This is quite complicated, so here’s an example. The following:

```scheme
(define-application flutter)
(add-route! flutter
            'GET '("users" user)
            `((user . ,find-user-by-username))
            (lambda (user) ...))
```

is identical in effect to:

```scheme
(define-application flutter
  (get ("users" (user find-user-by-username))
    ...))
```

## Route definition code

Route definition code may access the `(current-request)` parameter to access the raw request data which triggered the route call, and the `(current-responder)` parameter to access an escape continuation to return from a route early. Wibble provides the procedure `(respond . args)`, which calls `((current-responder) . args)`, for convenience.

Route definition code (the `body` of a syntax-based route definition, or whatever’s inside the procedure added to an app with `add-route!`) may return (either as its natural return value, or with the `(current-responder)` escape procedure):

* `#f`, in which case the route match is considered to have failed and the next-defined route matcher is tried.
* An integer in the range 100 to 999, which is interpreted as a response code. The body and headers are determined by customizable status code handlers for the application itself; thus returning 404 from a route is sufficient to show the application’s Not Found page. (TODO: Specify how the handlers are customized.)
* A string, which will be UTF-8 encoded and sent to the client with a status code of 200 and a `Content-Type: text/html; charset=utf-8` header.
* A bytevector, which will be sent to the client with a status code of 200 and a `Content-Type: application/octet-stream` header.
* Three values, `status headers body`, with the same semantics as SWAG except as follows. `body` may be a string, which will be UTF-8 encoded automatically; in this case, the `content-type` header is not touched if provided but set to `text/html; charset=utf-8` if not explicitly set.

The `response` procedure is internally used to convert these types to proper SWAG responses, and can be used in place of `values` for cleaner-looking code.

TODO: Should it be possible to customize the default response type on a per-application basis? IRC web services often need `text/plain`; less obscurely, applications which define an API will likely want to send `application/json` responses from all routes.

## Utility procedures and combinators for route bindings

### `((exists proc) arg)`

Returns `arg` if not `#f`, or else raises an error. Useful for validation/processing procedures where `proc` returns a value if something is found or `#f` if not.

### `(numeric arg)`

Like `string->number`, but `arg` must be a positive decimal number which does not begin with leading 0s (or else a single 0 character). Useful for the common case where entity IDs are positive integers and only the canonical form should match.

Example of the two previous used together: `("articles" (article-id numeric (exists article-with-id)))`. For the URL `/articles/123`, this matches with the initial value `"123"`, then calls `numeric` to convert that to the integer `123`, then tries to look up the article with `article-with-id` (a procedure which returns either an article object or `#f` if there’s none with the given id), and succeeds with `article` bound to the article object if it there’s an article 123, or fails if not.

### `(path arg)`

Takes either a string or a regexp match. Splits `arg` by `#\/` then URL decodes each of the components according to the usual fallback rules. Useful when a regular expression is used to match an arbitrary path within a URL and you want to deconstruct that path into its components as Wibble would usually.

Example: `("help" ((+ any) article-location path))` matches `/help/fran%C3%A7ais/r%C3%A9cursivit%C3%A9`; `article-location` is bound to `("français" "récursivité")`.
