# HTML templating in Wibble

## `(template . forms)` (Syntax)

Each `form` in `forms` is one of:

* `(*inherit* template)`

  `template` is a value (probably usually a variable name) which defines a parent template from which this template inherits its behaviour. May only appear at the top level of a template.

* `(*block* name . forms)`

  If there is no block called `name` (a symbol) previously defined in this or a parent/inherited block, this defines the block `name` within this template and fills it with the default value `forms`, having the same meaning recursively as in `template` itself.

* `(*include* template)`

  `template` is a value (probably usually a variable name) pointing to another template. That template is included and its result spliced into the overall template result at the point where `*include*` form was.

## Example

```scheme
(define-template layout
  (*doctype* "html")
  (meta (@ charset "utf-8"))
  (title "My Weblog" (*block* page-title))
  (*block* content))

(define-template homepage
  (*inherit* layout)
  (*block* content
    (h1 "Recent posts")
    ,@(for-each (lambda (x) ...) posts)))
```

TODO: Work this out properly.
