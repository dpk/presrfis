# Mutable environments

## `(definition-environment IMPORT-SET_1 ...)` (procedure)

Returns a <dfn>definition environment</dfn> initialized with the bindings imported from the `IMPORT-SET`s. A definition environment allows new bindings to be created in it by evaluation of `define` and `define-syntax` forms, and mutation of newly-established variable bindings using `set!`. This is subject to the same limitations on imported bindings in programs or libraries: it is not allowed to mutate imported bindings.

In other words, this procedure is an exact analogue of the small report’s procedure `environment`, except that definition of new bindings, and mutation of those bindings once established, is allowed in the returned environment.

## `(environment-import! ENV IMPORT-SET_1 ...)` (procedure)

Extend the definition or interaction environment `ENV` by the additional imports given by the `IMPORT-SET`s. If the newly-imported bindings would conflict with any already-imported bindings, or with bindings created by evaluation of `define` or `define-syntax` etc. in the environment, it is an error.

## `(environment-freeze! ENV)` (procedure)

Irreversibly forbid the establishment of new bindings in `ENV` or their mutation by `set!`.
