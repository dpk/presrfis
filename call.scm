;;; (call expr ...)
;;
;; Expands to (expr ...) but guarantees that the expr ... will be
;; evaluated in left-to-right order.

(define-syntax call
  (syntax-rules ()
    ((_ . args)
     (%call-aux args ()))))

(define-syntax %call-aux
  (syntax-rules ()
    ((_ (expr . more-exprs) (exprs-w/gensyms ...))
     (%call-aux more-exprs (exprs-w/gensyms ... (gensym expr))))
    ((_ () ((gensym expr) ...))
     (let* ((gensym expr) ...)
       (gensym ...)))))
