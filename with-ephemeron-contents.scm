(define-syntax begin0
  (syntax-rules ()
    ((_ expr1 more-expr ...)
     ;; this reflects Emacs’s strange ideas about indentation and
     ;; layout of call-with-values; I’d do it differently, but I
     ;; assume this is a de facto standard
     (call-with-values (lambda () expr1)
       (lambda x
         more-expr ...
         (apply values x))))))

(define-syntax with-ephemeron-contents
  (syntax-rules ()
    ((_ (kvar dvar) eph intact-clause broken-clause)
     (let ((kval (ephemeron-key eph))
           (dval (ephemeron-datum eph)))
       (if (ephemeron-broken? eph)
           broken-clause
           (let-syntax ((kvar
                         (identifier-syntax
                          (begin0 kval
                            (reference-barrier kval))))
                        (dvar (identifier-syntax dval)))
             intact-clause))))))

