;; TODO: Handle obs-fold
;; TODO: chunked transfer-encoding
;; TODO: better error handling

(define (latin1->string u8vec)
  (string-tabulate (lambda (idx) (integer->char (bytevector-u8-ref u8vec idx)))
                   (bytevector-length u8vec)))
(define (string->us-ascii str)
  (let ((u8vec (make-bytevector (string-length str))))
    (let loop ((idx 0))
      (if (>= idx (bytevector-length u8vec)) u8vec
          (let ((ascii-value (char->integer (string-ref str idx))))
            (if (> ascii-value 127)
                (error "character outside of ASCII character set"
                       (string-ref str idx))
                (begin
                  (bytevector-u8-set! u8vec idx)
                  (loop (+ idx 1)))))))))

(define (bytevector-null? u8vec) (= (bytevector-length u8vec) 0))
(define (bytevector-strip-newline u8vec)
  (cond ((= (bytevector-length u8vec) 0) u8vec)
        ((and (>= (bytevector-length u8vec) 2)
              (= (bytevector-u8-ref u8vec (- (bytevector-length u8vec) 2))
                 #x0D)
              (= (bytevector-u8-ref u8vec (- (bytevector-length u8vec) 1))
                 #x0A))
         (bytevector-copy u8vec 0 (- (bytevector-length u8vec) 2)))
        ((memq (bytevector-u8-ref u8vec (- (bytevector-length u8vec) 1))
               '(#x0A #x0D))
         (bytevector-copy u8vec 0 (- (bytevector-length u8vec) 1)))
        (else u8vec)))
(define (bytevector-index u8vec needle)
  (let loop ((idx 0))
    (cond ((>= idx (bytevector-length u8vec)) #f)
          ((= (bytevector-u8-ref u8vec idx) needle) idx)
          (else (loop (+ idx 1))))))

(define (magic-header-name? name)
  (let ((name-str (symbol->string name)))
    (and (char=? (string-ref name-str 0) #\*)
         (char=? (string-ref name-str (- (string-length name-str) 1)) #\*))))

(define char-set:http-token
  (string->char-set "!#$%&'*+-.^_`|~\
                     0123456789\
                     abcdefghijklmnopqrstuvwxyz\
                     ABCDEFGHIJKLMNOPQRSTUVWXYZ"))
(define (token-char? char)
  (char-set-contains? char-set:http-token char))

(define *http-max-method-length* 32)
(define *http-max-path-length* 8192) ;; RFC 7230 s. 3.1.1
(define *http-max-header-line-length* 8192)

(define (read-to-terminator ip terminator max)
  (call-with-port
   (open-output-bytevector)
   (lambda (op)
     (let loop ((n max))
       (let ((b (read-u8 ip)))
         (cond ((= b terminator) (get-output-bytevector op))
               ((= n 0)
                (error "exceeded maximum allowable length"
                       ip (get-output-bytevector op)))
               (else
                (write-u8 b op)
                (loop (- n 1)))))))))


(define (read-http-method ip)
  (string->symbol
   (string-upcase (latin1->string
                   (read-to-terminator ip #x20 *http-max-method-length*)))))

(define (read-http-path ip)
  (latin1->string (read-to-terminator ip #x20 *http-max-path-length*)))

(define (read-http-protocol ip)
  ;; should be to \r\n, not to \n, but some clients are bad
  (latin1->string (bytevector-strip-newline (read-to-terminator ip #x0A 32))))

(define (parse-header raw-header)
  (let* ((colon-index (bytevector-index raw-header #x3A))
         (raw-header-name (bytevector-copy raw-header 0 colon-index))
         (raw-header-value (bytevector-copy raw-header (+ colon-index 1)))
         (header-name (string-foldcase (latin1->string raw-header-name))))
    (cond ((not (string-every header-char? header-name))
           (error "invalid header field-name" header name))
          (else (cons (string->symbol header-name)
                      (string-trim-both (latin1->string raw-header-value)))))))
(define (read-http-headers ip)
  (let loop ((headers '()))
    (let ((raw-header (bytevector-strip-newline
                       (read-to-terminator ip #x0A
                                           *http-max-header-line-length*))))
      (if (bytevector-null? raw-header) (reverse headers)
          (let ((header (parse-header raw-header)))
            (loop (cons header headers)))))))

(define (path-query raw-path)
  (cond-expand
   ((library (srfi 130))
    (let ((q-cur (string-index raw-path (lambda (c) (char=? c #\?)))))
      (if (string-cursor=? q-cur (string-cursor-end raw-path))
          (values raw-path #f)
          (values (string-copy/cursors raw-path
                                       (string-cursor-start raw-path)
                                       q-cur)
                  (string-copy/cursors raw-path
                                       (string-cursor-next raw-path q-cur))))))))

(define (parse-raw-path raw-path request)
  (let-values (((path query) (path-query raw-path)))
    (if query
        (cons* (cons 'path path) (cons 'query query) request)
        (cons (cons 'path path) request))))

(define (read-http-request ip)
  (let* ((method (read-http-method ip))
         (raw-path (read-http-path ip))
         (protocol (read-http-protocol ip))
         (headers (read-http-headers ip)))
    (parse-raw-path
     raw-path
     (list (cons 'method method)
           (cons 'protocol protocol)
           (cons 'headers headers)))))

(define (write-http-response op status headers body)
  (write-bytevector (string->us-ascii "HTTP/1.0 ") op)
  (write-bytevector (string->us-ascii (number->string status)) op)
  (write-u8 #x20 op)
  (write-bytevector
   (string->us-ascii
    (let ((code (assv status *http-codes*)))
      (if code (cdr code)
          "Something")))
   op)
  (write-bytevector #u8(#x0D #x0A) op)
  (flush-output-port op)
  (for-each
   (lambda (header)
     (let ((name (car header))
           (value (cdr header)))
       (unless (or (and (bytevector? body) (eq? name 'content-length))
                   (magic-header-name? name))
         (write-bytevector (string->us-ascii (symbol->string name)) op)
         (write-bytevector #u8(#x3A #x20) op)
         (write-bytevector (string->us-ascii value) op)
         (write-bytevector #u8(#x0D #x0A) op))))
   headers)
  (flush-output-port op)
  (cond ((bytevector? body)
         (write-bytevector (string->us-ascii "content-length: ") op)
         (write-bytevector (string->us-ascii
                            (number->string (bytevector-length body)))
                           op)
         (write-bytevector #u8(#x0D #x0A #x0D #x0A) op)
         (write-bytevector body op)
         (flush-output-port op))
        (else
         (write-bytevector #u8(#x0D #x0A #x0D #x0A) op)
         (let ((chunk (make-bytevector 4096)))
           (let loop ((bytes-read (read-bytevector! chunk body)))
             (when (not (eof-object? bytes-read))
               (write-bytevector chunk op 0 bytes-read)
               (flush-output-port op)
               (loop (read-bytevector! chunk body))))))))

(define http-1.0-serve-request
  (case-lambda
    ((application input output)
     (http-1.0-serve-request application input output '()))
    ((application input output base-request)
     (let ((request (append! (read-http-request input) base-request)))
       (let-values (((status headers body) (application request)))
         (write-http-response output status headers body))))))

(define (http-1.0-server application host port)
  (let* ((addrinfo (get-address-info host port))
         (listener (make-listener-socket addrinfo)))
    (run-net-server
     listener
     (lambda (ip op sock addr)
       (let loop ()
         (http-1.0-serve-request application
                                 ip
                                 op)
         (unless (eof-object? (peek-u8 ip)) (loop)))))))
