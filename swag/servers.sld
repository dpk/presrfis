(define-library (swag servers)
  (import (scheme base)
          (scheme case-lambda)
          (scheme char)
          (scheme write)
          
          (scheme list)

          (srfi 14)
          (srfi 130)

          (chibi net)
          (chibi net server))
  (export http-1.0-serve-request
          http-1.0-server)

  (include "http-data.scm")
  (include "servers.scm"))
