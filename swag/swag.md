# SWAG (Scheme Web Application Gateway)

If this becomes a SRFI, references to libraries beginning with `(swag)` will presumably start with `(srfi xxx)` instead.

## Application procedures

An application procedure may be defined by a form such as `(lambda (request) . body)`; it returns three values, `status headers body`.

* `request` is an association list containing at least the following keys:
  * `method` is a symbol containing the HTTP request method used to call the application, normalized to all upper-case.
  * `path` is a string containing the HTTP URL path (not including query part) requested from the application. It must begin with a `/` character.
  * `query` is a string containing the query segment of the HTTP URL. It does not begin with a `?` character. It must be absent if no query was provided by the client.
  * `headers` contains an association list mapping header names (which are given as symbols normalized to lower case) to their values as strings. It is permissible to map all CGI or FastCGI `HTTP_*` environment variables to corresponding entries in the `headers` fields by deleting the `HTTP_` prefix from the names, converting the remainder to lowercase, and replacing `_` with `-` in the names. If there are multiple request headers with the same name, each should gets a seperate entry in the `headers` alist; if, for gateway-technical reasons, this is not possible, then the gateway server may have rewritten multiple request headers with the same name into a single request header, equivalent to the original form in terms of HTTP semantics. Servers should provide headers in this alist in the exact order they were provided by the client whenever possible.
  * `body` is a binary input port for reading the request body. This should always be present even if no request body was given, though it may return an end-of-file object whenever it is read.
  * `url-scheme` may contain either the symbol `http` or `https`, depending on which protocol was used to make the request. It may be absent if this information is not known to the server.
  * `protocol` is a string containing the complete name and version number of the protocol used to make the request, e.g. `"HTTP/1.0"`. It may be absent if this information is not known to the server.
  * `remote-addr` is a string containing the IP address of the client or proxy which made the request. It may be absent if this information is not known to the server.
  * `server-name` is a string containing the hostname or IP address of the server. It may be absent if this information is not known to the server.
* `status` is an integer in the range 100 to 999 inclusive indicating the HTTP status code returned by the application.
* `headers` contains the response headers in the same format as the request headers. Response header names which both begin and end with a `*` character are for communication between the application and the server and must not be sent to the client. Response header names must not be the empty symbol, and must not contain characters disallowed in header names by RFC 7230 (that is, any character other than ASCII letters and digits and the punctuation characters ``!#$%&'*+-.^_`|~``). Applications must not return a `status` header. Applications must not return header values containing any characters disallowed or considered obsolete in header values by RFC 7230 (that is, any character other than visible ASCII characters plus space and horizontal tab).
* `body` is either a bytevector or a binary output port which will be used for the HTTP response body.

Applications should not return headers which are incompatible or incorrect with regards to the returned `status` and `body` fields according to [RFC 7231][rfc7231]. For instance, it would be wrong to return a `body` other than the empty bytevector, or a binary output port already in the EOF state, with a `status` of 204 (No Content), or to set a `content-length` header on surch a response.

[rfc7231]: https://tools.ietf.org/html/rfc7231

### The `content-length` response header

If the application returns a bytestring as the response body, the server should set the `content-length` header appropriately for the length of the bytestring given.

If the application returns a binary output port as the response body and a `content-length` header, the server should not send more than `content-length` bytes to the client. It should log an error if more or less than `content-length` bytes are provided by the application to the server. However, the application developer is ultimately responsible for ensuring that the contract from the server to the browser to provide exactly `content-length` bytes of response body data

If the application returns a binary output port as the response body, but no `content-length` header, the server may use chunked transfer encoding (as in HTTP/1.1) to send the client’s response if it knows the client will be able to understand it, or else simply close the connection to the client when the transfer is complete.

The `content-length-middleware` (see below) can help server developers fulfill their obligations under this section.

## Middleware procedures

Middleware procedures are higher-order procedures which take a SWAG application procedure as their first (and usually only) argument and return another procedure which usually, but not always, calls the application procedure after pre-processing. Thus middleware can both modify the request information sent to the application procedure before it sees it, and modify the response received from the application procedure before passing it on to the web.

Modifications to request metadata and response headers should be non-destructive whenever possible, by consing onto the alists rather than modifying their contents; or, if it is not possible to avoid destructively modifying the alists themselves, then the individual values should be replaced and not mutated. For instance, middleware which modifies the `content-type` header of a response cannot avoid mutating the response header alist (because it is wrong in HTTP to send more than one `content-type` header), but it should do so by consing a completely new string for the new value, rather than by mutating the original string given.

### Standard middleware

The following middleware procedures are available by importing `(swag middleware)`.

*Rationale:* Most of these middlewares provide parsing for various data formats intrinsic to the semantics and syntax of HTTP. Providing parsers in SWAG saves application developers from having to develop their own parsers for each of these data formats.

### `(accept-middleware application)`

If the request has one of more of the `accept`, `accept-encoding`, or `accept-language` headers, these will be parsed by the middleware and put into correspondingly-named fields in the request object itself. The parsed form is a list of pairs: the car of each pair is the MIME type, encoding, or natural language which the client claims to accept in the relevant header; the cdr of the pair is an exact number reflecting the client’s given `;q=` quality value, or the integer 1 if no quality value was explicitly given. Values appear in the order in which they appeared in the original request headers.

More information may be found in the MDN documentation for the [`accept`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept), [`accept-encoding`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Encoding), and [`accept-language`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language) headers.

`accept-middleware` does not alter the response from the application.

TODO: Should we instead (stably) sort the values in decreasing order of quality value, in case the client gave us a weird order?

#### `(cookies-middleware application)`

If the request has one or more `cookie` headers, `cookies-middleware` parses the names and values of the cookies from that header and puts them in an alist with symbol keys and string values in the request metadata under the name `cookies`.

If the response has one or more `*set-cookie*` headers whose values are lists of the form `(name value flags)` where `name` is a symbol, `value` a string, and `flags` an alist, `cookie-middleware` will convert the list in the values to a `set-cookie` header with the correct form. The `flags` alist may have any or none of  the following keys, in any order, with no duplicates: 

* `expires` may be a string containing ready-formatted HTTP-date timestamp, or else some unambiguous object representing a point in time known to the specification, and sets the `Expires` attribute of the cookie to that point in time. If a Scheme language implementation both includes SWAG support and claims support for a particular RnRS which specifies such a data type, this data type must be supported as the value of `expires`.
* `max-age` must be an integer and sets the `Max-Age` attribute of the cookie.
* `domain` must be a string and sets the `Domain` attribute of the cookie.
* `path` must be a string and sets the `Path` attribute of the cookie.
* `secure` or `http-only` must, if present, be mapped to the empty list, and set the `Secure` and `HttpOnly` attributes of the cookie, respectively.
* `same-site` must be a symbol, one of either `strict`, `lax`, or `none`, and sets the `SameSite` attribute of the cookie accordingly.

If the response has one or more `*delete-cookie*` headers whose values are symbols, `cookies-middleware` will send the header `set-cookie: <name>=""; Max-Age=0` for that cookie name.

If the response has one or more `*refresh-cookie*` headers whose values are a pair, the car of the pair is a symbol representing a cookie name, and the cdr is either a string or time object as described in `expires`, or an integer. `cookies-middleware` will set the cookie to the same value it had in the request, but with the `Expires` or `Max-Age` attribute set as appropriate. `*refresh-cookie*` is a no-op if the given cookie name did not exist in the request.

`*delete-cookie*` takes precedence over both `*set-cookie*` and `*refresh-cookie*`, and `*set-cookie*` takes precedence over `*refresh-cookie*`, no matter what order they occur in. Any other `set-cookie` headers received in the response are unaffected.

More information may be found in the MDN documentation for the [`cookie`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cookie) and [`set-cookie`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie) headers.

#### `(form-data-middleware application)`

If the request has a `content-type` header which is either `application/x-www-form-urlencoded` or `multipart/form-data`, the body of the request will be read and decoded accordingly, and loaded into the `form-data` field in the request metadata object as an association list before the application is called.

If the request content-type is `multipart/form-data` and one or more of the parts in the request body has the `filename` set on its `content-disposition` header, those entries are instead put into a `files` alist in the request metadata, associating the name of the file as a string with a binary input port for reading the actual content of the file.

TODO: Is this actually semantically correct for how browsers handle `<input type=file>`? Is it useful to access the `content-type` header for each part? Check what Rack and WSGI do in this case.

TODO: Better specify the form of the `form-data` association list.

`form-data-middleware` does not alter the response from an application.

More information may be found in [RFC 7578](https://tools.ietf.org/html/rfc7578) (specification of the `multipart/form-data` content type).

#### `(query-data-middleware application)`

If the request has a `query` which can be parsed as form data, it is parsed and decoded and put as an alist into the `query-data` field on the request, just as the `form-data` is interpreted from a `application/x-www-form-urlencoded` body by the `form-data-middleware`. If the request has a `query` but it cannot be parsed as form data (because it doesn’t contain an `=` character), the request is unaltered.

`query-data-middleware` does not alter the response from an application.

TODO: Should `query-data` be inserted with an empty list when the query isn't parseable, instead of being omitted?

#### `(content-length-middleware application)`

This middleware is provided for the convenience of server developers and should not be used by application developers.

If the response body is a bytestring, the `content-length` header will be set to the correct length of the bytestring returned, throwing away any other (potentially incorrect) `content-length` header returned by the application.

If the response body is a binary output port and the application or middleware set a `content-length` header, the binary output port provided directly by the application will be wrapped in an output port that ensures exactly `content-length` bytes will be read from the binary output port and sent to the server. If, after `content-length` bytes have been read from the output port, the output port is not at end-of-file, or if end-of-file is reached before `content-length` bytes have been read, an error is logged. This does not prevent the HTTP server from breaking its contract to the HTTP client to provide exactly `content-length` bytes’ worth of response body.

If the response body is a binary output port and the application or middleware did not set a `content-length` header, the response is passed through unchanged.

TODO: Perhaps it would be good to replace this with a more general `conformance` middleware for server developers, which checks more than just `content-length` but also e.g. reports an error if the application tried to set a `status` header.

TODO: Are any other middleware procedures actually needed? Could a `utf8` middleware which converts strings and string output ports to bytevectors and binary output ports as the response `body` be handy, or would that just be asking for trouble? Or an `error-handler` middleware which standardizes the bodies and content-types of error pages (so an app can just return `404 () #u8()` to indicate a not-found page and the body will be filled in automatically by the middleware)? Various ideas here — look also at what Rack and WSGI do.

## Servers

Servers are procedures which take an application as their first argument, various server-specific configuration options as further arguments, and provide some means of interfacing from a SWAG application procedure either directly to HTTP itself, or to another gateway interface that speaks with HTTP semantics, so that actual users with actual Web browsers can use SWAG applications. Exactly what happens to a Scheme process when a server procedure is called is undefined, but usually the process or thread will hang waiting for a connection or for request data.

If the application procedure given to a server signals an error when called, the server must return a server 500 (Internal Server Error) response to the client, containing no information about the cause of the error whatsoever. The error information should be recorded in an internal log instead.

Servers may provide customizable logging facilities to an application by parameterizing the value of `current-error-port` to a suitable textual output port before calling the application.

Servers may add a `server` header to the response headers provided by the application, or modify the `server` header provided by the application by prefixing, to identify themselves in accordance with the specification of the Server header in section 7.4.2 of RFC 7231. However, in doing so, they should be aware of the security risks of making identifiable information about the server software used available, as making such information public may make it easier for attackers to identify Web services running vulnerable versions of server software. If servers are capable of adding or modifying the `server` header, they should also provide a way to turn this behaviour off if there is any expectation that the server software will be run in production.

If the `body` returned by an application is an output port, and `file-info-regular?` from [SRFI 170][srfi-170] indicates that the output port points to a regular file, and the server application is running on an operating system which supports the `sendfile(2)` system call and has access to that system call, the server application may use that system call to send the response body to the client.

[srfi-170]: https://srfi.schemers.org/srfi-170/srfi-170.html

### Standard servers

The following servers are available by importing `(swag servers)`.

#### `(http-1.0-server application host port)`

A single-threaded, single-process synchronous HTTP/1.0 server which binds to the specified `host` and `port`. Can only handle one request at once, but once that request is fulfilled, the server keeps running and answers further requests until the Scheme interpreter or the operating system interrupts the procedure using some mechanism not specified here. (For instance, the OS may just kill the Scheme process entirely.)

*Rationale:* Python and Ruby provide the single-request, synchronous servers `BaseHTTPServer` and WEBrick, to which the WSGI and Rack libraries provide bindings. Such servers are useful for local development purposes, where handling multiple requests at once is generally not a requirement.

TODO: When NetworkPortsCowan gets SRFI’d, change `host` and `port` to just get passed straight through to `make-network-listener`.

#### `(http-1.0-serve-request application input output [request])`

Parses one HTTP request from the provided binary `input` port, passes it to the `application` to process, and sends the response to the given `output` port. May be called simultaneously across multiple Scheme threads or processes; blocks until the request has been fully received and the response has been fully sent. Does not signal an error if the client sends an unparseable request, but rather immediately sends a 400 Bad Request response. May signal an error if the application breaks any requirements of SWAG or of HTTP syntax (such as by sending a header with an invalid name).

`request` may be an alist containing the initial values for the request object, in particular the values for `url-scheme`, `remote-addr`, `server-name`, and `server-port` which cannot be found out by the parser alone. The request information found by the HTTP request parser will be consed onto it non-destructively (but it cannot be certain that applications or middleware will not mutate it); it defaults to the empty list.

*Rationale:* Web server developers may, at least initially, be considerably more interested in questions such as load-balancing across multiple Scheme processes than in the nitty-gritty of HTTP parsing. This procedure allows them to implement the networking part of a production-ready HTTP server while delegating processing of HTTP syntax and semantics entirely.

#### `(cgi-server application input output)`

Implements CGI on the given binary input and output ports, using SRFI 170’s `get-environment-variables` to construct the request information and metadata and pass them to the application. Can only handle one request per Scheme process.

Note that performance-wise, this is among the worst ways to run a web application in production, since the entire application must be loaded into memory when the operating system spawns the CGI process, and all of its startup code run, before the request is handled; but for simpler scripts (and in some development environments) it can work acceptably.

*Rationale:* CGI is very easy to implement and, probably for this reason, is still supported (sometimes through a wrapper over FastCGI) by even very new Web servers like OpenBSD httpd and h2o.

## Encoding issues

### Character encoding of received HTTP data

When the HTTP protocol was designed, character encoding issues were not considered. As a result, the interpretation of bytes values greater than `#x7F` in HTTP method names, paths, and header names and values is ambiguous. [RFC 7230][rfc7230] indicates the sending and receiving of such bytes in these contexts is an obsolete part of the HTTP protocol, replaced by percent-encoding (see below).

[rfc7230]: https://tools.ietf.org/html/rfc7230

For compatibility with older and/or broken client software, when it is required for a server to provide data received from the client in these contexts to the application as a string, the ISO 8859-1 encoding shall be used to decode the received byte data into a string. (That is, every byte in the input shall be interpreted as the Unicode character whose codepoint number is that byte’s value.)

Servers must never send bytes greater than `#x7F` to clients. If an application includes characters with codepoints greater than U+007F in a response header name or value, it may use one of essentially two strategies: it may refuse the response and send a 500 Internal Server Error to the client, or it may attempt (with knowledge of context and where percent-encoding is permitted in HTTP) to UTF-8 percent-encode the data!

### Percent-encoding

When it is desired to include characters in HTTP requests which would otherwise be disallowed in the context in which they are used, it is allowed to use percent encoding of the characters. In percent encoding, every disallowed byte is replaced by a `%` character, followed by a two-byte (canonically uppercase) hexadecimal representation of the original character. For instance, space characters in paths are represented as `%20`.

The interpretation of percent-encoded bytes greater than `%7F` likewise poses problems, although it has since been standardized that these bytes should be interpreted as UTF-8; that is, the sequence `%C3%9F` in a URL represents the character ß (U+00DF). However, for compatibility with older client software, if an attempt is made to decode percent-encoded bytes into a character string using the UTF-8 encoding, but the decoded bytes are not valid UTF-8 for any reason, the attempt should be retried using the ISO 8859-1 encoding.

TODO: I’m not certain if this might actually be a security risk — these kinds of sniffing algorithms often are. Need input from someone with knowledge of these things.

Applications, middleware, and servers must only send percent-encoded UTF-8 to clients in contexts where character data including non-ASCII characters must be sent to the client.

## Acknowledgements

SWAG is rather obviously inspired by the Web application gateway interfaces for Python and Ruby, WSGI ([PEP 3333](https://www.python.org/dev/peps/pep-3333/), [WSGI.org](http://wsgi.org/)) and [Rack](https://github.com/rack/rack). Thanks are due to their original authors, Phillip J. Eby for WSGI and Leah Neukirchen for Rack, and all those who have contributed to those specifications over the years. The [Ring](https://github.com/ring-clojure/ring) interface in Clojure also provided useful examples to follow in some cases.

The extensive collection of middleware and basic server bindings provided by the Rack project inspired the standard provision in SWAG.
