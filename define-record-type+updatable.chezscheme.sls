;; Defines an R6RS ‘equivalent’ to the updatable fields of
;; define-value-type. This exports a new version of define-record-type
;; which supports a new field type declaration:
;;
;; (updatable name)
;; (updatable name accessor updater)
;;
;; This field is declared immutable but provides an ‘updater’
;; procedure of two arguments r and v, which returns a copy of the
;; record r with this field updated to have the new value v. This
;; updater procedure is named ‘<record name>/<field name>’ by default.
;;
;; There are two major pains in the arse here compared to the
;; relatively simple SRFI 9/R7RS small define-value-type macro:
;;
;; 1. The R6RS define-record-type syntax is supposed to look
;;    extensible, but isn’t actually extensible unless you’re either
;;    the maintainer of the core of an implementation, or you’re
;;    willing to go to the absurd kinds of measures this file goes to.
;;    Even then, extensions don’t compose with one another.
;;
;; 2. The Right Thing is that if you call an updater on an instance of
;;    a subtype of the record type which declares one of its fields
;;    updatable, then what you get back should be an instance of that
;;    subtype, with all the subtype instance’s field values intact,
;;    and only the parent type’s field values changed. This is
;;    consistent with non-functional mutators. R6RS’s record system
;;    screws you up here because your subtype might have declared
;;    itself opaque, in which case your updater on the parent type
;;    can’t get to the record-type descriptor of the subtype in order
;;    to access its fields and make a copy of it. Even if we could do
;;    that, R6RS offers no way out of its insistence that you have to
;;    access the fields of each level of a subtype hierarchy
;;    separately, so reconstructing a copy would be slow anyway. So we
;;    have to write an implementation-specific library that goes in
;;    through the back door, ignoring all declarations of opacity,
;;    field mutability etc. to create a copy of any arbitrary subtype
;;    we’re given.

#!chezscheme

(library (define-record-type+updatable)
  (export define-record-type updatable)
  (import (except (chezscheme) define-record-type)
          (rename (only (rnrs (6)) define-record-type)
                  (define-record-type r6rs:define-record-type)))
  (import (only $system
                $record
                $record-type-descriptor
                $record-type-field-count
                $record-ref
                $record-set!))

  (meta define (construct-identifier . components)
    (assert (exists identifier? components))
    (let* ((context-ids (filter identifier? components))
           (names
            (map
             (lambda (c)
               (cond ((identifier? c)
                      (symbol->string (syntax->datum c)))
                     ((symbol? c)
                      (symbol->string c))
                     (else
                      (assertion-violation 'construct-identifier
                                           "component is not a symbol nor identifier"
                                           c))))
             components))
           (symbolic-name
            (string->symbol (apply string-append names)))
           (ret-id
            (datum->syntax (car context-ids) symbolic-name)))
      (if (exists (lambda (id)
                    (not (bound-identifier=? ret-id
                                             (datum->syntax id symbolic-name))))
                  (cdr context-ids))
          (assertion-violation 'construct-identifier
                               "identifiers do not have the same context"
                               context-ids)
          ret-id)))

  (define-syntax updatable (syntax-rules ()))
  (define-syntax define-record-type
    (lambda (stx)
      (syntax-case stx ()
        ((_ name-spec clause ...)
         (let ()
           (define-values (record-name constructor-name predicate-name)
             (if (identifier? #'name-spec)
                 (values #'name-spec
                         (construct-identifier 'make- #'name-spec)
                         (construct-identifier #'name-spec '?))
                 (apply values (syntax->list #'name-spec))))
           (define-values (fields-clauses other-clauses)
             (partition
              (lambda (clause)
                (syntax-case clause (fields)
                  ((fields _ ...) #t)
                  (_ #f)))
              #'(clause ...)))
           (define fields-clause
             (cond ((null? fields-clauses)
                    #'(fields))
                   ((null? (cdr fields-clauses))
                    (car fields-clauses))
                   (else
                    (syntax-violation 'define-record-type
                                      "can only have one fields clause"
                                      stx fields-clauses))))
           (define-values (new-field-specs updater-specs)
             (let loop ((more (cdr (syntax->list fields-clause)))
                        (idx 0)
                        (new-field-specs '())
                        (updater-specs '()))
               (if (null? more)
                   (values (reverse new-field-specs)
                           updater-specs)
                   (syntax-case (car more) (updatable)
                     ((updatable name)
                      (loop (cdr more)
                            (+ idx 1)
                            (cons #'(immutable name) new-field-specs)
                            (cons (cons idx
                                        (construct-identifier
                                         record-name
                                         '/
                                         (syntax->datum #'name)))
                                  updater-specs)))
                     ((updatable name accessor-name updater-name)
                      (loop (cdr more)
                            (+ idx 1)
                            (cons #'(immutable name accessor-name) new-field-specs)
                            (cons (cons idx #'updater-name)
                                  updater-specs)))
                     (_
                      (loop (cdr more)
                            (+ idx 1)
                            (cons (car more) new-field-specs)
                            updater-specs))))))
           #`(begin
               (r6rs:define-record-type
                (#,record-name #,constructor-name #,predicate-name)
                (fields #,@new-field-specs)
                #,@other-clauses)
               (define field-idx-offset
                 (let ((parent (record-type-parent
                                (record-type-descriptor #,record-name))))
                   (if parent
                       ($record-type-field-count parent)
                       0)))
               #,@(map
                   (lambda (spec)
                     (define update-idx (car spec))
                     (define name (cdr spec))
                     #`(define (#,name old-record new-value)
                         (assert (#,predicate-name old-record))
                         (let* ((rtd ($record-type-descriptor old-record))
                                (rtd-size ($record-type-field-count rtd)))
                           (define new-record
                             (case rtd-size
                               ((1) ($record rtd #f))
                               ((2) ($record rtd #f #f))
                               ((3) ($record rtd #f #f #f))
                               ((4) ($record rtd #f #f #f #f))
                               ((5) ($record rtd #f #f #f #f #f))
                               (else
                                (apply $record rtd (make-list rtd-size)))))
                           (let loop ((idx 0))
                             (unless (>= idx rtd-size)
                               ($record-set! new-record
                                             idx
                                             ($record-ref old-record idx))
                               (loop (+ idx 1))))
                           ($record-set! new-record
                                         (+ field-idx-offset #,update-idx)
                                         new-value)
                           new-record)))
                   updater-specs))))))))
