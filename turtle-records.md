# Turtle records

> धृतवक्त्रसरीसृपो ऽपि गृध्रः प्रहरं तिष्ठति खे ऽल्पवीर्य एवं ।  
> गगने न कथं स कूर्मरूपः प्रतिकल्पं धृतभूरचिन्त्यशक्तः ॥

The goals of this proposal are very similar to some previous thoughts of mine on record types. The idea is to create a base record system which, while not necessarily very featureful or ergonomic in itself, can be the basis for people to implement [whatever record type system features they like](https://codeberg.org/scheme/r7rs/wiki/Record-system-features) with correct semantics and without losing too much efficiency compared to native implementations of these features. In particular, no efficiency should be lost for very common record operations (instance creation, type testing, field access and mutation). For less common operations (such as inspection) the asymptotic complexity should be the same as a native implementation, but the constant factors may be worse.

As such this proposal divides the notion of a ‘record system’ into two parts:

- the base record system (कूर्मः or turtle layer) is part of the Scheme Foundations; it primarily provides features for the implementation of high-level record systems, and is not intended to be used in its fulness directly, but since it subsumes SRFI 9/R7RS small records it is nonetheless suitable for the very simplest of applications
- the high-level or userspace record systems (अष्टदिग्गजाः or elephants layer) provide additional features by wrapping the features provided by the base record system; this may include, for example, an implementation of the R6RS record system in terms of the base record system

Specifically, the base record system provides the following features:

- Syntactic record type definitions
- Single inheritance, including the ability to seal a record type from further inheritance using Scheme’s standard lexical scoping and library export control
- Inspection, including the ability to prevent record instance inspection using Scheme’s standard lexical scoping and library export control

It does not provide a procedural record type creation layer, although a later version of this proposal could add one. In the meanwhile, one can be made using the `eval` procedure and the syntactic record type definition system.

Of the features of the R6RS record type system, the base record system proposed here also does not provide encapsulation of parent types’ field structure under single inheritance, nor nongenerativity of record type definitions, but these features can be provided by high-level or userspace record systems.

ISSUE: Should nongenerativity be provided by the base system? What happens if two record types with similar field structure are defined in two distinct record systems? Can they be inspected in both? Could that feature (one record type in multiple systems) be provided to generative record types somehow as well?

Finally, it does not provide integration of record instances with the `write`, `equal?`, and `equal-hash` procedures. A future version of this proposal, or a different proposal entirely, may add this feature.

Note the use of the modal verb ‘could’ in this document. This is not a final proposal for an API; the suggestion of an API is merely demonstrative of the underlying idea.

## Record systems

ISSUE: Consider whether ‘record schemes’ would be a better name. (Conflicts with SRFI 57.)

A record system is a growable set of record types, initially empty. A record system’s public interface consists of a system descriptor and an inspection procedure. Whoever has access to the system descriptor can directly create a record type belonging to the record system. Whoever has access to the inspection procedure can take a record instance belonging to the record type and retrieve the record type descriptor for the type of that record. Thus, creation of subtypes can be restricted to those who have access to the system descriptor (those who have direct access to it can provide indirect access to restrict subtyping to follow certain conditions); and retrieval of information about record types based on their instances can be restricted to those who have access to the inspection procedure.

Record systems could be defined using syntax such as the following.

### `(define-record-system <<descriptor name>> <<inspection procedure name>>)` — syntax

SYNTAX: `<<Descriptor name>>` and `<<inspection procedure name>>` are both identifiers. `Define-record-system` is a definition form.

SEMANTICS: Each evaluation of a `define-record-system` form creates a new record system, and binds `<<descriptor name>>` as a variable to the system descriptor and `<<inspection procedure name>>` as a variable to the inspection procedure for that record system.

The inspection procedure is a procedure of one argument which returns the record type descriptor (see below) for a record instance if that record instance’s type belongs to the created record system. If its argument is not a instance of some record type which belongs to the created record system, it returns `#f`.

## Record types

A record type is a growable set of record instances and a growable set of other record types (its subtypes), initially both empty.

A record type consists of a number of fields, each of which may be mutable or immutable. These fields become locations in each record instance belonging to the record type. They also implicitly become fields in all record types which are subtype of the record type, to which each subtype may add its own additional fields.

The main public interface to a record type in itself is a record type descriptor. Its interface also consists of: a constructor procedure, which creates record instances belonging to the record type; a predicate procedure of one argument, which returns `#t` (in constant time) if its argument is a record instance belonging to the record type or one of its subtypes; as many field accessor procedures of one argument each as there are fields in the record type, which each return the value in the corresponding field of a record instance; and as many field mutator procedures of two arguments each as there are mutable fields in the record type, which each set the value in the corresponding field of a record instance (its first argument) to the new value given as its second argument.

Whoever has access to a record type descriptor – whether directly from its definition or by retrieving it through the inspection procedure of the record system – can create a subtype of that record type. Those who have direct access to a record type descriptor and the components of a record system can offer indirect access to subtyping and restrict its availability according to certain conditions.

Record types could be defined using syntax such as the following.

### `(define-record-type <<descriptor clause>> <<constructor clause>> <<predicate name>> <<field clause>> ...)` — syntax

SYNTAX: The `<<constructor clause>>`, `<<predicate name>>`, and `<<field clause>>`s are as in the corresponding parts from SRFI 9/R7RS small. There is no requirement that field tags be unique, but the field tags as named in the `<<constructor clause>>` should be the same in the sense of `bound-identifier=?` as the corresponding field tags in the `<<field clause>>`s.

The `<<descriptor clause>>` is either an identifier `<<record name>>`, a 2-list `(<<record name>> <<system descriptor expression>>)`, or a 3-list `(<<record name>> <<system descriptor expression>> <<parent type descriptor expression>>)`. `<<System descriptor expression>>` and `<<parent type descriptor expression>>` must be expressions. For maximum optimizability, they should either be identifiers which are respectively the `<<descriptor name>>` and `<<record name>>` identifiers from other `define-record-system` and `define-record-type` forms, or at least should be forms which can be trivially partially evaluated to such identifiers.

SEMANTICS: If the `<<descriptor clause>>` consists only of a `<<record name>>`, the semantics are equivalent to a SRFI 9 record type definition. The `<<record name>>` is bound to a record type descriptor, but the system descriptor and inspection procedure for the system to which it belongs are not exposed, meaning the record type is effectively sealed and opaque.

If the `<<descriptor clause>>` has the form `(<<record name>> <<system descriptor expression>>)`, the `<<system descriptor expression>>` must evaluate to a system descriptor. The `<<record name>>` is bound to the record type descriptor for a new record type in that system, which is not the subtype of any other type. The created record type can thus be subtyped with the system descriptor, and the record type descriptor can be retrieved from a record instance using the inspection procedure of the record system.

If the `<<descriptor clause>>` has the form `(<<record name>> <<system descriptor expression>> <<parent type descriptor expression>>)`, the  `<<parent type descriptor expression>>` must additionally evaluate to the record type descriptor for another record type within the system described by the system descriptor. The newly-created record type is a direct subtype of that record type. There must be at least as many fields in the `<<constructor clause>>` and `<<field clause>>`s as the total number of fields in the ancestor types. The `<<field clause>>`s for those fields consist only of a `(<<field tag>>)`. The `<<field clause>>`s for any additional fields which form part of the subtype have the same form as in SRFI 9/R7RS small.

ISSUE: Should the field tags be required to be `bound-identifier=?` to those that were given when the ancestor record type(s) were created? That would be an extra guarantee against doing the wrong thing, and fairly easy to maintain given you’re supposed to only use subtyping through a higher-level/userspace record system.

## How to add additional features

The turtle layer’s record type descriptors publically expose no information whatsoever about a record type; they are used only to control access to subtyping, but their identity in the sense of `eq?` makes them useful as a key into a hash table, for example, which would map them to the more useful record type descriptors defined by a particular userspace record system at the elephants layer. Those objects could save the constructor, predicate, and field accessor and mutator procedures to make them available to a procedural layer of its own design, for example; due to the fact that it has complete control over the public interface to them, it can restrict access or apply type checks on fields in a way that does not break under access to a procedural or inspection layer.

ISSUE: I think the idea of using hash tables for this kind of extension breaks phasing. It might be better to also be able to associate some kind of payload with the system descriptor and type descriptors themselves, like the type payloads of SRFI 137.

Identifier properties can additionally be used to make the syntactic layers of userspace/elephant record-type systems exactly as efficient as direct use of the turtle layer.

## Acknowledgements

Thanks to Jonathan Rees for his dissertation [‘A Security Kernel Based on the Lambda Calculus’](https://mumble.net/~jar/pubs/secureos/) which demonstrates that record instance opacity and record type sealing are, like other security features, simply applications of lexical scoping; and to Christine Lemmer-Webber for suggesting using this as a basis to support these features in the Scheme record type system. It took me embarassingly long to work out how to do it right! (It will probably be a few weeks or months of twiddling yet before I’m convinced that this is the right way to do it; or else I will eventually notice that this is tragically wrong, and once again go back to the drawing board.)
