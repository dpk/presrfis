# A purely-functional approach to predicate-based generic functions

## Specification of `generic-lambda` (syntax)

A `generic-lambda` expression creates a generic procedure much as `case-lambda` creates a variadic Scheme procedure. A generic procedure is a generalization of a variadic procedure which can choose which can choose clauses of its implementation based not only on the number of its arguments, but also on their types. A generic procedure can also extend other generic procedures by providing additional clauses which specify the behaviour of the procedure with regards to additional types or specify different behaviour for particular subtypes of a type already specialized.

### Syntax

`(generic-lambda <<specialization clause>> ... <<extension clause>> ...)` <br>
`(generic-lambda <<specialization clause>> ... <<extension clause>> ... else <<does not understand clause>>)`

A `<<specialization clause>>` is `(<<specializing formals>> <<body>>)`.

`<<Specializing formals>>` are one of

> `(<<specializing formal>> ...)` <br>
> `(<<specializing formal>> ... . <<formal>>)` <br>
> `<<formal>>`

A `<<specializing formal>>` is an identifier (a `<<formal>>`) or `(<<predicate expression>> <<formal>>)`.

An `<<extension clause>>` is an `<<expression>> <<ellipsis>>`.

A `<<does not understand clause>>` is a `(<<formals>> <<body>>)` clause as in `case-lambda`.

### Semantics

In a generic procedure with no extension clauses, the leftmost specialization clause whose formals completely match the number and type of the arguments is selected. If there is no such clause, the arguments are passed to the `<<does not understand>>` clause. If no `<<does not understand>>` clause is provided, a default one is supplied which simply signals a domain error.

If a generic procedure extends multiple other generic procedures which in turn each extend a single original generic procedure, the clauses unique to each are checked (in left-to-right order of which extension clause appeared first) before any of the original generic procedure’s clauses are checked.

(explain more)

### Note

Specializing formals which do not include any predicate expressions for any of the individual formals can effectively make the does not understand clause unreachable even within generic procedures which extend this generic procedure. This is always the case if the `<<specializing formals>>` consist only of one `<<formal>>`. This allows generics to effectively forbid redefining their does-not-understand clause within extensions.

### Issues

The left-to-right order of clause matching in `generic-lambda` is the opposite to that used by Common Lisp generic functions, where (aiui) later definitions override earlier ones (except that Common Lisp also has a notion of subtyping which `generic-lambda` carefully avoids formalizing, so actual redefinitions are rare). The left-to-right convention matches other similar Scheme forms including `case-lambda`.

## Specification of `next-applicable-clause` (syntax parameter)

Within the scope of the `<<body>>` in a `<<specialization clause>>` or `<<does not understand clause>>` of a `generic` expression, the `next-applicable-clause` syntax keyword is parameterized as follows.

### Syntax

`(next-applicable-clause <<ellipsis>>)` <br>
`(next-applicable-clause <<expression>> ...)`

In the first form, `next-applicable-clause` expands into a procedure call which has the effect of continuing the search for clauses whose specializing formals, beginning with the clause conceptually to the right of the clause currently selected. That clause will then be entered with the same arguments as those passed to the originally-selected clause.

In the second form, `next-applicable-clause` will expand into a procedure call with the same effect of continuing the search for a clause to select, but the results of evaluating the given `<<expression>>`s in an unspecified order will be taken as the arguments which will be used to find a new clause to select, and those arguments will be used to call that clause.

Note that `next-applicable-clause` is not an early return form. The result of calling the new selected clause will be returned to the originally-selected clause. If the new selected clause raises an error, the original clause will also be able to catch it.

### Issues

Bikeshed the name, it’s terrible.

## A calculus of generic procedures

The assumption underlying this formulation of generic procedures is that the ability to override is a sufficient means of providing correct subtyping semantics: that within a single `generic` expression, subtype specializations will be defined before their supertypes, or (more likely) that a `generic` expression will be used to extend another generic procedure to add subtype-specific behaviour for a type whose supertype already existed on the generic procedure being extended. (What a mess of a sentence.)

However, the does not understand clause can be used to invert this order if a supertype’s behaviour for some reason has to be specified after that of the subtype. (explain)
