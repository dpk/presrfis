(import :gerbil/expander/stx)
(import <expander-runtime>)

(import :std/srfi/151)
(import :scheme/comparator)


(define (bound-identifier<? xid yid)
  (if (bound-identifier=? xid yid) #f ;; sanity check
      (let ((xname (syntax->datum xid))
            (yname (syntax->datum yid)))
        (if (eq? xname yname)
            (let ((xctxhash (eq?-hash (stx-identifier-context xid)))
                  (yctxhash (eq?-hash (stx-identifier-context yid))))
              (if (= xctxhash yctxhash)
                  (< (equal?-hash (stx-identifier-marks xid))
                     (equal?-hash (std-identifier-marks yid)))
                  (< xctxhash yctxhash)))
            (string<? (symbol->string xname)
                      (symbol->string yname))))))

(define (bound-identifier-hash id)
  (bitwise-xor (symbol-hash (syntax->datum id))
               (eq?-hash (stx-identifier-context id))
               (equal?-hash (stx-identifier-marks id))))

(define bound-identifier-comparator
  (make-comparator identifier?
                   bound-identifier=?
                   bound-identifier<?
                   bound-identifier-hash))

(define (free-identifier<? xid yid)
  (let ((xe (resolve-identifier xid))
        (ye (resolve-identifier yid)))
    (cond
     ((and xe ye)
      (and (not (eq? xe ye))
           (binding? xe)
           (binding? ye)
           (string<? (symbol->string (binding-id xe))
                     (symbol->string (binding-id ye)))))
     (xe #t)
     (ye #f)
     (else (bound-identifier<? xid yid)))))

(define (free-identifier-hash id)
  (cond ((resolve-identifier id) =>
         (lambda (e)
           (if (binding? e)
               (symbol-hash (binding-id e))
               (default-hash e))))
        (else (bound-identifier-hash id))))

(define free-identifier-comparator
  (make-comparator identifier?
                   free-identifier=?
                   free-identifier<?
                   free-identifier-hash))
