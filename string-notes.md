# Notes on Strings

## Properties of Internal String Representations

- A UTF-8 bytevector with a table of character offsets allows amortized O(1) `string-ref` and O(n) `string-set!`. `string-set!` may need the bytevector to be resized, making it less efficient than actually reallocating the string. UTF-8 interoperates well with most C libraries one might wish to call through an FFI. UTF-8 is at least as memory-efficient as UTF-16 for text in Latin, Greek, Cyrillic, Hebrew, Arabic, and some other minor scripts, but less efficient for all other scripts.
- A UTF-16 bytevector has the same performance characteristics as a UTF-8 bytevector. Many C libraries support UTF-16, and it is the native string representation of JavaScript, Java, and C#, but UTF-16 is not considered by C programmers as often as it used to be.
- A UTF-32 bytevector allows flat O(1) `string-ref` and `string-set!` without a table of character offsets. At least 25% of memory used by strings is wasted for text regardless of the language/script of the text, and usually the wasteage is closer to 50% or even 75% for mostly ASCII texts. UTF-32 support in C libraries is comparatively rare.
- A bytevector with three bytes per character is the most compact encoding which allows amortized O(1) `string-ref` and `string-set!`. On modern Intel CPUs, unaligned memory access costs no more than aligned memory access, so this is even flat O(1) on those machines. I’ve heard GNU Emacs uses this representation internally, but probably nobody else does.
- ‘Flexible string representation’, i.e. using ISO 8859-1 xor UCS-2 xor UCS-4 depending on what the string actually contains, allows flat O(1) `string-ref`, but has the same performance concerns as UTF-8 and UTF-16 for `string-set!`. The representation is theoretically optimal in memory efficiency, but wastes a lot of space when e.g. a mostly-ASCII text contains a single emoji from the Supplementary Multilingual Plane. Few libraries in C support all three representations.
- Using a rope allows amortized O(log n) `string-ref` and `string-set!`, and makes substring sharing cheap, but may add comparatively high constant factors for small strings. It also makes the `char` and `chdr` (`string-head` and `string-tail`) operations reasonably cheap, although in practice Schemers have settled on a vector-like style of working with strings. `string-append` is also very cheap, as are operations like inserting text into the middle of a string (both O(n) with any of the other approaches mentioned here), but in practice the need for these operations in most applications is limited. The rope would have to be flattened into a bytevector to pass a string to a library through an FFI.
- SRFI 135 represents a text (immutable string) as a growable vector of fixed-size immutable chunks, allowing amortized O(1) performance for `text-ref`, and sharing of chunks for higher-level space efficiencies. If this approach were adopted for mutable strings too, it would make `string-set!` amortized O(1) as well. This too must be converted to a bytevector to pass through an FFI.

Tables of character offsets don’t need to cover all characters in a string for amortized O(1) performance: this provides the O(1) efficiency of SRFI 135, but without chunk sharing.

## String Cursors

[SRFI 130][srfi-130] was voted down on the Red and Tangerine ballots. They seem to just be too much of a pain to work with, not to mention that implementing them efficiently requires implementations to adopt a new pointer tag. 

[srfi-130]: https://srfi.schemers.org/srfi-130/srfi-130.html

Also, `string-set!` breaks string cursors. You might as well keep a table of character offsets and just use indexes. Indeed, an efficient-ish implementation of SRFI 13/SRFI 152 is possible in a Scheme implementation with string cursors, I think, by using a table of string cursors, assuming no `string-set!` is used. To be portable, you’d keep a table mapping strings to their respective tables of character offsets. The table would have to have weak keys to be friendly to the garbage collector. It would also be better to build the table lazily, so that it’s only built as far as the last character index that has actually been needed so far.

## Deprecating `string-set!` ?

We are required by WG2’s charter to remain compatible with R7RS small, which by WG1’s charter was, in turn, effectively required to retain `string-set!`. We can’t get rid of `string-set!`, but could we deprecate it like the compatibility procedures in SRFI 125 hash tables?

This wouldn’t get rid of the requirement for it to behave correctly, and for other procedures to behave correctly in its presence, for example.
