# Secure Network Ports

This is a minimal high-level counterpart to [NetworkPortsCowan](https://github.com/johnwcowan/r7rs-work/blob/master/NetworkPortsCowan.md) with SSL/TLS support, made in acknowledgement that a full SSL/TLS interface would be too damned complicated (Python’s SSL module printed is 50 pages just on its own), but that many practical networking applications today require SSL/TLS.

## Secure Network Context

A secure network context is an object of unspecified type which contains the security-relevant configuration of a secure network port, including (for example) which SSL/TLS versions and ciphers are allowed. Two pre-made network contexts are provided. Implementations may provide ways to create others with more advanced configuration.

### `high-security-network-context` (constant)

A secure network context which sets up connections with the settings consistent with the best current practice for high-security connections. At time of writing, this would include requiring at minimum TLS 1.2 support. At a minimum, secure network ports created with this context must signal an error if the certificate provided by the server is expired, for the wrong domain name, or not trusted.

### `no-security-network-context` (constant)

A secure network context which accepts any security information offered by the other party, including any supported TLS version and cipher suite, expired, incorrect, or untrusted certificates, etc.

This is intended for use in applications which need to access TLS-protected resources, but where actual security on the connection is not necessary: for example, when testing and developing applications and accessing them through a local port which uses a self-signed certificate. Information retrieved from secure network ports using the `no-security-network-context` should be considered untrusted in every sense of the word, and programs should prefer using the `high-security-network-context` wherever possible.

## Client procedures

### `(open-secure-network-client ENDPOINT SECURE-CTX)` (procedure)

Analogous to `open-network-client` using the `SECURE-CTX` to negotiate TLS/SSL security before returning the ports.

*Issue:* Should a client certificate optional argument be provided?

## Server procedures

### `(make-secure-network-listener ENDPOINT SECURE-CTX CERT PRIVKEY)` (procedure)

Analogous to `make-network-listener` using the `SECURE-CTX` to negotiate TLS/SSL security before returning the ports. `CERT` and `PRIVKEY` are bytevectors containing the certificate and private key for the server in DER format.

### `(open-secure-network-server SECURE-LISTENER)`

Analagous to `open-network-server`.

### `(close-secure-network-listener SECURE-LISTENER)`

Analagous to `close-network-listener`.
