(define-record-type Weak-Table
  (%make-weak-table buckets size comparator)
  weak-table?
  (buckets weak-table-buckets weak-table-buckets-set!)
  (size weak-table-size-upper-bound weak-table-size-upper-bound-set!)
  (comparator weak-table-comparator))

(define (weak-table-hash-function wt)
  (comparator-hash-function (weak-table-comparator wt)))
(define (weak-table-equality-predicate wt)
  (comparator-equality-predicate (weak-table-comparator wt)))

(define (void . o) (if #f #f))
(cond-expand (chibi (define reference-barrier void)))

(define (make-weak-table comparator)
  (%make-weak-table
   (make-vector 23 '()) ;; 23 as initial size is from Chibi’s (srfi 69)
   0
   comparator))

(define (weak-table comparator . initial-entries)
  (let ((wt (%make-weak-table
             (make-vector (max 23 (round (* (length initial-entries)
                                            1/2 3/2)))
                          '())
             0
             comparator)))
    (let loop ((more-entries initial-entries))
      (if (null? more-entries) wt
          (let ((k (car more-entries)) (v (cadr more-entries)))
            (weak-table-set! wt k v)
            (loop (cddr more-entries)))))))

(define (weak-table-bucket-idx wt key)
  (remainder ((weak-table-hash-function wt) key)
             (vector-length (weak-table-buckets wt))))

(define (weak-table-walk-bucket wt idx proc)
  (let ((bucket (vector-ref (weak-table-buckets wt) idx)) (first? #t))
    (if (null? bucket) (void)
        (let loop ((cell bucket) (prev-cell #f))
          (let* ((entry (car cell))
                 (more-entries (cdr cell))
                 (entry-key (ephemeron-key entry))
                 (entry-datum (ephemeron-datum entry)))
            (if (ephemeron-broken? entry)
                (begin
                  (weak-table-size-upper-bound-set!
                   wt (- (weak-table-size-upper-bound wt) 1))
                  (if first?
                      (vector-set! (weak-table-buckets wt) idx more-entries)
                      (set-cdr! prev-cell more-entries))
                  (if (not (null? more-entries))
                      (loop more-entries prev-cell)))
                (begin
                  (proc cell entry-key entry-datum)
                  (reference-barrier entry-key)
                  (set! first? #f)
                  (if (not (null? more-entries))
                      (loop more-entries cell)))))))))

(define (weak-table-set! wt key value)
  (call/cc
   (lambda (ret)
     (weak-table-walk-bucket
      wt (weak-table-bucket-idx wt key)
      (lambda (cell k v)
        (if ((weak-table-equality-predicate wt) k
                                                key)
            (begin
              (set-car! cell (make-ephemeron key value))
              (ret (void))))))
     ;; no early return, so create a new cell
     (weak-table-maybe-resize! wt 1)
     (let ((idx (weak-table-bucket-idx wt key)))
       (weak-table-size-upper-bound-set! wt
                                         (+ (weak-table-size-upper-bound wt) 1))
       (vector-set! (weak-table-buckets wt) idx
                    (cons (make-ephemeron key value)
                          (vector-ref (weak-table-buckets wt) idx)))))))

(define (weak-table-ref/default wt key default)
  (call/cc
   (lambda (ret)
     (weak-table-walk-bucket
      wt (weak-table-bucket-idx wt key)
      (lambda (cell k v)
        (if ((weak-table-equality-predicate wt) k key)
            (ret v))))
     ;; no return, so return default
     default)))

(define *weak-table-sentinel* (list '*no-value*))
(define (weak-table-ref wt key)
  (let ((val (weak-table-ref/default wt key *weak-table-sentinel*)))
    (if (eq? val *weak-table-sentinel*)
        (error "key not found in weak table" key wt)
        val)))

(define (weak-table-for-each proc wt)
  (let loop ((idx 0))
    (if (>= idx (vector-length (weak-table-buckets wt))) (void)
        (begin
          (weak-table-walk-bucket wt idx
                                  (lambda (cell k v)
                                    (proc k v)))
          (loop (+ idx 1))))))

(define (weak-table-gc! wt)
  (weak-table-for-each void wt)
  wt)

(define (weak-table-size wt)
  (weak-table-size-upper-bound (weak-table-gc! wt)))

(define (weak-table-maybe-resize! wt n)
  (if (and (> (+ (weak-table-size-upper-bound wt) n)
              (* (vector-length (weak-table-buckets wt)) 3/2))
           (> (+ (weak-table-size-upper-bound (weak-table-gc! wt)) n)
              (* (vector-length (weak-table-buckets wt)) 3/2)))
      (let* ((new-size (round (* (+ (vector-length (weak-table-buckets wt)) n)
                                 3/2)))
             (new-buckets (make-vector new-size '())))
        (weak-table-for-each
         (lambda (k v)
           (let ((idx (remainder ((weak-table-hash-function wt) k)
                                 new-size)))
             (vector-set! new-buckets idx
                          (cons (make-ephemeron k v)
                                (vector-ref new-buckets idx)))))
         wt)
         (weak-table-buckets-set! wt new-buckets))))
