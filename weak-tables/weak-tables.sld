(define-library (weak-tables)
  (import (scheme base)
          (scheme comparator)
          (scheme ephemeron))
  (export make-weak-table weak-table
          weak-table-size-upper-bound weak-table-size
          weak-table-set!
          weak-table-ref/default weak-table-ref
          weak-table-for-each
          weak-table-gc!)
  (include "weak-tables.scm"))
