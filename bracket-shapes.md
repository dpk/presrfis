# Bracket Shapes

Strawman proposal, 10 March 2022.

## Reader Directive

This proposal adds a new reader directive, `#!brackets`. The term reader directive in the sense of `#!fold-case` and `#!no-fold-case`.

`#!brackets` is followed by any amount of horizontal whitespace, followed by either `()` or `[]` or `{}`, followed by a Scheme identifier. The Scheme identifier must refer to a macro transformer which was already imported into the environment of the file containing `#!brackets`, before the file was loaded into that environment.

## Semantics

When the reader encounters a bracketed expression in quoted or unquoted form, it constructs a syntax object for that expression as usual, as if the brackets were normal Scheme round brackets. (That is, subexpressions are read up to a possible dotted tail followed by close bracket and consed into a syntax list.)

It then looks up the transformer procedure associated, by means of the `#!brackets` reader directive, with the syntactic keyword the style of bracket used for the expression and calls it, passing that syntax list as the argument. The result of reading the bracketed expression is the result of calling the syntax transformer.

## Example

Given `brackets-example.sld`:

```scheme
(define-library (brackets-example)
  (import (scheme base)
          #;syntax-case #;cut #;etc)

  (begin
    (define-syntax disallow-brackets
      (lambda (stx)
        (read-violation "bracket style is disallowed")))

    (define-syntax brackets->cut
      (lambda (stx)
        #`(cut #,@stx))))

  (include "brackets-example.scm"))
```

Then `brackets-example.scm` may contain:

```scheme
;; Forbids square brackets from being used.
#!brackets [] disallow-brackets

;; Use curly braces to create procedures by cut
#!brackets {} brackets->cut

{* _ 2} ;;=> evaluates to a simple number doubling procedure
```

## R6RS compatibility

When R6RS compatibility is used, `#!brackets` for `()` and `[]` are both set by default to a syntactic keyword whose transformer procedure is equivalent to `values`, i.e. the identity procedure. The transformer procedure for `{}` is undefined.

## R7RS compatibility

When R7RS compatibility is used, `#!brackets` for `()` is set by default to a transformer procedure equivalent to `values`. The transformer procedures for `[]` and `{}` are undefined.

## Issues

- Should it even be allowed to redefine reading `()`?
- It may be useful for the syntax transformer procedures to know when they are in a quoted environment.
- Should it be possible to specify brackets transformers in a call to `include` somehow? How?
