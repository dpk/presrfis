# String Proposal

I propose that [SRFI 152](https://srfi.schemers.org/srfi-152/srfi-152.html) be adopted as `(scheme string)` with the following amendments:

- Under the definitions of `string?`, `string-null?`, and `string-length` is added: ‘This operation must take O(1) time.’

- Under the definition of `string-ref` is added:

  > Repeated applications of `string-ref` to the same *string* with different *idx*es must run on average in time no greater than O(|m - n|), where *m* is the *idx* accessed by the previous invocation and *n* is the *idx* argument of the current invocation. The previous invocation for a given *string* may have occurred in a different thread.
  >
  > *Note:* Implementations with amortized O(1) or O(log n) implementations of `string-ref` satisfy this requirement ipso facto.

- Under the heading ‘Mutation’ is added:

  > The following procedures are deprecated. No guarantee is made about their performance characteristics: in particular, invoking these procedures may require the *string* or *to* arguments to be internally copied to accommodate a resize due to different characters having different lengths in the internal representation of strings in memory. Using any of these procedures between two consecutive `string-ref` operations also invalidates the performance guarantees for that procedure.

