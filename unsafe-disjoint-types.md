# Unsafe disjoint types

## Withdrawal notice

**This proposal is withdrawn.**

While the rationale is sound and I continue to look for ways to solve the record type problem which retains the positive quality of minimalism which it describes, without losing performance, this specific idea loses for a few reasons.

First, the memory overhead for precise garbage collectors is worse than I thought; I had kind of been implicitly assuming a conservative garbage collector when I wrote this, and only considered the precise case too late. Most instances of the same type would always have the same number of slots, but that number would likely have to be redundantly stored in each individual instance so that the garbage collector knows how much to scan and how much to free. There are ways around this which would penalize only the performance of instance construction for types which don’t always have the same number of slots, which might be an acceptable sacrifice, but the specific performance impact would be nontrivial and would have to be investigated carefully.

Second, implementing subtyping or a procedural inspection layer in terms of this proposal incurs another one-word-per-instance cost compared to a native implementation which can store subtyping information within its own heap tag, whereas you’d need to add your own secondary heap tag here. I think this cost in itself is acceptable; you might disagree. In combination with the above, though, that makes three memory words of overhead per instance for any record type which supports subtyping or inspection! (One word of ‘real’ heap tag, one word of size, one word of heap tag used by the userland implementation of record typing.) For me, that pushes the cost over the line from acceptability to unacceptability, considering that many record types only have two or three fields anyway – an overhead of more than 100%. (All of this handwringing over a few words of memory may seem like excessive performance paranoia, but multiply it by every record instance in a large and complex program and you have a real problem.)

But the real kicker is losing safety. Obviously, I knew this when I wrote the proposal, but on later reflection it’s too high a price to pay to bake this in to the core of the language. R6RS libraries’ and programs’ safety can be trivially statically evaluated by simply traversing the import graph and seeing if they (transitively) import anything unsafe. Baking an unsafe proposal like this into the core of the language and expecting people to actually use it to implement their own record system would destroy that ability: while you could write a safe high-level interface on top of this, guaranteeing that safety for someone else’s library depends strongly on just trusting their programming competence (and non-maliciousness).

So this doesn’t work.

(Usually I simply remove proposals from this repository when they are withdrawn, but since I had linked to this page elsewhere and I think the rationale is worth keeping, I wrote this explanation instead.)

## Rationale

This SRFI is part of a trilogy of SRFIs exploring the design space of record systems for R7RS large:

- [SRFI 256](https://srfi.schemers.org/srfi-256/srfi-256.html), an extension of [SRFI 9](https://srfi.schemers.org/srfi-9/srfi-9.html), provides a safe high-level interface to create record types with single inheritance;
- [SRFI xyz (Tagged procedures with type safety)](type-safe-tagged-procedures.md) provides a low-level means of creating record instances with procedure-like behaviour;
- this SRFI provides an unsafe, low-level interface for defining records and other disjoint types which can be modelled as an array of storage locations

The history of record type systems in Scheme is described extensively in [SRFI 99](https://srfi.schemers.org/srfi-99/srfi-99.html). Experience since the implementation of record-type definitions through SRFIs [9](https://srfi.schemers.org/srfi-9/), 99, R6RS, and R7RS small has shown that Schemers desire more featureful record systems, but also that correct compositional behaviour for many record system features is poor unless those features are incorporated into the core of the record-type system. Since Scheme places value on a minimal set of composable, core primitives, this is an undesirable approach.

From this perspective, the absolutely minimal feature set of SRFI 9/R7RS small record type definitions – generative, sealed, opaque types with no subtyping – represents a surprisingly good point in the design space. Encapsulation is fully provided by Scheme’s standard lexical scoping and library namespacing; in the absence of any procedural or inspection layers of the type system, there is no way to break the encapsulation. It can be used as the basis of a record-type definition system with additional features, such as [SRFI 253’s](https://srfi.schemers.org/srfi-253/srfi-253.html) record type system with type-checked fields; again, the lack of native subtyping and layers besides the absolutely basic syntactic layer protects the type guarantees provided by this feature from the weaknesses which would arise if a subtyping or procedural record layer allowed creating new constructors or field mutators.

One of the extensions that can be implemented on top of such an utterly minimal record-type system is subtyping, as demonstrated by the R7RS small sample implementation of [SRFI 256](https://srfi.schemers.org/srfi-256/srfi-256.html). Unfortunately, implementing a record system with subtyping on top of the SRFI 9-style system requires an extra indirection to a structure (usually a vector) containing the field values. This induces a performance cost: not only because of the additional pointer dereference on accesses, but also because of more allocation, more memory use, worse data locality, and (usually) bounds checking on accesses to the vector, even though generally only a pre-determined fixed set of indexes will be accessed which corresponds to the size of the vector set at record creation time.

Directly providing a means of disjoint vector-like types whose field size is determined at instance creation time, not at type definition time, resolves this problem. Happily, this also reflects the internal interface to record types in many implementations of Scheme. The basic calculus of vector-like disjoint types in Scheme would consist of an operation which creates a constructor procedure for an instance with a given number of slots, a type predicate procedure, and procedures to get or set the value of a slot. While this suffices for a formal semantics, there are two performance problems with this basic calculus when implemented in practice:

- if the number of slots is set at instance construction time, a bounds check on the procedure to get or set slot values is needed to maintain the ordinary Scheme guarantees of safety (guaranteed by R6RS and de facto by most implementations), which is not the case for SRFI 9 and R6RS-style record type definitions where the number of fields can be known at type definition time; and
- if the procedures to set slot values can be invoked on any record at any time, a compiler has no way to know that a record instance is entirely immutable and can be optimized as such

This SRFI proposes slightly relaxing Scheme’s safety guarantee in order to be able to eliminate the bounds check on slot indexes, and adding an additional procedure to this calculus to declare a record instance immutable once its slots have been initialized. Safety is only relaxed where it would otherwise present an obstacle to implementing a higher-level record interface with equivalent run-time efficiency to one such as R6RS’s or R7RS small’s. Furthermore, instance-level declarations of immutability of a type instance are provided, as well as declarations of assumed immutability on accesses to individual slot values. It is also intended that the potentially-unsafe forms defined by this SRFI should not be part of any public API, but rather that other new public APIs should be defined which have other means of ensuring the invariants that guarantee safety are maintained. These new public APIs may include novel high-level record type APIs which provide procedural and inspection layers as well as many other features.

## Specification

Some of the procedures defined by this SRFI are unsafe. When an operation is defined to be unsafe, the behaviour of that operation is undefined (in the sense used by the R7RS large drafts); it is an error (in the sense used by the R7RS small and R6RS reports); and the R6RS guarantee of ‘safety’ explicitly does *not* apply to the operation in that situation. In other words, a program may crash or execute with inconsistent semantics after performing an unsafe operation.

Unsafety in this SRFI is always conditional: no procedure or syntax defined by this SRFI is unsafe in all cases; rather, safety depends on the operation being done with correct parameters. The correctness of parameters for each operation is well-defined. Correctness of parameters is a diachronically-conditioned property which can by affected by prior operations. Each operation has strictly defined parameters of safety and unsafety; implementations must be safe except in those situations in which this SRFI explicitly allows them to be unsafe.

### `(define-disjoint-type <<constructor name>> <<predicate name>> <<ref name>> <<set! name>> <<freeze! name>>)` — syntax

SYNTAX: All of the subforms are identifiers.

SEMANTICS: Every evaluation of a `define-disjoint-type` form creates a new type disjoint from all existing Scheme object types, including types created by any prior evaluation of the same `define-disjoint-type` form. Objects which are instances of types created by this procedure have a number of slots – storage locations, numbered beginning from 0, which can be used as containers for any Scheme object. The number of such slots is determined when an instance is created.

The identifiers which are the subforms of `define-disjoint-type` are bound to five newly-created procedures for creating, accessing, and manipulating instances of this new type. These procedures are specified below under the respective names `CONSTRUCT`, `PREDICATE`, `SLOT-REF`, `SLOT-SET!`, and `FREEZE!`.

This syntax is always safe to compile and to evaluate.

#### `(CONSTRUCT N-SLOTS SLOT-VALUE ...)` — procedure

`N-SLOTS` must be an exact nonnegative integer. The number of `SLOT-VALUE` arguments must be less than or equal to the value of `N-SLOTS`. Otherwise, an assertion violation is raised.

Returns a newly-created instance of the created type with `N-SLOTS` slots, where the first slots (i.e. those numbered beginning from 0) are initialized to the given `SLOT-VALUE`s. If `N-SLOTS` is smaller than the number of `SLOT-VALUE`s, the remaining slots are uninitialized and must be set with `SLOT-SET!` in order to be safely accessed with the `SLOT-REF` procedure. The newly-created instance is distinct from all existing objects in the sense of the `eqv?`, `eq?`, and `equal?` procedures.

Calls to this procedure are always safe to evaluate.

#### `(PREDICATE OBJ)` — procedure

Returns `#t` if the `OBJ` belongs to the created type, or `#f` otherwise.

Calls to this procedure are always safe to evaluate.

#### `(SLOT-REF INSTANCE K)` — procedure <br> `(SLOT-REF INSTANCE K ASSUMING-IMMUTABLE?)` — procedure

Returns the object in the `K`th slot of the `INSTANCE`. If `INSTANCE` is not an instance of the created type, an assertion violation is raised.

The `ASSUMING-IMMUTABLE?` argument is a boolean value that allows the user of this procedure to specify whether it is safe for the compiler to assume that the value of the slot `K` will not have changed since a previous invocation of the `SLOT-REF` procedure on this `INSTANCE` and `K`, nor will change before a potential future invocation. This allows a compiler to eliminate redundant repeated calls to the `SLOT-REF` procedure. If the `ASSUMING-IMMUTABLE?` argument is not given, it defaults to `#f`. Repeated calls to `SLOT-REF` when the `ASSUMING-IMMUTABLE?` argument is `#t` may return values which do not reflect intervening mutations to the given slot of the given instance.

A call to this procedure is unsafe to evaluate if and only if one or more of the following conditions applies:

- `K` is negative, or is greater than or equal to the value of `N-SLOTS` which was given when the `INSTANCE` was created with the `CONSTRUCT` procedure;
- the value of the `K`th slot of `INSTANCE` has not been initialized by one of the `SLOT-VALUE` arguments passed to `CONSTRUCT`, nor by a call to the `SLOT-SET!` procedure.

#### `(SLOT-SET! INSTANCE K OBJ)` — procedure

Sets the value in the `K`th slot of the `INSTANCE` to `OBJ` and returns unspecified values. If the `K`th slot of the `INSTANCE` was not previously initialized, it is safely initialized after calling this procedure and may be accessed with `SLOT-REF`. If `INSTANCE` is not an instance of the created type, an assertion violation is raised.

A call to this procedure is unsafe to evaluate if and only if one or more of the following conditions applies:

- `K` is negative, or is greater than or equal to the value of `N-SLOTS` which was given when the `INSTANCE` was created with the `CONSTRUCT` procedure;
- the `INSTANCE` has previously been frozen by an invocation of the `FREEZE!` procedure

#### `(FREEZE! INSTANCE)` — procedure

Calling this procedure on an instance of the created type declares to the compiler that no further mutations of the instance’s slots with the `SLOT-SET!` procedure will occur. It does not guarantee that future calls to the `SLOT-SET!` procedure on the instance will signal any kind of error: that operation then becomes unsafe on the instance. Repeated calls to `FREEZE!` on the same instance are a no-op. If `INSTANCE` is not an instance of the created type, an assertion violation is raised.

Calls to this procedure are always safe to evaluate, but calling this before all slots in the instance have been initialized will make accessing or mutating those fields permanently unsafe.

RATIONALE: A compiler can easily recognize that a procedure which constructs a new instance, initializes its slot values, and then immediately thereafter freezes the instance can safely be evaluated at compile time, enabling compile-time instantiation and other optimizations only possible when the compiler knows that a value is completely immutable.

## Example

The following example implements Scheme’s standard vector type, with bounds checking but without lexical syntax, as a disjoint type.

```scheme
(library (vectors)
  (export make-vector
          vector?
          vector-ref
          vector-set!)
  (import (except (rnrs)
                  make-vector
                  vector?
                  vector-ref
                  vector-set!)
          (srfi :xxx this-srfi))

  (define-disjoint-type
    %make-vector vector? %vector-ref %vector-set! %vector-freeze!)

  (define (make-vector k fill)
    (let ((vec (%make-vector (+ k 1) k)))
      (let loop ((n 0))
        (if (>= n k)
            vec
            (%vector-set! vec (+ n 1) fill)))))

  (define (vector-ref vec k)
    (assert (< -1 k (%vector-ref vec 0)))
    (%vector-ref vec (+ k 1)))

  (define (vector-set! vec k obj)
    (assert (< -1 k (%vector-ref vec 0)))
    (%vector-set! vec (+ k 1) obj)))
```

The following example implements SRFI 9.

```scheme
(library (srfi :9 records)
  (export define-record-type)
  (import (except (rnrs) define-record-type)
          (only (srfi :1 lists) iota every list=)
          (srfi :xxx this-srfi))

  (define-syntax define-record-type
    (lambda (stx)
      (syntax-case stx ()
        ((_ record-name
            (constructor-name field-tag_1 ...)
            predicate-name
            (field_tag_2 field-accessor-name . maybe-field-mutator) ...)
         (list= bound-identifier=? #'(field-tag_1 ...) #'(field-tag_2 ...))
         (let ((n-fields (length #'(field-tag_2 ...))))
           #`(begin
               (define-disjoint-type
                 primitive-constructor predicate-name field-ref field-set! instance-freeze!)
               (define (constructor-name gen-field-tag ...)
                 #,(if (every (lambda (c)
                                (syntax-case c ()
                                  ((_ _) #t)
                                  (_ #f)))
                              #'((field-accessor-name . maybe-field-mutator) ...))
                       #`(instance-freeze!
                          (primitive-constructor #,n-fields
                                                 gen-field-tag ...))
                       #`(primitive-constructor #,n-fields
                                                gen-field-tag ...)))
               #,@(map
                   (lambda (spec idx)
                     (syntax-case spec ()
                       ((_ accessor)
                        #`(define (accessor rec)
                            (field-ref rec #,idx #t)))
                       ((_ accessor mutator)
                        #`(begin
                            (define (accessor rec)
                              (field-ref rec #,idx #f))
                            (define (mutator rec obj)
                              (field-set! rec #,idx obj))))))
                   #'((field-accessor-name . maybe-field-mutator) ...)
                   (iota n-fields)))))))))
```

TODO: Example demonstrating the implementation of a more sophisticated type system with features comparable to those in SRFI 99 and R6RS.

## Issues for future SRFIs

A minimal disjoint-type system such as this has no way to control its interaction with some standard Scheme procedures. A future SRFI should consider extending this SRFI to allow disjoint types control over the behaviour of `write`, `equal?` and `equal-hash` when used on instances of types defined by this SRFI.

## Implementation

A faithful (i.e. maximally efficient) portable implementation of this SRFI is not possible. Efficient implementations for Chez Scheme 10 and Guile 3.0 can be found in the repository for this SRFI. (TODO) Note that these implementations both depend on internal APIs of those implementations which are not intended to be stable nor public; they may not work on future or previous versions.
