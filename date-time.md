# Dates and Times

Original design: John Cowan and Daphne Preston-Kendal<br>
Specification: Daphne Preston-Kendal <br>
Implementation: Arvydas Silanskas

## Index

- [Design goals and non-goals](#desirable-properties-of-a-date-and-time-library)
- [Initial definitions](#initial-definitions) of timekeeping concepts as modelled in this SRFI
- Data types and operations:
  - [Dates](#dates)
  - [Clock times](#clock-times)
  - [Moments](#moments), measurements of a point in time from a scientific perspective
  - [Timestamps](#timestamps), points in time as recorded for civil timekeeping purposes
  - [Time zones](#time-zones)
  - [Time-deltas](#time-deltas) or durations
  - [Procedures for getting the current time as one of these objects](#the-current-time)
- [Examples](#examples)
- [Acknowledgements](#acknowledgements)

## Desirable properties of a date and time library

- It should make doing the right thing easy, and attempts to do wrong things should look obviously wrong.

- It should be modelled on existing international standards for representing and communicating dates and times, both machine-readably (ISO 8601) and internationally (CLDR).

- It should allow measuring time very precisely. In particular, it should not lose precision as time moves further away from some particular epoch. Small Scheme’s `current-second` procedure gets less precise the further away from 1970 we get.

- It should allow representation of dates and times in the very distant past (specific dates are known to within a single day going back at least 2,500 years) and the very distant future. There should be no limit on the range of dates and times representable which any application, including those dealing with far-past historical events and those dealing with far-future projections, is ever likely to encounter.

- It should not be specific to any calendar system, even though the Gregorian calendar (and, in industrial applications, the derived ISO week calendar) is now effectively a worldwide standard. Fortunately this is not too hard, because humans have been accurately counting the number of sunsets and sunrises for thousands of years, and their methods of organizing their counts of these events into more managable periods of time usually follow strict arithmetic rules. A count of days from a known epoch, and knowledge of the arithmetic rules of each calendar system, is thus sufficient for calendar system neutrality.

- It should be possible to use the date and time library to represent time values from sources including operating system calls, network requests, and file formats both ancient and modern.

- It should be possible to accurately perform date and time arithmetic using rules that make sense to the majority of humans despite the irregularity of calendar systems. E.g. adding 2 years to 1 March 2023 should result in an answer of 1 March 2025, notwithstanding that the year 2024 was one day longer than a common year. Adding 1 month to 31 January should probably result in 28 February, 29 Feburary, or maybe 1 March, because there is no year with a 30 February (except in Sweden in 1712) or 31 February.

### Non-goals, at least for now

- Formatting of dates and times to non-machine-readable formats (i.e. other than ISO 8601 and possibly RFC 822-reduced). Someone should write a library providing locale-sensitive date formatting according to the CLDR data.

## Initial definitions

### The TAI calendar

The TAI calendar is defined as follows. A TAI Gregorian date consists of an integer count of days, and a real count of seconds in the range [0, 86400).

At midnight on 1 January 1958, TAI and UTC were in synch. (That is, they both had the same idea of when midnight on that date was.) This is therefore the epoch of the TAI calendar, at which both the count of days and the count of seconds were 0. (This is equivalent to <i>rata die</i> number 714,780 and Julian Day 2,436,205, with a seconds count of 0 and 43,200 respectively.) Every 86,400 TAI seconds exactly since this epoch, the count of days increases by 1 and the second counter resets to 0. For convenience, the 86,400 seconds in a day of the TAI calendar can be divided up into 24 hours of 60 minutes, each with 60 SI seconds. The count of days can also be divided up into years and months according to any standard arithmetical calendar.

### Nearly accurate UTC

TAI to UTC conversion in the period from 1958 to 1971 is particularly tricky, because during this period UTC was kept approximately in synch with mean solar time by adjusting the length of a second in the UTC scale compared to the TAI scale. A TAI second was an SI second, but a UTC second was fractionally longer; moreover, the amount by which it was longer changed several times to account for changes in the Earth’s rotation. The leap second regime replaced this system in 1972.

Comparing the TAI and UTC data for this period, it is possible to generate a table of when the IERS would have inserted leap seconds if that had been the way they were keeping atomic time and mean solar time in approximate synch during this time. Applying these rules results in a TAI to UTC conversion that is nearly accurate – accurate enough for most computer timekeeping purposes, especially considering that very few computers in this time period had clocks synchronized with atomic sources anyway – and where the difference between TAI and UTC is always an integer except during a leap second, which is easy to detect.

Using this rule, leap seconds are inserted at the end of the UTC dates (that is, an extra second 23:59:60 UTC is inserted in the dates):

- 30 Jun 1959
- 30 Jun 1961
- 31 Dec 1963
- 31 Dec 1964
- 30 Jun 1966
- 30 Jun 1967
- 30 Jun 1968
- 30 Jun 1969
- 30 Jun 1970

This particular set of possible leap second insertions also avoids a potentially problematic leap second insertion immediately before the beginning of the POSIX epoch.

When an implementation of this SRFI is required to convert between TAI and UTC, it may do so using the scientifically correct definition of UTC, or this nearly accurate UTC, for the time period 1958 to 1972. (Implementers should note that when the author once announced her intention to implement the conversion correctly, the universal reaction of timekeeping experts who responded was that the effort would be monstrously difficult and utterly pointless.) Before 1958, UTC and TAI are always considered to be in synch; since neither of them formally existed before 1950, this is a useful fiction which allows modelling of civil times during the pre-atomic era of timekeeping.

### Announcement and addition of leap seconds and changes to time zone rules

Every time a leap second is added or the rules for time zones change, the correspondence between TAI and UTC, and between UTC and local time in a particular location, is changed.

In order to handle changes in the leap second set and in time zone rules, implementations should use the time zone database provided by the operating system where possible. The method by which time zone database information is found is implementation-specified. Furthermore, in case of a long-running program, they should reload the database at least once every 24 hours in order to pick up any upgrades, and should use the reloaded version for all subsequent conversions between TAI and UTC or UTC-derived time zones, including for objects created before the reload.

If this is not possible, implementations may use their own database of time zones and leap seconds.

## Dates

Date objects represent a simple count of days from some unspecified epoch. They are independent of any time zone. The same date may, depending on the location, refer to a period of 24 hours which does not overlap at all the 24 hours to which that date refers in another location. (This is because some time zones in the Pacific Ocean are more than 12 hours offset from the meridian.)

RATIONALE: Many events are defined as taking place on a certain date, regardless of what time zone an observer happens to be in. 20 April 2025 is Easter of the Western Church in 2025, regardless of whether one celebrates it in Hawaii at UTC-10:00 or 2,000 km further south in Kiribati at UTC+14:00, which do not overlap at all. Other such events include birthdays and anniversaries, as well as national holidays in countries which span more than one time zone: the <i lang=fr>fête nationale française</i> is celebrated annually on 14 July in UTC-10:00 in French Polynesia, and in UTC+12:00 in Wallis and Futuna, which overlap by only two hours. The date object is intended to represent the dates of such events.

The date procedures are defined with reference to Edward M. Reingold and Nachum Dershowitz (2018) <cite>Calendrical Calculations: The Ultimate Edition</cite>; each procedure which has a corresponding formula in that book names that formula within its specification. In Reingold and Dershowitz, dates are represented by the ‘fixed’ data type, which in their Common Lisp implementation is simply an exact integer.

The procedures and procedure names here assume use of the Gregorian calendar for both its proleptic and its current era, and indeed for any potential future era in which the Gregorian calendar is superseded. However, this need not be intrinsic in the representation nor the use of date objects: those who wish to provide procedures for other calendar systems should use standard date objects, using one of the procedures which converts a date object to a count of days from an epoch and applying their own calendar system’s arithmetic to it, with a constructor which uses one of the procedures to convert a count of days to a date object.

In principle there is no reason for the range of dates supported by an implementation to be limited at all: a simple count of days can fit in a bignum. Implementations should support a practically unlimited range of dates. However, implementations must support all dates within the current Julian period (4713 BC to AD 3267 inclusive). Assuming a rata die-based representation, this requires at least a 22 bit signed integer to represent the count of days.

A date object corresponds to a complete representation of calendar date in ISO 8601 (section 5.2.2.1 of ISO 8601:2019). Accessors are also provided for complete representations of week dates (section 5.2.4.1 of ISO 8601:2019).

### `(make-date YEAR MONTH DAY)` — procedure

`YEAR`, `MONTH`, and `DAY` must be exact integers, subject to the requirements listed below.

Return a new date object for the date which is represented in the Gregorian calendar by the given `YEAR`, `MONTH`, and `DAY`. The `YEAR` is interpreted in the sense of ISO 8601 (so that 1 BC is `YEAR` 0). `MONTH` is numbered starting from 1 for January, and `DAY` for the first day of each month.

An assertion violation is signalled if `MONTH` is not in the range [1, 12], or `DAY` is not positive or is more than the number of days in the given `MONTH` of the given `YEAR`.

(Reingold and Dershowitz `fixed-from-gregorian`; `gregorian-leap-year?` is needed to implement the check that `DAY` is sensible for the values of `MONTH` and `YEAR`.)

### `(date? OBJ)` — procedure

Returns `#t` if `OBJ` is a date object, or `#f` otherwise.

### `(date-ymd DATE)` — procedure

Returns three values, the year, month, and day numbers respectively of the date represented by the given `DATE`. The numbering convention is identical to that used by `make-date`, so that the following procedure yields an object which is `date=?` to `d` for all date object values of `d`:

```scheme
(lambda (d)
  (call-with-values
      (lambda () (date-ymd d))
    make-date)))
```

(Reingold and Dershowitz `gregorian-from-fixed`.)

### `(date-year DATE)` — procedure <br> `(date-month DATE)` — procedure <br> `(date-day DATE)` — procedure

Returns, respectively, the first, second, and third values that would result from applying the `date-ymd` procedure to their arguments.

(`date-year` is Reingold and Dershowitz `gregorian-year-from-fixed`. The other two are conveniences for the corresponding values from `date-ymd`, and cannot be calculated independently of one another.)

### `(date->iso-8601 DATE)` — procedure

Returns a newly-allocated string representing the date from the given date object in Gregorian-based ISO 8601 format: the year formatted to at least 4 places, with a leading U+002D HYPHEN-MINUS for years prior to 1 BC; a U+002D HYPHEN-MINUS; the month formatted to 2 places; a U+002D HYPHEN-MINUS; and the day of the month formatted to 2 places. The year, month, and day are printed in decimal with leading 0s if needed for padding.

Note that some consumers and protocols use profiles of ISO 8601 which do not accept years before 1 BC or after AD 9999.

NOTE: No inverse of this or any other ISO 8601 formatting procedure is provided, because in practice the complete ISO 8601 specification offers very many possible formatting variants and usually only a subset or profile of these is used in any specific protocol. Future SRFIs, or non-SRFI libraries, should provide parsers for these profiles.

### `(date-weekday DATE)` — procedure

Returns the day of the week for the date represented by the given date object as an exact integer in the range [0, 6] where 0 is Sunday.

ISSUE: Should there really be both this and `date-iso-weekday`?

(Reingold and Dershowitz `day-of-week-from-fixed`.)

### `(date-iso-week DATE)` — procedure

Returns the ISO week number for the date represented by the given date object.

(The `week` from Reingold and Dershowitz’s `iso-from-fixed`.)

### `(date-iso-week-year DATE)` — procedure

Returns the year number in the ISO week calendar for the date represented by the given date object.

(The `year` from Reingold and Dershowitz’s `iso-from-fixed`.)

### `(date-iso-weekday DATE)` — procedure

Returns the day of the week for the date represented by the given date object as an exact integer in the range [1, 7] where 1 is Monday.

ISSUE: Should there really be both this and `date-weekday`?

(The `day` from Reingold and Dershowitz’s `iso-from-fixed`.)

### `(date->mjd DATE)` — procedure

Returns the Modified Julian Day number corresponding to the date represented by the given date object, as an exact integer. The Modified Julian Day is a simple count of days beginning from a standard epoch of 17 November 1858 in the Gregorian calendar, which is Modified Julian Day 0.

(Reingold and Dershowitz `mjd-from-fixed`.)

### `(mjd->date MJD)` — procedure

`MJD` must be an exact integer.

Returns a date object for the date represented by the Modified Julian Day number `MJD`.

(Reingold and Dershowitz `fixed-from-mjd`.)

### `(date->rata-die DATE)` — procedure

Returns the Rata Die number corresponding to the date represented by the given date object, as an exact integer. The Rata Die is a simple count of dates beginning from a standard epoch of 1 January in the year AD 1 of the proleptic Gregorian calendar, which is Rata Die 1.

(This is the ‘fixed’ type used consistently as a lingua franca between calendar systems by Reingold and Dershowitz.)

### `(rata-die->date RD)` — procedure

`RD` must be an exact integer.

Returns a date object for the date represented by the Rata Die number `RD`.

### `(date=? DATE_1 DATE_2)` — procedure <br> `(date<? DATE_1 DATE_2)` — procedure <br> `(date<=? DATE_1 DATE_2)` — procedure <br> `(date>? DATE_1 DATE_2)` — procedure <br> `(date>=? DATE_1 DATE_2)` — procedure

These procedures measure whether one date is before, the same as, or after another. `Date=?` returns `#t` if the two date objects it is given refer to the same date and `#f` otherwise. `Date<?` returns `#t` if the date represented by the date object `DATE_1` is strictly before the date represented by `DATE_2`. The other procedures are defined in the usual way accordingly.

ISSUE: Variadic?

## Clock times

A clock time represents the time of day independently of any time zone. As with dates, some events are scheduled at certain times of day no matter which time zone the observer of that event happens to be in. ‘Babylonian time’, in its UTC-adjusted variant, is even more universal than the Gregorian calendar; no particular accommodation is made for alternative clock systems, though it should be relatively easy to convert from any astronomical notion of hours to this variant.

A clock time object has three components, respectively the hour of the day, minute of the hour, and second of the minute. The hour is in the range [0, 24); the minute in the range [0, 60), and the second in the range [0, 61). The hour and minute are returned as exact integers, and the second as an exact rational.

A clock time object corresponds to a complete representation of a local time of day in ISO 8601 (section 5.3.1.2 of ISO 8601:2019), potentially with a fractional part of a second (section 5.3.1.2.a of ISO 8601:2019).

### `(make-clock-time HOUR MINUTE SECOND)` — procedure

### `(clock-time-hms CLOCK-TIME)` — procedure

### `(clock-time-hour CLOCK-TIME)` — procedure <br> `(clock-time-minute CLOCK-TIME)` — procedure <br> `(clock-time-second CLOCK-TIME)` — procedure

## Moments

Moments are instants within the TAI timescale, represented using the TAI calendar — that is, they can be decomposed into a date and a count of seconds, although (as of writing, and for the foreseeable future) that count of seconds will appear to be going slightly fast compared to UTC-based civil time (for which see next section).

The count of seconds is a nonnegative exact rational in the range [0, 86,400). Moments can therefore be measured with arbitrary precision, although computer time-keeping is rarely done with more than nanosecond precision, and is rarely accurate to more than a few milliseconds.

In order to discourage mistaken use of moments to display time values intended to be civil times, direct accessors for the Gregorian calendar values of year, month, and day are not provided.

NOTE: ISO 8601 does not define a serialization form for dates on the TAI timescale, because TAI is officially a count of seconds with no notion of minutes, hours, or days.

ISSUE: I hoped to include serialization to and from djb’s tai64n notation, but there are two conflicting specs. One page claims the epoch (`@400000000000000000000000`) is midnight in TAI on 1 January 1970; another claims it’s 00:00:10 in TAI on 1 January 1970 (which is similar to nearly accurate UTC, but assumes that |TAI − UTC| was always 10 even before the first leap second on 30 June 1972).

### `(make-moment DATE SECONDS)` — procedure

`SECONDS` is an exact rational.

Returns a moment corresponding to `SECONDS` seconds into the given `DATE` in the TAI calendar.

### `(moment? OBJ)` — procedure

Returns `#t` if `OBJ` is a moment object, or `#f` otherwise.

### `(moment-date MOMENT)` — procedure

Returns a date object for the day in the TAI calendar (as defined above) represented by the given moment object.

### `(moment-second-of-day MOMENT)` — procedure

Returns an exact rational in the range [0, 86,400) representing the second of the day in the TAI calendar which the given moment object represents.

### `(moment=? MOMENT_1 MOMENT_2)` — procedure <br> `(moment<? MOMENT_1 MOMENT_2)` — procedure <br> `(moment<=? MOMENT_1 MOMENT_2)` — procedure <br> `(moment>? MOMENT_1 MOMENT_2)` — procedure <br> `(moment>=? MOMENT_1 MOMENT_2)` — procedure

These procedures measure whether one moment is before, the same as, or after another. `Moment=?` returns `#t` if the moment objects `MOMENT_1` and `MOMENT_2` represent exactly the same date and second measurement in the TAI calendar. `Moment<?` returns `#t` if either the date measurement in `MOMENT_1` is the less than the date measurement in `MOMENT_2`, or if the date measurement in `MOMENT_1` is the same as the date measurement in `MOMENT_2` but the seconds measurement in `MOMENT_1` is less than the seconds measurement in `MOMENT_2`. The other procedures are defined in the usual way accordingly.

ISSUE: Variadic?

## Timestamps

Timestamps are what humans usually think of as dates and times. They are instants within a specific local timescale which, for the sake of convenience, is taken to be derived from TAI by way of UTC. (In some places, the official local time zone is defined by reference to mean solar time, but computer time-keeping invariably ignores the distinction.)

Every timestamp corresponds unambiguously to a single moment. Representationally, timestamps are a combination of a date with an hour in the range [0, 24), a minute in the range [0, 60), and a second in the range [0, 61), plus a timezone. The timezone may be interpreted in two ways to yield an unambiguous moment: either as a simple offset from UTC applicable at the corresponding moment of the timestamp; or as a dynamic set of rules applicable .

Although procedures to compare the corresponding moments of two timestamps are theoretically unambiguous, they are not directly provided because of the unclarity of the `timestamp=?` procedure: should it consider two timestamps the same if they have the same corresponding moment even if their time zones are different? Considered in the context of the less-than or greater-than operators, it seems so; but strictly, those timestamps are not the same as one another. So users must explicitly convert to moments and use the moment comparison procedures.

A timestamp object corresponds to a complete representation for calendar date and time of day in ISO 8601 (section 5.4.2.1 of ISO 8601:2019); a timestamp always includes a time zone component, which, as an extension to ISO 8601, may consist of information about the periods of validity of time shift designators for producers or consumers of date and time information.

### `(make-timestamp YEAR MONTH DAY HOUR MINUTE SECOND TIMEZONE)` — procedure

### `(date+clock-time->timestamp DATE CLOCK-TIME TIMEZONE)` — procedure

### `(timestamp? OBJ)` — procedure

### `(moment->timestamp MOMENT TZ)` — procedure

### `(timestamp->moment TIMESTAMP)` — procedure

### `(timestamp-in-timezone TIMESTAMP TZ)` — procedure

Perform a time zone conversion on a timestamp, returning a timestamp which reflects the same corresponding moment but with a different time zone. Equivalent to:

```scheme
(moment->timestamp (timestamp->moment TIMESTAMP) TZ)
```

ISSUE: What time zone interpretation should this procedure, and the one above, use?

### `(posix-time->utc-timestamp POSIX-SECONDS)` — procedure <br> `(posix-time->utc-timestamp POSIX-SECONDS NANOSECONDS)` — procedure

Return a timestamp in the UTC time zone corresponding to the given POSIX time (a count of seconds since 1 January 1970, minus leap seconds). If `NANOSECONDS` is not given, `POSIX-SECONDS` may be an exact or inexact rational. If `NANOSECONDS` is given, both `POSIX-SECONDS` and `NANOSECONDS` must be exact integers.

EXAMPLE: Having obtained a POSIX timespec from, for example, a `stat` operation on a file to obtain the last-modified date, the following code will get a timestamp for it in the user’s local time zone.

```scheme
(timestamp-in-timezone (posix-time->utc-timestamp tv-sec tv-nsec)
                       (system-timezone))
```

### `(timestamp->utc-posix-time TIMESTAMP)` — procedure

Returns an exact rational representing the `TIMESTAMP` as a count of seconds since midnight UTC on 1 January 1970, minus leap seconds. The timestamp object may be in any time zone. If the `TIMESTAMP` corresponds to a moment that is within a leap second (i.e. if the result of the `timestamp-second` procedure is greater than or equal to 60), one second is subtracted from the `TIMESTAMP` before the conversion takes place.

NOTE: The fact that the names of this procedure and of `posix-time->utc-timestamp` are not perfect inverses is intended to reflect the fact that the two do not round-trip losslessly because of the leap second problem with POSIX time. Thus, doing a wrong thing (attempting a round trip conversion in the expectation of losslessness) will look wrong.

### `(timestamp-date TIMESTAMP)` — procedure

Return a date object for the date represented by the timestamp object. The returned date object is not guaranteed to be the same as any other date object in the sense of `eqv?` or `eq?`, including date objects returned by previous invocations of this procedure on the same timestamp object.

### `(timestamp-ymd TIMESTAMP)` — procedure <br> `(timestamp-year TIMESTAMP)` — procedure <br> `(timestamp-month TIMESTAMP)` — procedure <br> `(timestamp-day TIMESTAMP)` — procedure

Convenience procedures which are respectively the equivalents of:

```scheme
(date-ymd (timestamp-date TIMESTAMP))
(date-year (timestamp-date TIMESTAMP))
(date-month (timestamp-date TIMESTAMP))
(date-day (timestamp-date TIMESTAMP))
```

### `(timestamp-clock-time TIMESTAMP)` — procedure

Returns a clock-time object representing the hour, minute, and second of the date represented by the timestamp. The returned date object is not guaranteed to be the same as any other clock-time object in the sense of `eqv?` or `eq?`, including clock-time objects returned by previous invocations of this procedure on the same timestamp object.

### `(timestamp-hms TIMESTAMP)` — procedure <br> `(timestamp-hour TIMESTAMP)` — procedure <br> `(timestamp-minute TIMESTAMP)` — procedure <br> `(timestamp-second TIMESTAMP)` — procedure

Convenience procedures which are respectively the equivalents of:

```scheme
(clock-time-hms (timestamp-date TIMESTAMP))
(clock-time-hour (timestamp-date TIMESTAMP))
(clock-time-minute (timestamp-date TIMESTAMP))
(clock-time-second (timestamp-date TIMESTAMP))
```

### `(timestamp-timezone TIMESTAMP)` — procedure

Returns the time zone object which is used to calculate the corresponding moment. Unlike `timestamp-date` and `timestamp-clock-time`, the returned object is guaranteed to be the same in the sense of `eqv?` as the timezone object that was passed to the procedure which originally constructed the timestamp object.

### `(timestamp-fold TIMESTAMP)` — procedure

Return an exact integer 0 or 1 representing the number of times this timestamp has repeated within the timezone (considered as a timezone object, not as an offset) at the corresponding moment. This integer is almost always 0 except immediately following a switch from summer time to winter time. In particular, if the time zone object used in the timestamp consists only of a fixed offset, this procedure always returns 0.

### `(timestamp-timezone-offset TIMESTAMP)` — procedure

Returns a time delta object representing the offset from UTC in effect in the timestamp’s timezone at the corresponding moment.

### `(timestamp->iso-8601 TIMESTAMP)` — procedure

Returns a newly-allocated string containing the date and time specified by the given `TIMESTAMP` together with the offset from UTC in hours, minutes, and possibly seconds.

The first part of the returned string consists of the ISO 8601 formatted date for the timestamp, exactly as if `date->iso-8601` had been called on the result of applying `timestamp-date` to the timestamp object. Then follows a U+0054 LATIN CAPITAL LETTER T; the hour formatted to 2 places; a U+003A COLON; the minute formatted to two places; a U+003A COLON; and the integral part of the second formatted to two places.

If the second is not an exact integer, there follows a U+002E FULL STOP followed by the fractional part of the second. Because seconds in a timestamp are represented as an exact rational, it may not be possible to convert them exactly to a terminating decimal number. `timestamp->iso-8601` shall therefore format seconds to at most nanosecond precision. If the fractional part of the second can be precisely represented in fewer than 9 places, there shall be no trailing zeroes.

Then follows the time zone part, which is formatted as follows. If the time delta object resulting from calling `timestamp-timezone-offset` on the timestamp object is equivalent to a duration of zero seconds, the time zone part shall be a single U+005A LATIN CAPITAL LETTER Z. If the time delta object contains a seconds part or a part for any time unit greater than or equal to 24 hours, an assertion violation is signalled. If the time delta is negative, there follows a U+002D HYPHEN-MINUS, otherwise a U+002B PLUS SIGN; then the hours part of the time delta formatted to two places; a U+003A COLON; and the minutes part of the time delta formatted to two places.

NOTE: The formatting rules described here are compatible with the IETF and W3C date-time formats, except that, as noted under `date->iso-8601`, these profiles of ISO 8601 do not support BC or far-future AD dates. The author of this specification considers this a bug in those profiles. While some historical time zones do include a seconds part, these may not be represented even in the full ISO 8601 format.

See the remark under `date->iso-8601` on why no inverse of this procedure is provided.

## Time zones

A time zone represents an offset from UTC which is used for local clock-times in some specific location, possibly including a dynamic set of rules for determining when a location changes from one offset to another for its local time, either according to standard rules (as for daylight savings/summer time in most of the world) or according to one-off political changes (as for summer time in the United Kingdom before the 1990s, or for more permanent alterations, such as countries in the Pacific Ocean deciding to hop to the other side of the International Date Line).

A time zone object is modelled on entries from the Olson/ICANN tz database, but on operating systems which provide some other means of finding time zone change information it should be possible to provide a useful implementation of timestamps and time zones with the information they provide.

NOTE: When converting between timestamps, which are based on UTC, and moments, which are based on TAI, time zone data from the Olson/ICANN tz database should be taken from the `right` variant of each time zone, not the default `posix` variant. Note that some operating systems do not include the `right` versions of the time zone data; in this case, the `leapseconds` file should provided anyway, and will have to be combined with the default `posix` data.

ISSUE: Should a way to access the name (ICANN name, CLDR name) of a time zone object be provided? What if there is none?

### `(timezone? OBJ)` — procedure

Returns `#t` if `OBJ` is a time zone object and `#f` otherwise.

### `utc-timezone` — constant

A time zone which adds leap seconds to TAI only.

### `(utc-offset-timezone DT)` — procedure

Returns a time zone objects which is consistently offset from UTC by the amount of time specified by the time-delta object `DT`. The new time zone object is guaranteed to be distinct from all existing time zone objects in the sense of the `eqv?` procedure.

RATIONALE: This procedure is intended to be used only in unparsing dates from formats such as ISO 8601, which include an offset from UTC but not location information. Note that any date and time arithmetic done on a timestamp created this way is not guaranteed to correctly reflect local time in the location of the person who generated the time string, because the offset from UTC in that location might be different at the moment for the calculated timestamp than at the moment when the timestamp was created.

### `(system-timezone)` — procedure

Returns a timezone object corresponding to the current operating system or user timezone setting.

### `(tz-timezone NAME)` — procedure

`NAME` must be a string.

Returns a time zone object corresponding to the Olson/ICANN tz database entry for `NAME`. An assertion violation is raised if the time zone `NAME` is not known to the system.

### `(tz-timezones)` — procedure

Returns a list of the names of all Olson/ICANN tz database entries known to the system as strings, i.e. all strings that would be valid argument to the `tz-timezone` procedure, in an unspecified order.

## Time-deltas

A time-delta represents a measurement of an amount of time, although they do not necessarily represent an actual duration of time on the absolute TAI or UTC time scale, except maybe in an abstract sense. (A time-delta used to represent a timezone offset could be interpreted as meaning ‘when it is midnight in UTC, you will have to wait minus this time delta amount of time before it is midnight in that time zone’, but this is very abstruse.)

A time-delta is a product type whose components are years, months, weeks, days, hours, minutes, and seconds. All the components must be exact integers, except for seconds, which must be an exact rational. If the value of one of these components is 0, the time-delta object may also be described as not having that component.

A time-delta object corresponds to a duration in ISO 8601 (section 5.5.2 of ISO 8601:2019).

NOTE: Versions of ISO 8601 prior to the 2019 revision did not have the concept of a negative duration.

### `(dt? OBJ)` — procedure

Returns `#t` if `OBJ` is a time-delta object, and `#f` otherwise.

### `(years-dt X)` — procedure <br> `(months-dt X)` — procedure <br> `(weeks-dt X)` — procedure <br> `(days-dt X)` — procedure <br> `(hours-dt X)` — procedure <br> `(minutes-dt X)` — procedure <br> `(seconds-dt X)` — procedure

Returns a time-delta object with the corresponding number `X` of the unit within the name of the procedure.

### `(dt-years DT)` — procedure <br> `(dt-months DT)` — procedure <br> `(dt-weeks DT)` — procedure <br> `(dt-days DT)` — procedure <br> `(dt-hours DT)` — procedure <br> `(dt-minutes DT)` — procedure <br> `(dt-seconds DT)` — procedure

Returns the value of the corresponding component in the time-delta object `DT`.

### `(dt+ DT ...)` — procedure

Returns a time-delta object which is the sum of the given time-delta arguments. Specifically, each component is summed; no overflow of smaller units to larger ones is calculated.

### `(dt-negate DT)` — procedure

Returns a time-delta object which has all its components negated, yielding a time-delta of equivalent magnitude but opposite direction.

### `(date+ DATE DT)` — procedure

`DT` must be a time-delta object with no hours, minutes, or seconds component.

Calculate the date that results from adding the time-delta `DT` to the date object `DATE` and return a new date object representing that date. The calculation is performed according to Annex D of the ISO 8601-2:2019 specification.

### `(timestamp+ TIMESTAMP DT)` — procedure

Calculate a timestamp that results from adding the time-delta `DT` to the timestamp object `TIMESTAMP`, returning a new timestamp object containing the result. The calculation is performed according to Annex D of the ISO 8601-2:2019 specification.

## The current time

### `(current-moment)` — procedure

### `(current-utc-timestamp)` — procedure

Equivalent to `(moment->timestamp (current-moment) utc-timezone)`.

### `(current-system-timestamp)` — procedure

Equivalent to `(moment->timestamp (current-moment) (system-timezone))`.

## Examples

The following Scheme code demonstrates the use of the epoch procedures on dates to implement constructors for a non-Gregorian calendar; specifically, the French republican calendar in its later, purely arithmetic form. (Implementing the astronomical form is an exercise for the reader – a long, but not particularly intellectually challenging one, for anyone armed with a copy of Reingold and Dershowitz.)

```scheme
(define proclamation-de-la-république (make-date 1792 9 22))

(define (an-bisextil? an)
  (and (= (modulo an 4) 0)
       (not (memv (modulo an 4) '(100 200 300)))
       (not (= (modulo an 4000) 0))))

(define (construire-date an mois jour)
  (rata-die->date
   (+ (date->rata-die proclamation-de-la-république)
      -1
      (* 365 (- an 1))
      (floor-quotient (- an 1)
                      4)
      (- (floor-quotient (- an 1)
                         100))
      (floor-quotient (- an 1)
                      400)
      (- (floor-quotient (- an 1)
                         4000))
      (* 30 (- mois 1))
      jour)))

(define (date-amj date)
  (let* ((rd (date->rata-die date))
         (approx
          (+ (floor (* 4000/1460969
                       (+ (- rd (date->rata-die proclamation-de-la-république))
                          2)))
             1))
         (an
          (if (date<? date (construire-date approx 1 1))
              (- approx 1)
              approx))
         (mois
          (+ 1 (floor (* 1/30 (- rd (date->rata-die
                                     (construire-date an 1 1)))))))
         (jour
          (+ 1 rd (- (date->rata-die (construire-date an mois 1))))))
    (values an mois jour)))

(define (date-an date)
  (let-values (((an mois jour) (date-amj date))) an))
(define (date-mois date)
  (let-values (((an mois jour) (date-amj date))) mois))
(define (date-jour date)
  (let-values (((an mois jour) (date-amj date))) jour))
```

A program or library which uses these procedures to construct and access the day, month, and year of native date objects can interoperate transparently with code which works with other calendar systems.

## Acknowledgements

This SRFI is derived, at some length, from an original proposal by John Cowan.

Thanks to the following people for suggesting new features in the specification: Arthur Glecker (clock-time type); Antero Mejr (ability to create timestamp/moment conversions independently of the standard implementation; explicit note that the method of finding time zone database information is implementation-specific).

Thanks to the following people for finding errors in the specification: Vincent Manis; Marc Nieper-Wißkirchen.

Thanks to the following people for general encouragement: Alex Shinn; Wolfgang Corcoran-Mathe.
