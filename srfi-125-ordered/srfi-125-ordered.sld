(define-library (srfi-125-ordered)
  (import (scheme base)
          (scheme case-lambda)
          (srfi 1)
          (srfi 27)
          (srfi 128)
          (srfi 133)
          (srfi 151)
          (srfi 160 base))
  (export make-hash-table
          hash-table-size
          hash-table?
          hash-table
          alist->hash-table
          hash-table-set!
          hash-table-delete!
          hash-table-clear!
          hash-table-ref
          hash-table-ref/default
          hash-table-contains?
          hash-table-empty?
          hash-table-keys
          hash-table-values
          hash-table-entries
          hash-table-fold
          hash-table-fold-right
          hash-table-for-each
          hash-table->alist
          hash-table-find
          hash-table-count
          hash-table-prune!
          hash-table-union!
          hash-table-intersection!
          hash-table-difference!
          hash-table-xor!
          hash-table-unfold
          hash-table-intern!
          hash-table-update!
          hash-table-update!/default)

  (include "srfi-125-ordered.scm"))
