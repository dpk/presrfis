;; -*- eldoc-documentation-function: eldoc-documentation-default -*-
;; scheme-complete eldoc is bizarrely slow in this buffer

;; utilities which R7RS should provide but doesn't
(define (void . ignored) (if #f #f))

;; record type
(define-record-type Hash-Table
  (%make-hash-table type-test-function hash-function same?-function
                    size next-idx compact-index
                    keys-vector values-vector)
  hash-table?
  (type-test-function hash-table-type-test-function)
  (hash-function hash-table-hash-function)
  (same?-function hash-table-same?-function)
  (size hash-table-size hash-table-size-set!)
  (next-idx hash-table-next-idx hash-table-next-idx-set!)
  (compact-index hash-table-compact-index hash-table-compact-index-set!)
  (keys-vector hash-table-keys-vector hash-table-keys-vector-set!)
  (values-vector hash-table-values-vector hash-table-values-vector-set!))

(define *unfilled*
  (let ()
    (define-record-type Unfilled (make-unfilled) unfilled?)
    (make-unfilled)))

(define (unfilled? obj) (eq? obj *unfilled*))

(define-record-type Deletion
  (make-deletion bucket)
  deletion?
  (bucket deletion-bucket))

(define *growth-rate* 3/2)

;; memory-efficient compact integer arrays
(define (make-compact-array size)
  (cond ((< size #xFF) (make-bytevector size #xFF))
        ((< size #xFFFF) (make-u16vector size #xFFFF))
        ((< size #xFFFFFFFF) (make-u32vector size #xFFFFFFFF))
        (else (make-u64vector size #xFFFFFFFFFFFFFFFF))))

(define (compact-array-ref sa idx)
  (define (max-to n) (lambda (x) (if (= x n) #f x)))
  (cond ((and (bytevector? sa) (bytevector-u8-ref sa idx))
         => (max-to #xFF))
        ((and (u16vector? sa) (u16vector-ref sa idx))
         => (max-to #xFFFF))
        ((and (u32vector? sa) (u32vector-ref sa idx))
         => (max-to #xFFFFFFFF))
        ((and (u64vector? sa) (u64vector-ref sa idx))
         => (max-to #xFFFFFFFFFFFFFFFF))
        (else (error "not a compact array" sa))))

(define (compact-array-set? sa idx)
  (not (not (compact-array-ref sa idx))))

(define (compact-array-set! sa idx val)
  (define (set-with-max setter max)
    (if (>= val max)
        (error "can't store value in compact array, try upgrading it" val)
        (setter sa idx val)))
  (cond ((bytevector? sa)
         (set-with-max bytevector-u8-set! #xFF))
        ((u16vector? sa)
         (set-with-max u16vector-set! #xFFFF))
        ((u32vector? sa)
         (set-with-max u32vector-set! #xFFFFFFFF))
        ((u64vector? sa)
         (set-with-max u64vector-set! #xFFFFFFFFFFFFFFFF))
        (else (error "not a compact array" sa))))

(define (compact-array-delete! sa idx)
  (cond ((bytevector? sa)
         (bytevector-u8-set! sa idx #xFF))
        ((u16vector? sa)
         (u16vector-set! sa idx #xFFFF))
        ((u32vector? sa)
         (u32vector-set! sa idx #xFFFFFFFF))
        ((u64vector? sa)
         (u64vector-set! sa idx #xFFFFFFFFFFFFFFFF))
        (else (error "not a compact array" sa))))

(define (compact-array-length sa)
  (cond ((bytevector? sa)
         (bytevector-length sa))
        ((u16vector? sa)
         (u16vector-length sa))
        ((u32vector? sa)
         (u32vector-length sa))
        ((u64vector? sa)
         (u64vector-length sa))
        (else (error "not a compact array" sa))))

(define *nice-n-buckets*
  '#(2 2 5 5 7 11 11 11 13 17 17 17 19 23 23 23 29 29 29 29 31 37
     37 37 37 41 41 41 43 47 47 47 53 53 53 53 59 59 59 59 61 67
     67 67 67 71 71 71 73 79 79 79 79 83 83 83 89 89 89 89 97 97
     97 97 97))

(define (find-nice-n-buckets k)
  (if (< k (vector-length *nice-n-buckets*))
      (vector-ref *nice-n-buckets* k)
      (let ((limit (+ 1 (floor (* k 3/2)))))
        ;; todo: put a little bit more effort into this
        (if (even? limit)
            (+ 1 limit)
            limit))))

;; the actual hash table bit: fundamental constructor
(define make-hash-table
  (case-lambda
    ((comparator) (make-hash-table comparator 7))
    ((comparator k)
     (%make-hash-table (comparator-type-test-predicate comparator)
                       (comparator-hash-function comparator)
                       (comparator-equality-predicate comparator)
                       0
                       0
                       (make-compact-array (find-nice-n-buckets k))
                       (make-vector k *unfilled*)
                       (make-vector k *unfilled*)))))

;; construct and pre-fill
(define (hash-table comparator . keys-and-values)
  (let ((ht (make-hash-table comparator (/ (length keys-and-values) 2))))
    (apply hash-table-set! ht keys-and-values)
    ht))

(define alist->hash-table
  (case-lambda
    ((alist comparator)
     (alist->hash-table alist comparator (length alist)))
    ((alist comparator k)
     (let ((ht (make-hash-table comparator k)))
       (let loop ((more alist))
         (cond ((null? more) ht)
               ((hash-table-contains? ht (caar more)) (loop (cdr more)))
               (else
                (hash-table-set! ht (caar more) (cdar more))
                (loop (cdr more)))))))))


;; internal helpers
(define (hash-table-hash ht key)
  ((hash-table-hash-function ht) key))

(define (hash-table-same? ht a b)
  ((hash-table-same?-function ht) a b))

(define (%hash-table-bucket-for ht hash key)
  (let loop ((hash (abs hash)))
    (let* ((bucket
            (modulo hash
                    (compact-array-length (hash-table-compact-index ht))))
           (entry-idx
            (compact-array-ref (hash-table-compact-index ht) bucket)))
      (if entry-idx
          (let ((found-key
                 (vector-ref (hash-table-keys-vector ht) entry-idx)))
            (if (and (not (deletion? found-key))
                     (or (unfilled? found-key)
                         (hash-table-same? ht key found-key)))
                bucket
                (loop (+ hash 1))))
          bucket))))

(define (hash-table-bucket-for-key ht key)
  (%hash-table-bucket-for ht (hash-table-hash ht key) key))

(define (hash-table-grow-entries! ht)
  ;; first determine if we actually need to grow the entries arrays,
  ;; or if pruning dead entries would suffice
  (if (<= (hash-table-size ht)
          (* (vector-length (hash-table-keys-vector ht))
             (/ 1 *growth-rate*)))
      (hash-table-prune-dead-entries! ht #f)
      ;; otherwise, actually grow the entries array
      (let* ((new-size
              (floor (* (vector-length (hash-table-keys-vector ht))
                        *growth-rate*)))
             (new-keys (make-vector new-size *unfilled*))
             (new-values (make-vector new-size *unfilled*)))
        (vector-copy! new-keys 0 (hash-table-keys-vector ht))
        (vector-copy! new-values 0 (hash-table-values-vector ht))
        (hash-table-keys-vector-set! ht new-keys)
        (hash-table-values-vector-set! ht new-values)
        (hash-table-prune-dead-entries! ht #f))))

(define (hash-table-prune-dead-entries! ht fast?)
  ;; NB only set fast? to #t if you are going to be rehashing all
  ;; entries anyway!
  (unless #f #;(= (hash-table-size ht) (hash-table-next-idx ht))
    (let loop ((from-idx 0)
               (to-idx 0))
      (cond ((or (>= from-idx (hash-table-next-idx ht))
                 (unfilled? (vector-ref (hash-table-keys-vector ht) from-idx)))
             (vector-fill! (hash-table-keys-vector ht)
                           *unfilled*
                           (hash-table-size ht)
                           (hash-table-next-idx ht))
             (vector-fill! (hash-table-values-vector ht)
                           *unfilled*
                           (hash-table-size ht)
                           (hash-table-next-idx ht))
             (hash-table-next-idx-set! ht (hash-table-size ht)))
            ((deletion? (vector-ref (hash-table-keys-vector ht) from-idx))
             (unless fast?
               (compact-array-delete! (hash-table-compact-index ht)
                                      (deletion-bucket
                                       (vector-ref (hash-table-keys-vector ht) from-idx))))
             (loop (+ from-idx 1) to-idx))
            ((= from-idx to-idx) (loop (+ from-idx 1) (+ to-idx 1)))
            (else
             (vector-set! (hash-table-keys-vector ht)
                          to-idx
                          (vector-ref (hash-table-keys-vector ht) from-idx))
             (vector-set! (hash-table-values-vector ht)
                          to-idx
                          (vector-ref (hash-table-values-vector ht) from-idx))
             (unless fast?
               (compact-array-set! (hash-table-compact-index ht)
                                   (hash-table-bucket-for-key
                                    ht
                                    (vector-ref (hash-table-keys-vector ht)
                                                to-idx))
                                   to-idx))
             (loop (+ from-idx 1) (+ to-idx 1)))))))

(define (hash-table-grow-compact-index! ht)
  ;; this parameter isn't tunable: the compact index is always between
  ;; 1/2 and 2/3 full except when the table is brand new
  (let* ((new-size
          (- (floor (* (compact-array-length (hash-table-compact-index ht)) 4/3)) 1))
         (new-compact-index (make-compact-array new-size)))
    ;; take the opportunity to prune dead entries, since we have to
    ;; iterate all of them anyway
    (hash-table-prune-dead-entries! ht #t)
    (hash-table-compact-index-set! ht new-compact-index)
    (let loop ((idx 0))
      (unless (>= idx (vector-length (hash-table-keys-vector ht)))
        (let ((key (vector-ref (hash-table-keys-vector ht) idx)))
          (unless (unfilled? key)
            (compact-array-set! new-compact-index
                                (hash-table-bucket-for-key ht key)
                                idx)
            (loop (+ idx 1))))))))

;; #t if the hash table’s compact index has to grow to accommodate the
;; next association added
(define (hash-table-compact-index-must-grow? ht)
  (>= (hash-table-next-idx ht)
      (floor (* (compact-array-length (hash-table-compact-index ht)) 2/3))))

;; add to the entries arrays, setting the bucket in the compact index
(define (hash-table-add-entry! ht bucket key value)
  (if (>= (hash-table-next-idx ht)
          (vector-length (hash-table-keys-vector ht)))
      (hash-table-grow-entries! ht))
  (if (hash-table-compact-index-must-grow? ht)
      (hash-table-grow-compact-index! ht))
  (vector-set! (hash-table-keys-vector ht)
               (hash-table-next-idx ht)
               key)
  (vector-set! (hash-table-values-vector ht)
               (hash-table-next-idx ht)
               value)
  (compact-array-set! (hash-table-compact-index ht)
                      bucket
                      (hash-table-next-idx ht))
  (hash-table-size-set! ht (+ (hash-table-size ht) 1))
  (hash-table-next-idx-set! ht (+ (hash-table-next-idx ht) 1)))

;; basic public interface, getting and setting
(define (hash-table-set! ht . keys-values)
  (cond
   ((not (even? (length keys-values)))
    (error "there must be as many keys as values"
           (length keys-values) keys-values))
   ((null? keys-values) (void))
   (else
    (let loop ((key (car keys-values))
               (value (cadr keys-values))
               (more-keys-values (cddr keys-values)))
      (hash-table-set-one! ht key value)
      (unless (null? more-keys-values)
        (loop (car more-keys-values)
              (cadr more-keys-values)
              (cddr more-keys-values)))))))

(define (hash-table-set-one! ht key value)
  (let* ((bucket (hash-table-bucket-for-key ht key))
         (entry-idx (compact-array-ref (hash-table-compact-index ht)
                                       bucket)))
    (if entry-idx
        (vector-set! (hash-table-values-vector ht) entry-idx value)
        (begin
         (when (hash-table-compact-index-must-grow? ht)
           (hash-table-grow-compact-index! ht))
         (hash-table-add-entry! ht bucket key value)))))

(define hash-table-ref
  (case-lambda
    ((ht key)
     (hash-table-ref ht key
                     (lambda () (error "key not in table" key ht))
                     values))
    ((ht key failure)
     (hash-table-ref ht key failure values))
    ((ht key failure success)
     (let* ((bucket (hash-table-bucket-for-key ht key))
            (entry-idx (compact-array-ref (hash-table-compact-index ht)
                                          bucket)))
       (if entry-idx
           (success (vector-ref (hash-table-values-vector ht) entry-idx))
           (failure))))))

(define (hash-table-ref/default ht key default)
  (hash-table-ref ht key (lambda () default)))

(define (hash-table-delete! ht . keys)
  (let loop ((n-deleted 0)
             (more-keys keys))
    (if (null? more-keys)
        (begin
          (hash-table-size-set! ht (- (hash-table-size ht) n-deleted))
          (when (> (- (hash-table-next-idx ht) (hash-table-size ht))
                   (* 1/3 (hash-table-size ht)))
            (hash-table-prune-dead-entries! ht #f))
          n-deleted)
        (if (hash-table-delete-one! ht (car more-keys))
            (loop (+ n-deleted 1) (cdr more-keys))
            (loop n-deleted (cdr more-keys))))))

(define (hash-table-delete-one! ht key)
    (let* ((bucket (hash-table-bucket-for-key ht key))
           (entry-idx (compact-array-ref (hash-table-compact-index ht)
                                         bucket)))
      (if entry-idx
          (begin
            (vector-set! (hash-table-keys-vector ht)
                         entry-idx (make-deletion bucket))
            (vector-set! (hash-table-values-vector ht)
                         entry-idx *unfilled*)
            #t)
          #f)))

(define (hash-table-clear! ht)
  ;; assumes the hash table is going to be refilled with approximately
  ;; the same number of associations as were previously in it
  (hash-table-compact-index-set! ht
                                 (make-compact-array
                                  (compact-array-length
                                   (hash-table-compact-index ht))))
  (hash-table-keys-vector-set! ht
                               (make-vector
                                (vector-length
                                 (hash-table-keys-vector ht))
                                *unfilled*))
  (hash-table-values-vector-set! ht
                               (make-vector
                                (vector-length
                                 (hash-table-values-vector ht))
                                *unfilled*))
  (hash-table-size-set! ht 0)
  (hash-table-next-idx-set! ht 0))

(define (hash-table-contains? ht key)
  (let* ((bucket (hash-table-bucket-for-key ht key))
         (entry-idx (compact-array-ref (hash-table-compact-index ht)
                                       bucket)))
    (not (not entry-idx))))

(define (hash-table-empty? ht) (zero? (hash-table-size ht)))

;; cursor-based iteration
(define (hash-table-cursor-first ht)
  (hash-table-cursor-next ht -1))
(define (hash-table-cursor-last ht)
  (hash-table-cursor-previous ht (vector-length (hash-table-keys-vector ht))))

(define (hash-table-cursor-next ht cur)
  (let loop ((n (+ cur 1)))
    (if (>= n (vector-length (hash-table-keys-vector ht))) n
        (let ((key (vector-ref (hash-table-keys-vector ht) n)))
          (cond ((unfilled? key) n)
                ((deletion? key) (loop (+ n 1)))
                (else n))))))
(define (hash-table-cursor-previous ht cur)
  (let loop ((n (- cur 1)))
    (if (< n 0) n
        (let ((key (vector-ref (hash-table-keys-vector ht) n)))
          (if (or (unfilled? key) (deletion? key))
              (loop (- n 1))
              n)))))

(define (hash-table-cursor-key ht cur)
  (vector-ref (hash-table-keys-vector ht) cur))
(define (hash-table-cursor-value ht cur)
  (vector-ref (hash-table-values-vector ht) cur))
(define (hash-table-cursor-key+value ht cur)
  (values (hash-table-cursor-key ht cur)
          (hash-table-cursor-value ht cur)))

(define (hash-table-cursor-at-end? ht cur)
  (or (negative? cur)
      (>= cur (vector-length (hash-table-keys-vector ht)))
      (unfilled? (vector-ref (hash-table-keys-vector ht) cur))))

(define (hash-table-keys ht)
  (unfold
   (lambda (cur) (hash-table-cursor-at-end? ht cur))
   (lambda (cur) (hash-table-cursor-key ht cur))
   (lambda (cur) (hash-table-cursor-next ht cur))
   (hash-table-cursor-first ht)))

(define (hash-table-values ht)
  (unfold
   (lambda (cur) (hash-table-cursor-at-end? ht cur))
   (lambda (cur) (hash-table-cursor-value ht cur))
   (lambda (cur) (hash-table-cursor-next ht cur))
   (hash-table-cursor-first ht)))

(define (hash-table-entries ht)
  (values (hash-table-keys ht)
          (hash-table-values ht)))

(define (hash-table-fold proc seed ht)
  (let loop ((cur (hash-table-cursor-first ht))
             (acc seed))
    (if (hash-table-cursor-at-end? ht cur)
        acc
        (loop (hash-table-cursor-next ht cur)
              (proc (hash-table-cursor-key ht cur)
                    (hash-table-cursor-value ht cur)
                    acc)))))

(define (hash-table-fold-right proc seed ht)
  (let loop ((cur (hash-table-cursor-last ht))
             (acc seed))
    (if (hash-table-cursor-at-end? ht cur)
        acc
        (loop (hash-table-cursor-previous ht cur)
              (proc (hash-table-cursor-key ht cur)
                    (hash-table-cursor-value ht cur)
                    acc)))))

(define (hash-table-for-each proc ht)
  (let loop ((cur (hash-table-cursor-first ht)))
    (unless (hash-table-cursor-at-end? ht cur)
      (call-with-values
          (lambda () (hash-table-cursor-key+value ht cur))
        proc)
      (loop (hash-table-cursor-next ht)))))

(define (hash-table->alist ht)
  (hash-table-fold-right
   (lambda (l k v)
     (cons (cons k v) l))
   '() ht))

(define (hash-table-find proc ht failure)
  (let loop ((cur (hash-table-cursor-first ht)))
    (cond ((hash-table-cursor-at-end? cur)
           (failure))
          ((call-with-values
               (lambda () (hash-table-cursor-key+value ht cur))
             proc))
          (else (loop (hash-table-cursor-next ht cur))))))

(define (hash-table-count pred ht)
  (hash-table-fold
   (lambda (acc k v)
     (if (pred k v)
         (+ acc 1)
         acc))
   0
   ht))

(define (hash-table-prune! proc ht)
  (let loop ((cur (hash-table-cursor-first ht)) (n-deleted 0))
    (if (hash-table-cursor-at-end? ht cur)
        (begin
          (hash-table-size-set! ht (- (hash-table-size ht) n-deleted))
          (when (> (- (hash-table-next-idx ht) (hash-table-size ht))
                   (* 1/3 (hash-table-size ht)))
            (hash-table-prune-dead-entries! ht #f))
          n-deleted)
        (let-values (((k v) (hash-table-cursor-key+value ht cur)))
          (if (and (proc k v)
                   (hash-table-delete-one! ht k))
              (loop (hash-table-cursor-next ht cur) (+ n-deleted 1))
              (loop (hash-table-cursor-next ht cur) n-deleted))))))

;; set-like operations
(define (hash-table-union! ht_1 ht_2)
  (hash-table-for-each
   (lambda (k v)
     (unless (hash-table-contains? ht_1 k)
       (hash-table-set! ht_2 k v)))
   ht_2)
  ht_1)

(define (hash-table-intersection! ht_1 ht_2)
  (hash-table-prune!
   (lambda (k v)
     (not (hash-table-contains? ht_2 k)))
   ht_1)
  ht_1)

(define (hash-table-difference! ht_1 ht_2)
  (hash-table-prune!
   (lambda (k v)
     (hash-table-contains? ht_2 k))
   ht_1)
  ht_1)

(define (hash-table-xor! ht_1 ht_2)
  (hash-table-for-each
   (lambda (k v)
     (if (hashtable-contains? ht_1 k)
         (hashtable-delete! ht_1 k)
         (hashtable-set! ht_1 k v)))
   ht_2)
  ht_1)

(define (hash-table=? value=? ht_1 ht_2)
  (and
   ;; check every association in ht_1 has a corresponding association
   ;; in ht_2
   (let loop ((cur (hash-table-cursor-first ht_1)))
     (cond ((hash-table-cursor-at-end? ht_1 cur) #t)
           ((and (hash-table-contains? ht_2
                                       (hash-table-cursor-key ht_1 cur))
                 (value=? (hash-table-cursor-value ht_1 cur)
                          (hash-table-ref ht_2 (hash-table-cursor-key ht_1 cur))))
            (loop (hash-table-cursor-next ht_1 cur)))
           (else #f)))
   ;; also check there are no entries in ht_2 absent in ht_1
   (let loop ((cur (hash-table-cursor-first ht_2)))
     (cond ((hash-table-cursor-at-end? ht_2 cur) #t)
           ((hash-table-contains? ht_1
                                  (hash-table-cursor-key ht_2 cur))
            (loop (hash-table-cursor-next ht_1 cur)))
           (else #f)))))

;; public utility procedures, with implementations closely adapted
;; from Will Clinger’s original implementation

;; not continuation-safe :-/
(define (hash-table-unfold stop? mapper successor seed comparator)
  (let ((ht (make-hash-table comparator)))
    (let loop ((seed seed))
      (if (stop? seed)
          ht
          (call-with-values
           (lambda () (mapper seed))
           (lambda (key val)
             (hash-table-set! ht key val)
             (loop (successor seed))))))))

(define (hash-table-intern! ht key failure)
  (if (hash-table-contains? ht key)
      (hash-table-ref ht key)
      (let ((val (failure)))
        (hash-table-set! ht key val)
        val)))

(define (hash-table-update! ht key updater . rest)
  (hash-table-set! ht
                   key
                   (updater (apply hash-table-ref ht key rest))))

(define (hash-table-update!/default ht key updater default)
  (hash-table-set! ht key (updater (hash-table-ref/default ht key default))))

