# Ephemeron tables

## Implementation

The following is a stupidly simple implementation in terms of SRFIs 124 and 69.

```scheme
(define (ephemeron-key-eq? eph_1 eph_2)
  (eq? (ephemeron-key eph_1)
       (ephemeron-key eph_2)))
(define (hash-by-ephemeron-key-identity obj)
  (hash-by-identity (ephemeron-key obj)))

(define (make-ephemeron-table)
  (make-hash-table ephemeron-key-eq? hash-by-ephemeron-key-identity))
(define (ephemeron-table? obj)
  (and (hash-table? obj)
       (eq? (hash-table-equivalence-function obj)
            ephemeron-key-eq?)))

(define (ephemeron-table-ref et key)
  (let* ((eph_temp (make-ephemeron key #f))
         (eph_retrieved (hash-table-ref/default et eph_temp #f)))
    (if eph_retrieved
        (let ((value (ephemeron-datum eph_retrieved)))
          (if (ephemeron-broken? eph_retrieved)
              (error "not in ephemeron table" et key)
              (begin
                (reference-barrier key)
                value)))
        (error "not in ephemeron table" et key))))

(define (ephemeron-table-set! et key value)
  (let ((eph (make-ephemeron key value)))
    (hash-table-set! et eph eph)))

(define (ephemeron-table-delete! et key)
  (let ((eph (make-ephemeron key #f)))
    (hash-table-delete! et eph)))

(define (ephemeron-table-contains? et key)
  (let ((eph (make-ephemeron key #f)))
    (hash-table-exists? et eph)))
```
